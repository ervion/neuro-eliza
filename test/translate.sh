#!/bin/bash
#SBATCH -J pythonversion

#SBATCH -N 1

#SBATCH --partition=phi

#SBATCH --nodelist=booster4

#SBATCH -t 00:10:00

THEANO_FLAGS=mode=FAST_RUN,floatX=float32,device=cpu python result.py