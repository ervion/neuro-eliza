#!/bin/bash
#SBATCH -J pythonversion

#SBATCH -N 1

#SBATCH --partition=phi

#SBATCH --nodelist=booster4

#SBATCH -t 00:30:00

NEMATUS=~/hpctest/nematus/build/lib/nematus

THEANO_FLAGS=mode=FAST_RUN,floatX=float32,device=cpu $NEMATUS/nmt.py --datasets corpus.in corpus.out --dictionaries vocab.in.json vocab.out.json --finish_after 500