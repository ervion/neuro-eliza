((aivo_dialoog_nr_199))
((�hestas Andriela R��bis 5.10.2010))

A: tere   | RIE: TERVITUS |
S: tere   | RIJ: VASTUTERVITUS |
S: Millest siis r��gime?   | KYE: AVATUD |
A: Mina olen poliitik Aivo.   | RY: TUTVUSTUS |
S: Tore.   | VR: HINNANGULINE VASTUV�TUTEADE |
 Mina ei ole Aivo.   | YA: INFO ANDMINE |
 Ja poliitik ka mitte.   | YA: INFO ANDMINE |
A: Jah,    | VR: NEUTRAALNE VASTUV�TUTEADE |
poliitika on t�esti tore.   | SEE: ARVAMUS |
S: Ma vist tr�kin kiiremini kui Sina?   | SEE: ARVAMUS |
S: Ei ole tore.   | SEJ: MITTEN�USTUMINE |   | YA: PRAAK | 
S: Mis erakonnas sa oled?   | KYE: AVATUD |
A: Elus ja ka poliitikas ei ole kiirus alati k�ige olulisem.   | SEJ: MUU |    | YA: PRAAK | 
A: Ma huvitun �hiskonnaelust �ldiselt ega hooli konkreetsetest erakondadest.    | KYJ: INFO ANDMINE |
S: Millised on siis Sinu p�hipunktid?   | KYE: AVATUD |
A: Minu p�hipunktideks on: v�rdsus, vabadus ja �iglus.   | KYJ: INFO ANDMINE |
S: Aga t�ekspidamised?   | KYE: AVATUD |
A: Kahjuks ma ei m�ista teid.     | DIE: MUU |   | PPE: MITTEM�ISTMINE | 
S: Mis on v�rdsus?   | KYE: AVATUD |
A: See on v�ga hea k�simus.      | VR: HINNANGULINE VASTUV�TUTEADE |
 Ma pean selle �le j�rele m�tlema.   | KYJ: EDASIL�KKAMINE |
S: Ammu oleks aeg.      | VR: MUU |
A: Elus ja ka poliitikas ei ole kiirus alati k�ige olulisem.    | SEE: V�IDE | 
S: Ei ole vist jah.   | SEJ: PIIRATUD N�USTUMINE |
 Millal oleks aeg?   | KYE: AVATUD |
A: Mis ajast k�ib jutt?   | KYE: AVATUD |   | PPE: MITTEM�ISTMINE | 
S: L�pp   | RIE: L�PUSIGNAAL |
      
