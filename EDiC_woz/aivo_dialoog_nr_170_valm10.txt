((aivo_dialoog_nr_170))
((ühestas Andriela Rääbis 5.10.2010))

A: tere   | RIE: TERVITUS |
S: tervist   | RIJ: VASTUTERVITUS |
A: annan infot Tartu kinokavade kohta (kino Ekraan, kino Cinamon); info otsimine võib võtta aega, palun kannatlikkust    | YA: INFO ANDMINE |
S: Miks su nimi Aivo on?   | KYE: AVATUD |
A: kahjuks ei saa aru      | DIE: MUU | | PPE: MITTEMÕISTMINE |
S: Pole hullu.   | VR: HINNANGULINE VASTUVÕTUTEADE |
S: Halloo?   | KKE: ALGATUS |
A: palun täpsusta, vajan vastamiseks detaile   | KKJ: KINNITAMINE |   | DIE: SOOV |     | PPE: MITTEMÕISTMINE |
 S: Miks sa nii vaikne oled?   | KYE: AVATUD |
A: vaikne olen sellepärast, et olen rumal   | KYJ: INFO ANDMINE |
S: Miks sa nii arvad?   | KYE: AVATUD |
A: palun täpsusta   | DIE: SOOV |   | PPE: MITTEMÕISTMINE | 
S: Miks sa arvad, et sa rumal oled?   | DIJ: INFO ANDMINE |   | PPJ: LÄBIVIIMINE |    | KYE: AVATUD | 
A: olen rumal, kuna minu programm ei ole piisavalt ammendav   | KYJ: INFO ANDMINE |
S: head aega   | RIE: HÜVASTIJÄTT |
A: head aega   | RIJ: VASTUHÜVASTIJÄTT |
      
