((aivo_dialoog_nr_052))
((ühestas Andriela Rääbis 28.09.2010))

A: tere   | RIE: TERVITUS |
S: tere   | RIJ: VASTUTERVITUS |
A: annan infot Tartu kinokavade kohta (kino Ekraan, kino Cinamon); info otsimine võib võtta aega, palun olge kannatlik   | YA: INFO ANDMINE |
A: ootan küsimusi   | RY: MUU | 
S: millised filmid on täna Cinamonis   | KYE: AVATUD |
A: ?Kahepalgelised? 13.45   | KYJ: INFO ANDMINE |
A: ?Rentslimiljonär? 17.00   | KYJ: INFO ANDMINE |
A: ?Kiired ja vihased: Uus mudel? 13.00, 15.15, 17.30, 19.45, 22.00   | KYJ: INFO ANDMINE |
A: ?Koletised tulnukate vastu? 12.45, 15.00   | KYJ: INFO ANDMINE |
A: see on perefilm   | KYJ: INFO ANDMINE |
S: mis hinnaga on rentslimiljonär   | KYE: AVATUD |
A: ?Adrenaliin: Kõrgepinge?11.45; 16.15; 18.15, 20.15, 22.15   | KYJ: MUU |   | YA: PRAAK | 
A: sisestage nädalapäev ja kellaaeg, siis saan hinnakirjast teie piletihinna otsida   | DIE: SOOV |   | VTE: VASTUSE TINGIMUSTE TÄPSUSTAMINE | 
S: Laupäev 17.00   | DIJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE TÄPSUSTAMINE | 
A: kas täna?  | KYE: SULETUD KAS |  | PPE: ÜMBERSÕNASTAMINE |
A: 50 krooni   | KYJ: INFO ANDMINE |
A: internetist ostes on odavam   | KYJ: INFO ANDMINE |
A: see film on mitu Oscarit võitnud      | IL: TÄPSUSTAMINE |
A: tegite hea valiku      | IL: HINNANG |
S: MIllise hinnagi siis internteist ostes piletid on   | KYE: AVATUD |
A: kas soovite veel infot?   | KYE: JUTUSTAV KAS |   | RY: MUU |  | YA: PRAAK |
A: oleneb kellaajast        | KYJ: INFO ANDMINE |
A: kuni 17.00 45 krooni     | KYJ: INFO ANDMINE |  
A: al 17.01 77      | KYJ: INFO ANDMINE |
A: on ka mitmeid soodustusi      | KYJ: INFO ANDMINE |
A: boonuskliendile      | KYJ: INFO ANDMINE |
A: üliõpilasele      | KYJ: INFO ANDMINE |
A: lastele      | KYJ: INFO ANDMINE |
A: peredele      | KYJ: INFO ANDMINE |
A: kuku!?   | KKE: ALGATUS |
S: palju on üliõpilase soodustused    | KKJ: KINNITAMINE | | KYE: AVATUD |   
A: oleneb kellaajast      | KYJ: INFO ANDMINE |
A: mis kell soovid kinno minna?   | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE TÄPSUSTAMINE | 
A: oled seal?   | KYE: SULETUD KAS |   | KKE: ALGATUS | 
S: 17.00     | KYJ: INFO ANDMINE |      | VTJ: VASTUSE TINGIMUSTE TÄPSUSTAMINE |  | KKJ: KINNITAMINE |
A: 50 krooni   | KYJ: INFO ANDMINE | 
A: kassast ostes   | KYJ: INFO ANDMINE |
A: ER      | KYJ: INFO ANDMINE |
S: Üliõpilasele? | KYE: VASTUST PAKKUV |   | PPE: ÜLEKÜSIMINE | 
A: jah | KYJ: JAH | | PPJ: LÄBIVIIMINE |
A: kuni 17.00 on ühesugused hinnad   | IL: TÄPSUSTAMINE |   | PPJ: LÄBIVIIMINE | 
A: soodustus algab hilisematele aegadele   | IL: TÄPSUSTAMINE |   | PPJ: LÄBIVIIMINE | 
S: aga aitähh   | RIE: TÄNAN |
S: head aega   | RIE: HÜVASTIJÄTT |
A: palun   | RIJ: PALUN |
A: head aega   | RIJ: VASTUHÜVASTIJÄTT |
A: oli tore vestelda      | RIE: MUU |
      
