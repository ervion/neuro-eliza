((aivo_dialoog_nr_160))
((�hestas Andriela R��bis 18.01.2011))

A: tere   | RIE: TERVITUS |
S: tere   | RIJ: VASTUTERVITUS |
A: annan infot Tartu kinokavade kohta (kino Ekraan, kino Cinamon); info otsimine v�ib v�tta aega, palun kannatlikkust    | YA: INFO ANDMINE |
A: vestluse l�petamiseks kirjuta L�PP   | YA: INFO ANDMINE |
S: Kes sa selline oled?   | KYE: AVATUD |
A: olen Aivo   | KYJ: INFO ANDMINE |
S: Millega tegeled?   | KYE: AVATUD |
A: annan infot Tartu kinokavade kohta (kino Ekraan, kino Cinamon); info otsimine v�ib v�tta aega, palun kannatlikkust   | KYJ: INFO ANDMINE |
S: Kas sulle meeldib infot jagada kinokavade kohta?   | KYE: SULETUD KAS |
A: jah, meeldib k�ll   | KYJ: JAH |
S: Mida sulle veel meeldib teha?   | KYE: AVATUD |
A: antud info p�hjal ei oska ma vastata   | KYJ: INFO PUUDUMINE |
S: Kas t�na on �ues k�lm v�i soe?   | KYE: ALTERNATIIV |
A: annan infot Tartu kinokavade kohta (kino Ekraan, kino Cinamon); info otsimine v�ib v�tta aega, palun kannatlikkust      | YA: INFO ANDMINE |     | YA: PRAAK |
S: Millal sa puhkad?   | KYE: AVATUD |
A: puhkamisest ei tea ma midagi   | KYJ: INFO PUUDUMINE |
S: Millal sa magad?   | KYE: AVATUD |
A: pole sinu asi   | KYJ: KEELDUMINE |
A: annan infot Tartu kinokavade kohta (kino Ekraan, kino Cinamon); info otsimine v�ib v�tta aega, palun kannatlikkust   | YA: INFO ANDMINE | 
S: Milal sa s��d?   | KYE: AVATUD | 
S: Millal sa s��d?   | KYE: AVATUD |   | PA: ENESEPARANDUS | 
A: s��misest ei tea ma midagi   | KYJ: INFO PUUDUMINE |
A: kas sa k�id tihti kinos?     | KYE: JUTUSTAV KAS | 
S: Ei,    | KYJ: EI |
kas sina k�id tihti kinos?   | KYE: JUTUSTAV KAS |
A: kino on mu elu   | KYJ: INFO ANDMINE |
S: Head aega.   | RIE: H�VASTIJ�TT |
A: head aega   | RIJ: VASTUH�VASTIJ�TT |
      
