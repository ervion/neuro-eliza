((aivo_dialoog_nr_244))
((ühestas Andriela Rääbis 29.12.2010))

A: tere   | RIE: TERVITUS |
S: tere   | RIJ: VASTUTERVITUS |
A: Olen Aivo,   | RY: TUTVUSTUS |
annan infot lendude kohta EestisEestist      | YA: INFO ANDMINE |
S: Kas Eestist on otselendu Nigeeriasse ?   | KYE: JUTUSTAV KAS |
A: Info saamiseks sisestage tekst lause kujul      | YA: INFO ANDMINE |   | YA: PRAAK |
A: Otsin...   | KYJ: EDASILÜKKAMINE |
A: Kahjuks ei leitud lennujaama Nigeeriasse   | KYJ: INFO PUUDUMINE |
A: Sisestage uus otsing   | DIE: ETTEPANEK |
S: Sellest on kahju   | VR: HINNANGULINE VASTUVÕTUTEADE |
A: Vabandust, aga ma ei saanud teie lausest aru     | DIE: MUU |    | PPE: MITTEMÕISTMINE | 
S: Kas oskate soovitada lendu mõnda Aafrika riiki ?   | DIJ: NÕUSTUMINE |   | KYE: JUTUSTAV KAS | 
A: Otsin...   | KYJ: EDASILÜKKAMINE |
A: Kahjuks ei leitud lennujaama Aafrika   | KYJ: INFO PUUDUMINE |
A: Sisestage uus otsing   | DIE: ETTEPANEK |
S: Mõni lend Egiptusesse ?    | DIJ: NÕUSTUMINE |  | KYE: AVATUD |
A: Otsin...   | KYJ: EDASILÜKKAMINE |
A: Valige lennujaam järgnevatest:   | DIE: ETTEPANEK |   | VTE: VASTUSE TINGIMUSTE TÄPSUSTAMINE | 
A: Abu Rudeis (AUE) Abu Simbel (ABS) Al Arish (AAC) Alexandria, Borg El Arab Airport (HBE) Alexandria, El Nohza (ALY) Assiu   | DIE: ETTEPANEK |   | VTE: VASTUSE TINGIMUSTE TÄPSUSTAMINE | 
A: Luxor (LXR) Marsa Alam, Marsa Alam (RMF) Mersa Matruh (MUH) Port Said (PSD) Ramadan (TFR) Santa Katarina, Mount Sinai (S  | DIE: ETTEPANEK |    | VTE: VASTUSE TINGIMUSTE TÄPSUSTAMINE |
      
