((aivo_dialoog_nr_082))
((�hestas Andriela R��bis 5.10.2010))

A: tere   | RIE: TERVITUS |
S: teretere   | RIJ: VASTUTERVITUS |
A: annan infot Tartu kinokavade kohta (kino Ekraan, kino Cinamon); info otsimine v�ib v�tta aega, palun kannatlikkust   | YA: INFO ANDMINE |
A: kas sul on k�simusi?   | KYE: JUTUSTAV KAS |  | RY: MUU | 
S: muidugi   | KYJ: JAH |
A: kuulan       | VR: NEUTRAALNE J�TKAJA |
S: mis kino n�idatakse t�na Ekaanis kell 20.00   | KYE: AVATUD |
A: �ks hetk   | KYJ: EDASIL�KKAMINE |
A: kell 20.00 ei ole midagi   | KYJ: INFO ANDMINE |
A: Star Trek, kell 21.30   | KYJ: INFO ANDMINE |
A: see on ulmefilm   | KYJ: INFO ANDMINE |
S: aga kell 18.00 | KYE: AVATUD |
A: kell on 20.44   | KYJ: MUU |
S: t�nan   | RIE: T�NAN |
A: t�nasele seansile ei j�ua enam   | KYJ: MUU |    
A: palun   | RIJ: PALUN |
S: aga kell 18.00 paiku    | KYE: AVATUD |
A: �ks hetk   | KYJ: EDASIL�KKAMINE |
S: aga �tleme siis et homme kell 15.00 | KYE: AVATUD |
A: Star Trek, kell 18.45     | KYJ: INFO ANDMINE |  | YA: PRAAK | 
A: homme?    | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE | | YA: PRAAK | 
A: XMeeste algus: Wolverine, kell 14.15, | KYJ: INFO ANDMINE | 
S: jah | KYJ: JAH | | PPJ: L�BIVIIMINE | | YA: PRAAK |
A: action   | KYJ: INFO ANDMINE | 
A: alla 12a mittesoovitav   | KYJ: INFO ANDMINE |
A: pikkus: 109 min.   | KYJ: INFO ANDMINE |
S: aga mingi kom��dia?  | KYE: AVATUD |
A: film on esimene peat�kk Xmeeste saagast, mille fantastilise maailmaga puutusid vaatajad esmakordselt kokku aastal 2000   | KYJ: INFO ANDMINE |   | YA: PRAAK |
A: kom��dia?   | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE |   | YA: PRAAK |
A: kohe otsin   | KYJ: EDASIL�KKAMINE |
A: Ekraanis kom��diat ei ole | KYJ: INFO ANDMINE |
S: jah, homme Cinamonis | KYJ: JAH | | PPJ: L�BIVIIMINE |   | YA: PRAAK |
A: Cinamonis on 2 kom��diat | KYJ: INFO ANDMINE | 
A: J�lgi ja kanna ette, kell 19.30 ja 21.45   | KYJ: INFO ANDMINE | 
S: millised?   | KYE: AVATUD |     | YA: PRAAK |
S: ja teine      | KYE: AVATUD |
A: J�lle 17, kell 19.15, 21.30   | KYJ: INFO ANDMINE |
S: super   | VR: HINNANGULINE VASTUV�TUTEADE |
S: head aega   | RIE: H�VASTIJ�TT |
A: head aega   | RIJ: VASTUH�VASTIJ�TT |
A: kuhu kadusid?   | KYE: AVATUD |   | KKE: ALGATUS |
      

