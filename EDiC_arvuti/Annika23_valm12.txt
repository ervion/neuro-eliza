((Annika 23))
((�hestas Andriela R��bis 16.10.2012))

Annika:	tere! | RIE: TERVITUS | 
minult saate k�sida hambaravi, igemeravi, implantaatide, proteeside kohta ... | YA: INFO ANDMINE | 
Sina:	Kuidas implantaate pannakse? | KYE: AVATUD |
Sina:	Millest selle hind soltub? | KYE: AVATUD |
Annika:	p�hjalikum info on siin: [loe implantaatidest], t�pse hinna saab arst �elda peale esmast kontrolli, hindadest saate �levaate siit [ava hinnakiri]  | KYJ: MUU |
Annika:	�ks hetk, kohe r��gin teile l�hidalt implantaadi paigaldusest ... | KYJ: EDASIL�KKAMINE |
Annika:	k�igepealt asetatakse implantaat, mis sarnaneb kruviga, teie l�ualuusse | KYJ: INFO ANDMINE |
Annika:	see kaetakse ajutise hambaga, mis varjab t�himiku | KYJ: INFO ANDMINE |
Annika:	implantaadil lastakse 3-6 kuud luustuda, misj�rel kinnitatakse selle k�lge hambakroon | KYJ: INFO ANDMINE |
Annika:	hinnad on ligikaudu sellised, �ks tehisjuur <HIND>, selle paigaldus <HIND>, kroonimine <HIND>, kroon <HIND> | KYJ: INFO ANDMINE |
Annika:	kui midagi vastamata j�tsin, siis palun k�sige Dr. X-lt �le | DIE: ETTEPANEK |
Sina:	Millistel juhtudel peab ootama kuni kuus kuud? | KYE: AVATUD | | PPE: MITTEM�ISTMINE |
Annika:	soovite veel midagi lisada? | KYE: JUTUSTAV KAS | | RY: MUU |
Sina:	ei | KYJ: EI | 
aitah | RIE: T�NAN |
Annika:	v�tke heaks | RIJ: PALUN |
