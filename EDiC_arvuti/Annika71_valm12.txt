((Annika 71))
((�hestas Andriela R��bis 18.12.2012))

Annika:	tere!   | RIE: TERVITUS |
 minult saate k�sida hambaravi, igemeravi, implantaatide, proteeside kohta ...   | YA: INFO ANDMINE | 
Sina:	kui paigaldada hambaimplantaat, siis alates millest algavad hinnad ja millest see hind s�ltub    | KYE: AVATUD |
Annika:	p�hjalikum info on siin: [loe implantaatidest]   | KYJ: MUU |
Annika:	t�pse hinna saab arst �elda peale esmast kontrolli, hindadest saate �levaate siit [ava hinnakiri]   | KYJ: MUU |
Annika:	�ks hetk, kohe r��gin teile l�hidalt implantaadi paigaldusest ...   | KYJ: EDASIL�KKAMINE |
Annika:	k�igepealt asetatakse implantaat, mis sarnaneb kruviga, teie l�ualuusse   | KYJ: MUU |
Annika:	see kaetakse ajutise hambaga, mis varjab t�himiku   | KYJ: MUU |
Annika:	implantaadil lastakse 3-6 kuud luustuda, misj�rel kinnitatakse selle k�lge hambakroon   | KYJ: MUU |
Annika:	hinnad on ligikaudu sellised, �ks tehisjuur <HIND>, selle paigaldus <HIND>, kroonimine <HIND>, kroon <HIND>   | KYJ: INFO ANDMINE |
Annika:	kui midagi vastamata j�tsin, siis palun k�sige Dr. X-lt �le   | DIE: ETTEPANEK |
Sina:	ait�h   | RIE: T�NAN |
Annika:	v�tke heaks   | RIJ: PALUN |
      
