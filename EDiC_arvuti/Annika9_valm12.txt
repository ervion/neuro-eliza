((Annika 9))
((�hestas Andriela R��bis 4.02.2013))

Annika:	tere!   | RIE: TERVITUS |
 minult saate k�sida hambaravi, igemeravi, implantaatide, proteeside kohta ...   | YA: INFO ANDMINE |
Sina:	tahaks teada et kui mul on ajutised sild proteesid mis liimiga ma peaks neid kylge liimima    | KYE: AVATUD |
Annika:	sildprotees tsementeeritakse tugihammastele    | KYJ: INFO ANDMINE |
Annika:	proteesidega (kroonidega) variandid on kirjas siin vt. [loe proteesimisest]    | KYJ: INFO ANDMINE | 
Sina:	aga kui nad lahti tulevad kuidas ma nad kinnitan    | KYE: AVATUD |
Annika:	k�igepealt tuleb teha r�ntgen ja esmane �levaatus, seej�rel saab valida proteesi, mis teile sobib    | YA: INFO ANDMINE |
Annika:	proteeside konstruktsioone on v�ga palju erinevaid, ka proteeside eluiga on erinev    | YA: INFO ANDMINE |
Annika:	on v�imalik valmistada ka ajutine protees   | YA: INFO ANDMINE |
Annika:	ajutist proteesi kasutatakse ajal, mil implantaat luustub v�i ige paraneb ning p�hiproteesi valmistamine k�ib   | YA: INFO ANDMINE | 
Sina:	millist liimi peaks kasutama sild proteeside kinnitamiseks   | KYE: AVATUD |

      
