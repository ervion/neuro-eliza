((Annika 115))
((�hestas Andriela R��bis 8.10.2013))

Annika:	tere! | RIE: TERVITUS |
minult saate k�sida hambaravi, igemeravi, implantaatide, proteeside kohta ... | YA: INFO ANDMINE |
Sina:	kuidas igemep6letiku ravida ? | KYE: AVATUD |
Annika:	detailse raviplaani saame koostada peale esmast visiiti | KYJ: INFO ANDMINE |
Annika:	esmane ravi on biokile mehaaniline eemaldamine ja juurepindade silestamine, millele lisandub vajadusel antibiootikumravi | KYJ: INFO ANDMINE |
Annika:	gingiviit on igeme p�letik ja parodontiit on hammast �mbritsevate kudede krooniline haigus | KYJ: MUU |
Sina:	on see valus ? | KYE: SULETUD KAS |
Annika:	tehakse tuimestus ja ei ole valus | KYJ: EI |
Annika (woz):	puhastamise k�igus v�ib natuke valus siiski saada | IL: T�PSUSTAMINE |
Annika:	valuga tuleks p��rduda arsti juurde, kuid kui kohe ei saa, siis valuvaigistitega valu maha v�tta | IL: T�PSUSTAMINE |
Sina:	ma ei saa midagi narida | SEE: V�IDE |
Annika (woz):	jah, see viitab probleemi olemasolule | SEJ: N�USTUMINE |
Sina:	valus hakkab | IL: T�PSUSTAMINE |
Sina:	kas p2rast esimest ravi on k6ik korras ? | KYE: SULETUD KAS |
Annika (woz):	ei, see v�ib v�tta rohkem aega | KYJ: EI |
Annika (woz):	s�ltuvalt haiguse astmest | IL: T�PSUSTAMINE |
Sina:	kaua ? | KYE: AVATUD |
Sina:	umbes ? | IL: T�PSUSTAMINE |
Annika (woz):	aga lihtsamal juhul piisab paarist visiidist | KYJ: INFO ANDMINE |
Annika (woz):	on ka neid, kes k�ivad mitu korda kuus ja nii kolm kuud | KYJ: INFO ANDMINE |
Sina:	ja siis kaob paistetud 2ra, p2rast m6nda visiiti ? | KYE: VASTUST PAKKUV |
Annika:	visiiditasu on esmasel konsultatsioonil 19 kuni <HIND> (siin sisaldub ka raviplaani koostamine) | IL: T�PSUSTAMINE |
Sina:	paistetus | DIE: MUU |
Annika:	enne esmast visiiti tuleks teha panoraamr�ntgen | IL: T�PSUSTAMINE |
Annika:	r�ntgeni saate teha <KLIINIK> | IL: T�PSUSTAMINE |
Annika:	paistes? | VR: NEUTRAALNE VASTUV�TUTEADE |
paistetus v�ib olla p�ris t�sine olukord, tuleks tulla arsti vastuv�tule | DIJ: INFO ANDMINE |
Sina:	kas paistetus kaob p2rast m6nda visiiti ? | KYE: SULETUD KAS |
Annika (woz):	jah, see peaks taanduma | KYJ: JAH |
Sina:	ma saan suua ainult putru ja suppi | DIE: MUU |
Annika (woz):	jah, kui men��d n�nda piirate, siis peaksite m�nele arstile olukorda n�itama | DIJ: INFO ANDMINE |
Annika:	k�sige igaks juhuks Dr. X-lt �le  | DIJ: INFO ANDMINE | | DIE: ETTEPANEK |
Sina:	aga kaua see paistetus kestab ? | KYE: AVATUD |
Annika:	paistes? | VR: NEUTRAALNE VASTUV�TUTEADE |
paistetus v�ib olla p�ris t�sine olukord, tuleks tulla arsti vastuv�tule | DIJ: MUU |
Annika:	ravi kestvus on individuaalne ja selgub esmasel visiidil | KYJ: INFO PUUDUMINE |
Sina:	kuidas koduste vahenditega paistetust 2ra saada | KYE: AVATUD |
Sina:	? | KYE: MUU |
Annika (woz):	puhastage ettevaatlikult niidiga hambavahesid | KYJ: INFO ANDMINE |
Annika (woz):	arst saab m��rata ka loputusvahendi koduseks kasutamiseks | KYJ: INFO ANDMINE |
Sina:	olgu | VR: NEUTRAALNE J�TKAJA |
Annika (woz):	aga kodus seda nullist oma j�uga ravida v�imalik ei pruugi olla | KYJ: INFO ANDMINE |
Annika (woz):	kodus saab teha peamiselt ennetust ja j�relhooldust | KYJ: INFO ANDMINE |
Annika:	kui midagi vastamata j�tsin, siis palun k�sige Dr. X-lt �le | DIE: ETTEPANEK |
Sina:	aga kuidas ma hambaid pesen ? | KYE: AVATUD |
mul on v2ga valus ja igemed hakkavad koheselt veritsema | IL: T�PSUSTAMINE |
Annika:	hammaste pesemine aitab �ra hoida pindmise p�letiku e. gingiviidi teket. | YA: INFO ANDMINE |
luup�letikku e. parodontiiti ainult hambapesuga ravida ei saa | YA: INFO ANDMINE |
Annika:	veritsevate igemetega tuleks p��rduda arsti juurde | KYJ: MUU |
Annika:	valuga tuleks p��rduda arsti juurde, kuid kui kohe ei saa, siis valuvaigistitega valu maha v�tta | KYJ: MUU |
Annika:	tehakse tuimestus ja ei ole valus | YA: INFO ANDMINE |
Annika:	kodune hooldus seisneb hambavahede puhastamises niidiga | KYJ: INFO ANDMINE |
Annika (woz):	hambahari ei tohi olla v�ga tugevate harjastega | KYJ: INFO ANDMINE |
Annika:	hambahaiguste �rahoidmiseks on t�htis, et hammaste igalt pinnalt eemaldataks katt hoolsalt ja korrap�raselt | YA: INFO ANDMINE |
Annika:	ei tohi vigastada iget teravate tikkudega ja ei tohi kasutada hambaharja, mis on k�vade harjastega | YA: INFO ANDMINE |
Sina:	nahk tuleb hambalt 2ra | DIE: MUU |
Annika (woz):	jah, seda nahka ehk iget ei tohi vigastada | DIJ: MUU |
Annika:	soovite veel midagi lisada? | KYE: JUTUSTAV KAS | | RY: MUU |
Sina:	aga harjamisel tuleb verd | DIE: MUU |
Annika:	veritsusega tuleks p��rduda arsti juurde, kui veritsus v�ga suur, siis helistage 112 | DIJ: INFO ANDMINE |
