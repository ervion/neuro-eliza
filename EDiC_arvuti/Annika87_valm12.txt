((Annika 87))
((�hestas Andriela R��bis 18.12.2012))

Annika:	tere!   | RIE: TERVITUS |
 minult saate k�sida hambaravi, igemeravi, implantaatide, proteeside kohta ...   | YA: INFO ANDMINE |
Annika:	kui midagi vastamata j�tsin, siis palun k�sige Dr. X-lt �le   | DIE: ETTEPANEK |
Sina:	Tere   | RIJ: VASTUTERVITUS |
Sina:	Kas igemeravi pakutakse ainult Tartus v�i peaksid k�ik hambaarstid olema sellele spetsialiseerunud?   | KYE: ALTERNATIIV |
Annika:	igemeravi on hammast �mbritsevate kudede ravi   | KYJ: MUU |
Annika:	Dr. X number on <TEL_NR>, Tartus asume jah - <AADRESS>   | KYJ: MUU |
Annika:	igemeravi sisaldab erinevaid protseduure   | KYJ: MUU |
Annika:	alustuseks uuritakse kui ulatuslik on teie p�letik ning tehakse raviplaan   | KYJ: MUU |
Annika:	enne esmast visiiti tuleks teha panoraamr�ntgen   | IL: T�PSUSTAMINE |
Annika:	r�ntgeni saate teha <KLIINIK>   | IL: T�PSUSTAMINE |
Annika:	soovite veel midagi lisada?   | KYE: JUTUSTAV KAS | | RY: MUU |
Sina:	Lihtsalt m�tlen, et kas mu oma hambaarst ei oska igemete ravi kohta mingit raviplaani paika panna?   | KYE: JUTUSTAV KAS |
Annika:	oskan anda esmast infot igemeravi ja suutervishoiu osas   | KYJ: MUU |
Sina:	hambakivi jm k�ll eemaldati, ag an�en kuidas kolmel hambal j�tkuvalt ige taandub   | SEE: V�IDE |
Annika:	hambakivi on hambale ladestunud k�va katt, mis tuleks kindlasti arstil eemaldada     | YA: INFO ANDMINE |
Annika:	allavajunud ige v�ib olla haiguse v�i trauma tagaj�rg     | YA: INFO ANDMINE |
Annika:	kui j�tta hambakivi eemaldamata, siis on oht igemehaiguste tekkeks     | YA: INFO ANDMINE |
Annika:	hambakivi sisaldab suurel hulgal  baktereid, mille tagaj�rjeks on veritsevad ning valutavad igemed, paha maitse suus ning halb hinge�hk   | YA: INFO ANDMINE |
Sina:	hambakivi eemaldati, aga ikkagi j�tkub igeme taandumine   | SEE: V�IDE |
Annika:	ise koduste vahenditega hambakivi �ldjuhul eemaldada ei �nnestu      | YA: INFO ANDMINE |
Annika:	hambakivi eemaldamisel kasutatakse erinevaid meetodeid   | YA: INFO ANDMINE |
Sina:	arst eemaldas hambakivi, aga igemed taanduvad j�tkuvalt   | SEE: V�IDE |
      
