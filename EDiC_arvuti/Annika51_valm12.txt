Annika:	tere! | RIE: TERVITUS |
minult saate k�sida hambaravi, igemeravi, implantaatide, proteeside kohta ... | YA: INFO ANDMINE |
Annika:	kui midagi vastamata j�tsin, siis palun k�sige Dr. X-lt �le | DIE: ETTEPANEK |
Sina:	tere! | RIJ: VASTUTERVITUS | 
mul on igemetel m�da villid mis teha | KYE: AVATUD |
Annika:	tegemist v�ib olla ka suu limaskesta p�letikuga, oleks m�istlik seda arstile n�idata | KYJ: INFO ANDMINE |
Annika:	stomatiit on haavanditega kulgev suu p�letik, mille tekkimisv�imalusi on v�ga mitmeid, sh ka s��k | KYJ: INFO ANDMINE |
Sina:	kas pean p��rduma hambaarsti poole | KYE: SULETUD KAS | | PPE: �MBERS�NASTAMINE |
Annika:	Dr. X number on <TEL_NR> | KYJ: MUU |
Annika (woz):	v�ite ka telefonis lihtsalt k�sida tema arvamust | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE |
Sina:	aga mis linnas asub dr. X | KYE: AVATUD |
Annika (woz):	Tartus | KYJ: INFO ANDMINE |
Sina:	ma t�nan | RIE: T�NAN |
ja k�ike head | RIE: SOOVIMINE |
Annika (woz):	head �htut | RIE: H�VASTIJ�TT |
