((Annika 114))
((�hestas Andriela R��bis 15.02.2013))

Annika:	tere! | RIE: TERVITUS |
minult saate k�sida hambaravi, igemeravi, implantaatide, proteeside kohta ... | YA: INFO ANDMINE |
Sina:	millest alustada ? | KYE: AVATUD | | YA: RETOORILINE K�SIMUS |
Sina:	hambaid ei ole | DIE: SOOV |
Annika (woz):	�ldse mitte? | KYE: VASTUST PAKKUV | | PPE: �MBERS�NASTAMINE |
Annika (woz):	sellisel juhul proteesimine v�i implantaadid | DIJ: INFO ANDMINE |
Sina:	pooled on alles | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE |
Annika (woz):	siis saab kombineerida implantaatide ja proteesidega | DIJ: INFO ANDMINE |
Sina:	palju �ks implantaat maksab ? | KYE: AVATUD |
Annika:	p�hjalikum info on siin: [loe implantaatidest] | KYJ: MUU |
Annika:	t�pse hinna saab arst �elda peale esmast kontrolli, hindadest saate �levaate siit [ava hinnakiri] | KYJ: MUU |
Annika:	�ks hetk, kohe r��gin teile l�hidalt implantaadi paigaldusest ... | KYJ: EDASIL�KKAMINE |
Sina:	palju suut�is proteese maksavad ? | KYE: AVATUD |
Annika:	k�igepealt asetatakse implantaat, mis sarnaneb kruviga, teie l�ualuusse | KYJ: MUU | 
Annika:	see kaetakse ajutise hambaga, mis varjab t�himiku | KYJ: MUU | 
Annika:	implantaadil lastakse 3-6 kuud luustuda, misj�rel kinnitatakse selle k�lge hambakroon | KYJ: MUU | 
Annika:	hinnad on ligikaudu sellised, �ks tehisjuur <HIND>, selle paigaldus <HIND>, kroonimine <HIND>, kroon <HIND> | KYJ: INFO ANDMINE | 
Annika:	proteesidega (kroonidega) variandid on kirjas siin vt. [loe proteesimisest] | KYJ: MUU | 
Annika:	k�igepealt tuleb teha r�ntgen ja esmane �levaatus, seej�rel saab valida proteesi, mis teile sobib | KYJ: MUU | 
Annika:	proteeside konstruktsioone on v�ga palju erinevaid, ka proteeside eluiga on erinev | KYJ: MUU | 
Annika:	on v�imalik valmistada ka ajutine protees | KYJ: MUU | 
Annika:	ajutist proteesi kasutatakse ajal, mil implantaat luustub v�i ige paraneb ning p�hiproteesi valmistamine k�ib | KYJ: MUU | 
Annika:	kui midagi vastamata j�tsin, siis palun k�sige Dr. X-lt �le | DIE: ETTEPANEK |
Sina:	raha ei ole ei saa k�sida | DIJ: MITTEN�USTUMINE | | SEE: V�IDE |
Annika:	t�pse hinna saab arst �elda peale esmast kontrolli, hindadest saate �levaate siit [ava hinnakiri] | KYJ: MUU |
Sina:	arvata v�ib | VR: HINNANGULINE J�TKAJA |
Annika (woz):	on ka �ppekabinet, kus pakutakse tasuta ravi | SEJ: MUU |
Annika (woz):	see on Tartus Raekoja platsis | SEJ: MUU |
Annika (woz):	proovige sealt uurida | DIE: ETTEPANEK |
Annika (woz):	t�psemalt selle kohta ei oska infot anda | IL: T�PSUSTAMINE |
Annika:	soovite veel midagi lisada? | KYE: JUTUSTAV KAS | | RY: MUU |
Sina:	t�nan info eest | RIE: T�NAN |
Annika:	palun | RIJ: PALUN | | RIE: PALUN |
Sina:	t�nan | RIJ: T�NAN |
Annika:	k�sige igaks juhuks Dr. X-lt �le | DIE: ETTEPANEK |
