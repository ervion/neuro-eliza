((Annika 19))
((�hestas Andriela R��bis 4.02.2013))

Annika:	tere!    | RIE: TERVITUS |
minult saate k�sida hambaravi, igemeravi, implantaatide, proteeside kohta ...   | YA: INFO ANDMINE | 
Sina:	tere   | RIJ: VASTUTERVITUS | 
Sina:	mul on tihti hambad tundlikud ja peale harjamist tunnen,et hambad on ikka mustad   | DIE: MUU | 
Sina:	nagu mingi katt on peal    | IL: T�PSUSTAMINE |
Annika:	soovitame tulla visiidile, vaatame teie olukorra �le,   | DIJ: MUU |  | DIE: ETTEPANEK |
hambakivi on hambale ladestunud k�va katt, mis tuleks kindlasti arstil eemaldada    | DIJ: MUU |
Annika:	kui j�tta hambakivi eemaldamata, siis on oht igemehaiguste tekkeks    | IL: P�HJENDAMINE |
Annika:	hambakivi sisaldab suurel hulgal  baktereid, mille tagaj�rjeks on veritsevad ning valutavad igemed, paha maitse suus ning halb hinge�hk     | IL: P�HJENDAMINE |
Annika:	ise koduste vahenditega hambakivi �ldjuhul eemaldada ei �nnestu    | IL: P�HJENDAMINE |
Annika:	hambakivi eemaldamisel kasutatakse erinevaid meetodeid   | IL: P�HJENDAMINE | 
Sina:	selge   | VR: NEUTRAALNE PIIRITLEJA | 
Sina:	kas on mingeid vahendeid arstil,et seda edaspidi �ra hoida    | KYE: JUTUSTAV KAS |
Annika:	Dr. X number on <TEL_NR>   | YA: INFO ANDMINE | 
Sina:	mul seda ykskord juba tehtud   | IL: MUU |
      
