((82a6 teek�simine))
((�htlustas Andriela R��bis 10.05.2004))

B: jah | YA: MUU |
A: vabandage, | RY: KONTAKTEERUMINE |
B: jah | VR: NEUTRAALNE J�TKAJA |
A: kas te oskate �elda kuidas siit saab `bussi`jaama. | KYE: AVATUD |
B: ((k�hatab)) `bussi`jaama.    | KYJ: EDASIL�KKAMINE |

C: `maaliini,   | PPE: �MBERS�NASTAMINE |  | KYE: VASTUST PAKKUV |

B: `maa[liini,]   | PPE: �MBERS�NASTAMINE |  | KYE: VASTUST PAKKUV |

A: [`maa]liini.   | PPJ: L�BIVIIMINE |  | KYJ: JAH |

B: `maaliini bussi`jaama. | KYJ: EDASIL�KKAMINE | 
(0.8) `siit (.) l�hete n��d (1.2) `Raekoja `platsi. | KYJ: INFO ANDMINE |
 (1.0) siit l�hte `alla, (0.8) tuleb �ks `rist`t�nav. | KYJ: INFO ANDMINE |
 (.) sis l�hete (.) rae tuleb rae rae `raekoda.= | KYJ: INFO ANDMINE |
A: =jah | VR: NEUTRAALNE J�TKAJA |
B: ja sis l�hete `otse? | KYJ: INFO ANDMINE |
 (.) nii kau=kui tuleb (0.5) �ks: (.) suur magist`raal=tuleb. | KYJ: INFO ANDMINE |
 (.) `kaubamaja. | KYJ: INFO ANDMINE |
 (0.8) ja `kaubamaja `juurest p��rate `vasakule. | KYJ: INFO ANDMINE |
 (0.5) ja siis (.) j�e=`��res on | KYJ: INFO ANDMINE |
A: ahah | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(.)
B: Emaj�e [`��res.] | IL: T�PSUSTAMINE |
C: [sinna]`poole j��b `niivisi {---}    | KYJ: INFO ANDMINE |

B: umbes `niivisi (.) j��b=se `bussijaam.    | KYJ: INFO ANDMINE |

(.)
A: ahah | VR: NEUTRAALNE INFO OSUTAMINE UUEKS | 
(0.5) ait�h= | RIE: T�NAN |
B: =aga `muidu (0.5) kui l�hete `siit alla, | KYJ: INFO ANDMINE |
(0.5)
C: j�e ��rde `v�lja=ja [{---}]| KYJ: INFO ANDMINE |
B: [j�e ��rde `v�lja sis] ei ole `kuskile ei `eksi.= | KYJ: INFO ANDMINE |
C: =keerata. | KYJ: INFO ANDMINE |
B: voh= | YA: MUU |
A: =ja sis l�hen `otse. | PPE: �LEK�SIMINE |   | KYE: VASTUST PAKKUV |

B: [{---}] | YA: PRAAK | 
C: [ja kogu=aeg] (.) `m��da j�e=`��rt j�uategi    | PPJ: L�BIVIIMINE |   | KYJ: MUU |

B: jah | VR: MUU |
C: �le (.) `silla peate aint minema ja ongi [`bussijaam.]   | PPJ: L�BIVIIMINE |  | KYJ: MUU |

B: [ei=ei,] (.) silla `alt l�bi {minema}= | PA: PARTNERI PARANDUS |
C: =no silla `alt l�bi jah,    | PA: ENESEPARANDUS |
(.) ((taibates:)) vot `silla alt l�bi=jah.    | PA: ENESEPARANDUS |

B: tulge siit `paistab. | DIE: ETTEPANEK |
C: minge otse `alla ja sis ei `eksi `kuhugi. | IL: �LER�HUTAMINE |
B: siit ei olegi enam `kuhugi eksida.    | IL: �LER�HUTAMINE |
(...) ((k�nnivad)) siit l�hte alla j�e `��rt=pidi j�e ��rt=pidi l�hte `kogu=aeg. | IL: �LER�HUTAMINE | 
(1.0) ja siis tuleb (.) suur a- magistraal autode `sild tuleb, (.) sealt `alt l�hed `l�bi veel, (1.0) ja ja `ongi bussijaam.    | IL: �LER�HUTAMINE |

(0.5)
A: ja on sinna `pikk maa v�i.   | KYE: T�PSUSTAV |   | KYE: SULETUD KAS |

B: ((julgustavalt)) ei=`ole:.    | KYJ: EI |
(.) `pool kilumeetrit v�ib[olla.]    | IL: T�PSUSTAMINE |
vaat `siit pange otse alla j�e=��rde `v�lja. | IL: �LER�HUTAMINE |
A: [aa]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |

A: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) ait�h?   | RIE: T�NAN |

B: palun?    | RIJ: PALUN |

   

 
   

 

