((235, 9. telefonik�ne, klient - autokauplus))
((�htlustas Andriela R��bis 6.10.2004))

((kutsung))    | RIE: KUTSUNG |
V: {-}     | YA: PRAAK |
H: tervist.   | RIE: TERVITUS |
 (.) ee (.) ma `elistasin m�ni aeg `tagasi teil::=ee Renoo poolkel- `telgede asjus.   | RY: TUTVUSTUS |
 (0.5)
V: `t�na=v�.   | KYE: SULETUD KAS |
H: `ei    | KYJ: EI |
se oli: paar p�eva `tagasi=vist.       | IL: T�PSUSTAMINE |
(.) e on `v�imalik neid vel `tellida.         | KYE: JUTUSTAV KAS |
 (.)
V: on `ikka v�imalik `tellida:   | KYJ: JAH |
 on teil {-} kliendi`kaart.   | KYE: SULETUD KAS |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: kuidas?   | KYE: AVATUD |    | PPE: MITTEM�ISTMINE |
V: on teil meie kliendi`kaart.     | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |    | KYE: SULETUD KAS |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: ei kahjuks $ veel ei `ole. $   | KYJ: EI |
V: `siis tuleb tulla `kohale ja `tellida.   | KYJ: INFO ANDMINE |
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: siis tuleb pool `ette maksta:, sis `saab.   | KYJ: INFO ANDMINE |
H: a:hah.   | VR: NEUTRAALNE VASTUV�TUTEADE |
 ee oskate �elda ku `kalliks l�hevad `m�lemad pool`teljed Renoo: �he`teisk�mnele.   | KYE: AVATUD |
 (.)
V: e `inda ei �eldud teile tookord.   | KYE: JUTUSTAV KAS |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: ee mingi: e- ena- elementaarne `hind �eldi `parempoolsele    | KYJ: INFO ANDMINE |
aga oskate �elda mis=se `vasakpoolne maksma v�iks.   | KYE: AVATUD |
 `t�ism�ng ma=m�tlen.    | IL: T�PSUSTAMINE |
(1.2)
V: ma=vaatan?   | KYJ: EDASIL�KKAMINE |
 (43.0) ((on kosta jutuajamine kaupluses))
V: Re`noo oli `�ksteist=ja      | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: jah.   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
 (.)
V: mis aasta `auto ta `on.=   | KYE: AVATUD |    | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: =kaeksagend=`kolm.   | KYJ: INFO ANDMINE |
 (0.8) 
V: mis `mootoriga ta on.   | KYE: AVATUD |    | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: �he=koma=`�hesega.   | KYJ: INFO ANDMINE |
 (15.0)
V: tuhat seitsesada kaheksak�mend=kaheksa `krooni oli t�kk.    | KYJ: INFO ANDMINE |
(.)
H: ee `m�lemal.     | KYE: VASTUST PAKKUV |       | PPE: �MBERS�NASTAMINE |
V: jah.   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
 (.)
H: `selge.   | VR: NEUTRAALNE PIIRITLEJA |     | VR: PARANDUSE HINDAMINE |
 ja kui `t�na tellida kas: homsek- v�i=t�ndab `esmasp�evaks saaks `k�tte.      | KYE: JUTUSTAV KAS |
V: mm kui on `Tallinas sis < saab `homseks juba >   | KYJ: INFO ANDMINE |
H: aa.=   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: =aga kui ei `ole, sis sis sis=ta sis `esmasp�evaks kindlasti ei=`saa.   | KYJ: INFO ANDMINE |
H: ei=`saa jah.   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE |
V: ei=`saa,   | KYJ: N�USTUV EI |     | PPJ: L�BIVIIMINE |
 sest `laup�ev `p�hap�ev keegi t��d ei `tee Soomes et seda `saa[ta.      | IL: P�HJENDAMINE |     | PPJ: L�BIVIIMINE |
 siis saab vast `kolma]`p�evaks ilmselt.      | IL: T�PSUSTAMINE |     | PPJ: L�BIVIIMINE |
H: [aa, Soomest `tuuakse.]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |     | VR: PARANDUSE HINDAMINE |
H: ee `selge.   | VR: NEUTRAALNE PIIRITLEJA |     | VR: PARANDUSE HINDAMINE |
 no et `j�rgmise `n�dala jooksul `kindlasti sis.      | KYE: VASTUST PAKKUV |
V: jah jah seda `kindlasti.=      | KYJ: JAH |
H: =ee mis `kellani t�na `sobiks muidu=vel `tulla `tellima.   | KYE: AVATUD |
 (.) mis `kell se `viimane aeg `oleks.   | KYE: AVATUD |
V: * ee `vaatan `kohe, *    | KYJ: EDASIL�KKAMINE |
(4.0) �tlem=siin kuni kella `kaheni.   | KYJ: INFO ANDMINE |
H: kuni kella `kaheni.   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE |
V: jah.   | KYJ: JAH |    | PPJ: L�BIVIIMINE |
H: `s:elge.   | VR: NEUTRAALNE PIIRITLEJA |     | VR: PARANDUSE HINDAMINE |
 e kas mul on veel �ks k�simus kas neil on mingid erinevad modifikatsioonid `ka selles=suhtes=et `pikkuste erinevused v�i (.) [v�i `k�igukasti]   | KYE: JUTUSTAV KAS |
V: [{---}]   | YA: PRAAK |
H: KUIDAS   | KYE: AVATUD |    | PPE: MITTEM�ISTMINE |
V: paremal `vasakul pool=    | KYJ: INFO ANDMINE |    | PPJ: L�BIVIIMINE |    | KYE: ALTERNATIIV |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: =`m�lemal.    | KYJ: ALTERNATIIV: M�LEMAD |
(0.5) nagu [`m�lemad oleks l�bi.]    | KYJ: ALTERNATIIV: M�LEMAD |
V: [t�ndab (.) on] on `k�ll jah.   | KYJ: JAH |
H: ahaa   | VR: NEUTRAALNE VASTUV�TUTEADE |
 et peab `t�pselt `j�rgi vaatama selle k�igukasti: (.) `margi ja=sis v=ee v�i `aitab ainud `sellest et=ee mingisugune: t�ndab=      | KYE: JUTUSTAV KAS |
V: =ei, `s:ellist asja sin `k�ll n�d=ei=`ole.   | KYJ: EI |
 siin on    | KYJ: INFO ANDMINE |
(1.0) teen=ta uuesti `lahti,    | KYJ: EDASIL�KKAMINE |
(3.5) seal ei saa `muud olla lihtsalt.    | KYJ: INFO ANDMINE |
H: mhmh=   | VR: NEUTRAALNE J�TKAJA |
V: =kui=ta=on `kaheksak�mend `kolm aasta, sis tal ei=saa uuem- uuema au- mudeli oma olla seal `all, (.) sest=[et]    | KYJ: INFO ANDMINE |
(.)
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
V: `ei, siin ei `ole vari`anti.    | KYJ: INFO ANDMINE |
((veendunult))
H: ei `ole vari`anti.=   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE |
V: =ei.    | KYJ: N�USTUV EI |     | PPJ: L�BIVIIMINE |
(.) [te] ei pea `vaatama `midagi seal.   | IL: KOKKUV�TMINE |
H: [jah]     | VR: MUU |  | VR: PARANDUSE HINDAMINE |
H: `selge.   | VR: NEUTRAALNE PIIRITLEJA |
 aga on n�d v�imalik ka=iljem t�ndab `ringi vahetada `juhul ku nad s- `siiski ei [`sobi.]   | KYE: JUTUSTAV KAS |
V: [noo:] nad siisks: kindlasti `sobivad, kui=nad=ei=peaks mingil p�hjusel: `sobima,=   | KYJ: INFO ANDMINE |
H: =jah=   | VR: NEUTRAALNE J�TKAJA |
V: =siis: `saab nad ikkagi `tagasi anda aga siis on juba see (.) `siis on ju see juba teie teie `autoga on seal sees midagi `lahti, on [seda sis juba `�mber ehited (.) {tead}]   | KYJ: INFO ANDMINE |
H: [`selge.    | VR: NEUTRAALNE PIIRITLEJA |
s- (.) > t�hendab] ma m�tlen `seda et just regist- < re`gistris nad=ee `j(h)agasid `selle p�rast et ta=on nagu kolmek�mne=`kolme kilovatine=aga=   | YA: INFO ANDMINE |
V: =mhmh   | VR: NEUTRAALNE J�TKAJA |
H: k�ik `katalogid n�itasid nigu kolgend=`viis kilovatti=   | YA: INFO ANDMINE |
et v�ibola on mingisugused `erinevused seal.   | KYE: JUTUSTAV KAS |
V: ei seda ei `ole jah.   | KYJ: EI |
H: `selge.   | VR: NEUTRAALNE PIIRITLEJA |
 no suur ai`t�h=teile.   | RIE: T�NAN |
 > mis `kellani te muidu t�na `lahti olete.   | KYE: AVATUD |
 < viieni.      | KYE: VASTUST PAKKUV |
V: kella `kuueni.   | KYJ: INFO ANDMINE |
H: kuueni.   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) ja: asute `J�evee   | KYE: AVATUD |
V: neli.   | KYJ: INFO ANDMINE |
H: mt J�evee `neli.    | VR: NEUTRAALNE VASTUV�TUTEADE |
se=on: selle `v�rvikeskuse juures sis=v�.   | KYE: SULETUD KAS |
V: jah,    | KYJ: JAH |
samas `majas.      | IL: T�PSUSTAMINE |
H: selge.    | VR: NEUTRAALNE PIIRITLEJA |
suur ai`t�h=teile.   | RIE: T�NAN |
V: palun=   | RIJ: PALUN |
n�gemist   | RIE: H�VASTIJ�TT |
H: head=`p�eva   | RIJ: VASTUH�VASTIJ�TT |

   

 
   

 
