((392 reisib�roo))
((�hestas Andriela R��bis 8.03.2009))
((K ja H sisenevad ruumi))   
K: tervist.    | RIE: TERVITUS |
(8.5)
((tooli t�mbamine))  
K: tere    | RIE: TERVITUS |
(.) meid huvitaks (2.2) ((v�tab koti seljast)) `reisid.    | DIE: SOOV |
(.)
T: jah   | VR: NEUTRAALNE J�TKAJA |
(.)
K: �tleme p�hja`maadesse v�i: Eu`roopasse et=ee j�ulu`vaheajal minna.   | IL: T�PSUSTAMINE |
 et `mis=ee et mida `te `pakute.   | KYE: AVATUD |
(2.2)
((paberi t�mbamine))   
T: s=peaks olema siis mingi `Norra: `Rootsi `Taani `seda suunda (.) m�tlete=     | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
H: =`Soome on `ka v�i.        | KYE: JUTUSTAV KAS |
T: Soome.=       | KYE: VASTUST PAKKUV |      | PPE: �LEK�SIMINE |
H: =jah.   | KYJ: JAH |   | PPJ: L�BIVIIMINE |
T: ma arvan=et hakkavad varsti tulema nagu need `Soome e need `suusareisid.        | KYJ: INFO ANDMINE |
(0.5) midagi [sellist]      | YA: MUU |
H: [aga] kas nagu on niimodi=et `j�ulude ajal on need `hinnad nagu `kallimaks l�inud.        | KYE: JUTUSTAV KAS |
{-}   | YA: PRAAK |
T: `hinnad on �ltse `j�ulude `aastavahetuse=ajal k�ige `kallimad hinnad.       | KYJ: INFO ANDMINE |
(0.5) sest need on nagu `selle `asjaga et need nigunii m�iakse `maha?   | IL: P�HJENDAMINE |
siis v�etakse sealt nagu viimast.    | IL: P�HJENDAMINE |
(0.5) 
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: aga `hetkel nagu sellist < mingit pa`ketti > ma arvan et on mingi: mingi `Rovaniemi pakett midagi `sellist kokku pandud.    | KYJ: INFO ANDMINE |
(.) [{-}]   | YA: PRAAK |
H: [a=mis=se] a=mis=se `h�lmab siis.      | KYE: AVATUD |
(0.8)
T: see on mingi:=��=mm mingi `j�uluvanak�lastamise (.) * `selline asi. *   | KYJ: INFO ANDMINE |
K: mm   | VR: NEUTRAALNE J�TKAJA |
T: {ma arvan v�i} (.) `kui nad `on isegi `selle vel kokku pand aga=noh p�him�tselt mida v�iks `teha v�iks=� ((k�hatus eemalt)) (1.0) siis=on mingeid ho`telle v�i midagi `sellist otsida.    | KYJ: INFO ANDMINE |   | DIE: ETTEPANEK |
(.) aga meil on suhteliselt `kallid ho`tellid, (0.5) kellega me `koost��d `teeme.    | IL: T�PSUSTAMINE |
(0.5) mis=teid=nagu `t�psemalt=�� `huvi[tab] mis nagu      | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
K: [��]     | YA: MUU |
K: sooviksime �tleme (.) `kuue inimesega `minna,   | KYJ: INFO ANDMINE |        | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
T: mhmh   | VR: NEUTRAALNE J�TKAJA |
K: ja: et siis oleks ka `vabaaja sisustust mitte ainult � tu`rism et=siuke=noh (.) `meelelahutus `ka.   | KYJ: INFO ANDMINE |        | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
T: mhmh    | VR: NEUTRAALNE J�TKAJA |
(1.5)
K: et midagi `sellist.    | IL: KOKKUV�TMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.5)
T: kuskil `Soomes siis `p�hja [pool]       | KYE: VASTUST PAKKUV |   | PPE: �MBERS�NASTAMINE |
K: [��] (.) see=ei=ole �ltse `oluline v�ib=isegi=olla: kasv�i ka (0.5) �tleme: Keskeu`roopas.    | KYJ: MUU | | PPJ: L�BIVIIMINE |     
T: mhmh     | VR: NEUTRAALNE J�TKAJA |
K: v�i kuskil seal (.) j�ulu`vaheajal lihtsalt.    | KYJ: MUU | | PPJ: L�BIVIIMINE |      
T: mhmh (.) mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |      | VR: PARANDUSE HINDAMINE |
(1.0) peaks natuke seda `asja selles m�ttes `uurima=        | KYJ: EDASIL�KKAMINE |
et meil nagu mingit=e (1.2) `valmis pa`ketti teile nagu=nimodi `n�idata ei=`ole.=          | KYJ: INFO PUUDUMINE |
H: =mhmh=   | VR: NEUTRAALNE J�TKAJA |
T: =kuhu (.) kuhu v�iks `minna.          | IL: T�PSUSTAMINE |
(3.2)
H: lihtsalt hakkasime `uurima seda et �kki: `j�ulude ajal on (.) noh (.) t=�kki `on juba mingid asjad `koos v�i `tehakse et=sis=on suht k�hku vist (.) bro`neeritakse asjad `�ra (.) juba.    | IL: P�HJENDAMINE |
(2.0)
T: mhmh?    | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.5) see v�iks `olla sis mingi `kahek�mne (.) * mis ta akkab n�d.   | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
(.) `viiega v�. *      | KYE: VASTUST PAKKUV |   | PPE: �MBERS�NASTAMINE |
(0.5)
H: jah,       | KYJ: JAH |   | PPJ: L�BIVIIMINE |
ennem ennem `aastavahe- (.) nojah umbes.=   | IL: T�PSUSTAMINE |   | PPJ: L�BIVIIMINE |
T: =kakskend (0.5) kakskend`kolm hakkab `esmasp�eval.        | YA: INFO ANDMINE |
(1.5)
H: jah,     | VR: NEUTRAALNE VASTUV�TUTEADE |
nagu `selle aja sees v�iks `olla.=    | DIE: SOOV |
T: ={kuuep�evane}    | YA: MUU |
ma=�tlen meil on nagu `see=et meil on (1.0) kui ma sit hakkan `ise panema individu`aalpaketti `kokku mingit `lendu v�i midagi `sellist, (.) `teine variant et minna noh n�iteks `bussiga.     | DIJ: INFO ANDMINE |
(.) siis meil on `majutus suhteliselt `kallis.   | DIJ: INFO ANDMINE |
K: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: et nagu sellist `majutuse=osa saaks ise v�ibola `internetist teha.    | DIE: ETTEPANEK |
H: mhmh      | DIJ: PIIRATUD N�USTUMINE |
T: aga selle jaoks on vaja just seda: kre`diitkaardi `olemasolu mille `numbrit sinna sisse toksida.    | IL: T�PSUSTAMINE |
(2.5) `midagi v�iks `uurida    | IL: �LER�HUTAMINE |
(1.2) ((otsib arvutist))   
K: mm     | VR: MUU |
T: {---}   | YA: PRAAK |
 (...) ((hiirega klikkimine)) siin mingisugune `on nagu midagi need on sellised `suusaretked.      | DIJ: INFO ANDMINE |
(2.2)
K: mis `reisid teil nagu muidu (.) talvel (.) popu`laarsemad on siuksed (0.5) `noored inimesed �tleme (0.5) kuhu nad k�ige rohkem `l�hvad.   | KYE: AVATUD |
H: `Austriasse kas     | KYE: SULETUD KAS |
(.) [{-}]   | YA: PRAAK |
T: [`Austria] `on aga see on h�sti `kallis.      | KYJ: INFO ANDMINE |
H: see on `kallis jah.=       | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
T: =see on see `suusa oma.          | KYJ: MUU |      | PPJ: L�BIVIIMINE |
 (.) viisteist=tuhat `kaheksa`sada on `hind.            | KYJ: MUU |    | PPJ: L�BIVIIMINE |
`kaheses toas.   | IL: T�PSUSTAMINE |    | PPJ: L�BIVIIMINE |
(0.5)
H: aa   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |     | VR: PARANDUSE HINDAMINE |
K: ja kui (.) `kaua see `on sis mitme `p�evane=      | KYE: AVATUD |
T: =see on `seitse p�eva.    | KYJ: INFO ANDMINE |
{---}    | YA: PRAAK |
(...) ((hiirega klikkimine)) ja `see ei=ole j�uluperi`oodil.    | IL: T�PSUSTAMINE |
(3.2) siis on tehakse h�sti palju neid=ee `Tallinkiga `koost��s mis on need (0.8) � `Tahko ja `Kuopio ja sellised `suusareisid.       | KYJ: INFO ANDMINE |
need=praktiliselt on `k�ik nagu mingid sellised suusa`reisid.=      | KYJ: INFO ANDMINE |
H: =mhmh   | VR: NEUTRAALNE J�TKAJA |
T: v�i siis `lastele on t�esti see (.) `j�ulumaa (.) reis.          | KYJ: INFO ANDMINE |
(0.5) 
H: [{-}]   | YA: PRAAK |
T: [a sinna] v�ivad `suured ka minna, see ei=ole mingi selline=      | KYJ: INFO ANDMINE |
H: =a `suusareisid oleks    | KYE: AVATUD |
(0.5) 
T: a see on [nagu `veebruari] jah, sis=on �ks `Austria reis, `seitse ��d `koha=peal.    | KYJ: INFO ANDMINE |
H: [ka huvitavad]     | YA: MUU |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: aga nagu `enamasti need `talveasjad on k�ik `sellised `suusa`reisid.    | IL: KOKKUV�TMINE |
(1.0)
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (2.0) aga vist (.) v�ibla peale (0.5) `aastavahetust (.) `see periood �kki,   | SEE: ARVAMUS |
K: nojah   | SEJ: N�USTUMINE |
H: [see sobiks `ka.]   | DIE: SOOV |
T: [siis on `see] et on   | DIJ: INFO ANDMINE |
`koolivaheaeg kestab `kuuendani eksju,=     | KYE: VASTUST PAKKUV |
K: =jah      | KYJ: JAH |
H: jah       | KYJ: JAH |
T: sis need on nagu k�ik sellised h�sti `kallilt v�lja tulevad reisid.    | DIJ: INFO ANDMINE |
(1.5) < aga `kui v�tta n��d >    | DIJ: INFO ANDMINE |
(.) teid on `kuus jah       | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE |
K: mhmh=      | KYJ: JAH |   | PPJ: L�BIVIIMINE |
T: =ja �tleme=et kui v�tta n�iteks kui oleks `k�mme inimest saaks mingi `grupireisi teha on `Frankfurt pr�egu �sna `soodus pilet.    | DIJ: INFO ANDMINE |
(0.8)
H: aga samas    | YA: MUU |
(.) 
T: `bussihinnaga edasi=`tagasi.   | IL: T�PSUSTAMINE |
H: saab `inimesi �kki `juurde (.) `otsida.        | SEE: ARVAMUS |
(0.8)
K: jah   | SEJ: N�USTUMINE |
(1.2)
H: noh (.) jah    | YA: MUU |
(.) `pr�egu lihtsalt arvestatud `kuuega [kuid] kindlasti (.) saaks `gruppi nagu `suuremaks ajada.      | IL: �LER�HUTAMINE |
 
T: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(1.0)
T: no nagu need `Norra `k�ik on nagu mingid `suusa`asjad,      | DIJ: INFO ANDMINE |
(1.2) noh siin nad arvestavadki=et saaksite suusam�gedele `ligemale?   | DIJ: INFO ANDMINE |
ja siis sinna `m�gede=piletid ja asjad=      | DIJ: INFO ANDMINE |
et {need} kui=te=nagu n�d `suusahuvilised p�ris ei=`ole ((pastakaga toksimine)) siis `teile on see nagu `raiskamine,      | SEE: ARVAMUS |
K: jah   | SEJ: N�USTUMINE |
T: ma arvan.   | IL: �LER�HUTAMINE |
(1.8) ((toksimine)) aga muidu `pr�egu on selle   | DIJ: INFO ANDMINE |
(1.0) * kas ta n�itab. *   | DIJ: EDASIL�KKAMINE |
(...) ((klikkimine)) pr�egu on nagu `see et < `Frankfurd=h (.) `kaks=tuhat �heksada �heksakend=`viis, > (0.5) edasi=tagasi=`lennu`pilet       | DIJ: INFO ANDMINE |
see on nagu: �-�udsalt `h�sti nagu `saadud.    | IL: HINNANG |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
K: mhmh=   | VR: NEUTRAALNE J�TKAJA |
T: =seal `on=e (.) �tleme `bussiga mis ta n��d oli tuhat `kuussada kaheksa`k�mmend v�i `�ks suund.      | DIJ: INFO ANDMINE |
K: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: et `praktiliselt on ta `bussihinnaga aga see on `lennukiga edasi=tagasi aga alates `k�mnest `inimesest.       | DIJ: INFO ANDMINE |
(1.5)
K: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: seda ma nagu `v�iksema summaga ei=saa [(.)] `m�ia.       | DIJ: INFO ANDMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(1.5)
T: sin=l�b `lennujaamamaks veel `juurde see on < tsirka `seitse `sada `krooni, >      | DIJ: INFO ANDMINE |
 (1.0) �ks=etk korra `vaatan?      | DIJ: EDASIL�KKAMINE |
(...) ((klikkimine)) 
K: Frankfurdis on (.) `talvel (.) siuke `soe (.) kliima v�i (.) * ilmad v�i *=     | KYE: JUTUSTAV KAS |
T: =soojemad kui `meil,    | KYJ: INFO ANDMINE |
(2.8) n�d tuleb nagu `kohapeal (.) sis tuleb vaadata juurde need ho`tellide asjad mis [on nagu] suhteliselt `kallis Saksamaal p�ris [`kallis,]        | DIJ: INFO ANDMINE |
K: [mhmh]    | VR: NEUTRAALNE J�TKAJA |
[jah]      | VR: NEUTRAALNE J�TKAJA |
T: muidugi `teine variant on kui `saate noh          | DIE: ETTEPANEK |
(.) seda `mina ei=saa `teha.       | IL: P�HJENDAMINE |
(0.5) kui saate `ise vaadata sinna:=�� �li�pilas�his`elamutesse v�i sellis[tesse] kohtadesse     | DIE: ETTEPANEK |
selleprst=et seal on `ka siis (.) e kooli`vaheaeg.   | IL: P�HJENDAMINE |
H: [mm]      | DIJ: PIIRATUD N�USTUMINE |
H: mhmh        | DIJ: PIIRATUD N�USTUMINE |
T: sis � nagu `netist vaadata neid (.) {kodukaid} �ks on nagu (.) ve=ve=vee `hostels (.) `komm       | YA: INFO ANDMINE |
H: ot saate `�les (.) [kirjutada.]   | KYE: SULETUD KAS |
T: [{---}]   | YA: PRAAK |
(...) ((paberi sahin))  
T: {---}   | YA: PRAAK |
(...)
H: aga `Rootsi on `ikka ned ka mingid `soodusreisid sis.      | KYE: JUTUSTAV KAS |
(2.2)
T: < ee `Rootsi v�ib olla selle > (.) mitte p�ris `sellel ajal aga �tleme=et (0.5) `eelmine=aasta `enne j�ule ja `peale aastavahetust v�ivad olla need kru`iisid {-}   | KYJ: INFO ANDMINE |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: soodsa hinnaga �kki.    | KYJ: INFO ANDMINE |
`pr�egu ei oska nagu `�telda, mis see hind v�ib olla   | KYJ: INFO PUUDUMINE |
 eelmine aasta oli nimodi=et (1.0) et ee �ks `inimene sis `seitse`sada.    | KYJ: INFO ANDMINE |
(.) kas bee v�i `tsee [kajutis.]   | IL: T�PSUSTAMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(1.0)
T: aga=see on kru`iis praktiliselt `p�eval on seal ja [�htu] `tagasi.   | IL: T�PSUSTAMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
K: jah   | VR: NEUTRAALNE J�TKAJA |
(1.2)
T: < no `praegu ei oska nagu >   | IL: �LER�HUTAMINE |
H: a a millal need [`tulevad sis.]   | KYE: AVATUD |
T: [aga kas] `Peterburi ei taha minna.   | KYE: SULETUD KAS |
(1.5)
H: [{---}]   | YA: PRAAK |
T: [{---}] `odavam      | IL: P�HJENDAMINE |
 ta on �udselt `uvitav    | IL: HINNANG |
ma eelmine aasta just `selle: ajavahemikus `k�isin {-}      | IL: P�HJENDAMINE |
(.)
K: jah   | VR: NEUTRAALNE J�TKAJA |
T: �sti `ilus.      | IL: HINNANG |
(.) ja seal on t�eliselt `palju nagu teha selles=m�ts=et `Peterburi on nagu `selline koht kus on seda kul`tuuriasja nagu on, (.) ja `samas on seal `�htul ka �udselt palju `teha.    | IL: P�HJENDAMINE |
(1.8)
K: < ahah >   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.5)
H: huvitav ju.   | SEE: V�IDE |
K: see oleks ka `huvitav muidugi.   | SEJ: N�USTUMINE |     | SEE: V�IDE |
(0.5)
T: ta on `omamoodi.    | SEJ: N�USTUMINE |    | SEE: V�IDE |
�tleme ta ei=`ole mingisugune noh (0.5) ta=i=ole nagu ma=i=`tea, `v�lismaa $ eksju `selles m�ttes. $   | IL: P�HJENDAMINE |
K: {---}   | YA: PRAAK |
T: aga ta on (.) seal on `see asi=et `viisa tuleb sis nagu: varakult `korda ajada.    | IL: T�PSUSTAMINE |
(0.5) kuna viisa `aeglasem variant v�tab `kaksk�mend `t��p�eva aega mis t�hendab praktiliselt t��p�evad on esmasp�evast e `reedeni.    | IL: P�HJENDAMINE |
(0.5) kuu=`aega eksju.=   | IL: �LER�HUTAMINE |
H: =mm   | VR: NEUTRAALNE J�TKAJA |
(1.0)
T: ja siis e (1.0) siis nagu: `majutust on `ka Peterburiga nimodi=et seal on nagu `erinevaid=ee vari`ante.      | IL: T�PSUSTAMINE |
(0.8) nad=on `suhteliselt `kallid.    | IL: HINNANG |
(1.2) aga=oleneb=j�le kuhu `satud no meil on ka nagu erinevaid kon`takte=aga (1.8) `Venemaa puhul kehtib `see seadus et v�ibolla (1.0) neid `odavaid asju nagu reisib�rood �ltse=i=`v�tagi kuna me ei saa neid `usaldada kui [`partnerit] enam.   | IL: T�PSUSTAMINE |
H: [* mhmh *]   | VR: NEUTRAALNE J�TKAJA |
T: et see tuleb ka nagu v�ibolla (.) [`omal uurida.]   | IL: KOKKUV�TMINE |
H: [`ise uurida.]     | KYE: VASTUST PAKKUV |   | PPE: �MBERS�NASTAMINE |
T: jah        | KYJ: JAH |   | PPJ: L�BIVIIMINE |
(0.5) sest Peterburis iseenesest on `palju vaadata ja seal on `talvel on ilus ja `suvel on `ka on ilus=ja (.) seda ma nagu `julgen soovitada.      | IL: KOKKUV�TMINE |
aga muidugi v�ib vaadata seda Eu`roopa poolt `ka rohkem.        | DIE: PAKKUMINE |
(1.2)
K: jah         | DIJ: N�USTUMINE |
(.) et=sis=mitte=aint `suured linnad.      | KYE: VASTUST PAKKUV |
H: jah       | KYJ: JAH |
(0.5) aga millal need kru`iisiasjad nagu `selguvad.   | KYE: AVATUD |
 {-} [{-}]   | YA: PRAAK |
T: [`selguvad] `alles det`sembris.   | KYJ: INFO ANDMINE |
H: sis=nad=ju: suht `k�hk[u:] (.) broneeritakse [`�ra.]      | KYE: VASTUST PAKKUV |
T: [jah]        | KYJ: JAH |
 [jah]     | KYJ: JAH |
(1.2)
T: pakun=et alles det`sembris kuna nad siis hakkavad `vaatama oma (.) `kohtade olemasolu.   | KYJ: INFO ANDMINE |
ja tavaliselt on ka niimodi=et ta nagu (0.8) ei=`j�� nagu selleks `magusaks perioodiks kus=ee rikkad inimesed l�hvad nagu `kingi[tusi] $ ostma eks. $         | KYJ: INFO ANDMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
H: jah     | VR: NEUTRAALNE J�TKAJA |
T: et ta `j��b nagu `selleks perioodiks kus �tleme `koolivaheaeg on `ka juba l�bi et mitte `keegi enam midagi ei=tee ja k�igil on rahad `otsas.       | KYJ: INFO ANDMINE |
noh mingi [�tleme] peale mingi (.) `kuuendat `jaanuari v�ibolla.      | KYJ: INFO ANDMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(0.8)
T: s=kui pole nagu `kellegil enam v�imalik [kuskile] $ `minna. $      | KYJ: INFO ANDMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
H: mhemhe    | YA: MUU |
$ {-} $   | YA: PRAAK |
(0.5)
T: `pr�egu on kruiisid �sti `soodsad pr�egu=vel.    | YA: INFO ANDMINE |
(.) `kahene (.) ee nagu `bee kaks kajut on tuhat=`kaheksasada.   | IL: T�PSUSTAMINE |
(.)
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: see on nagu see ilma (.) `aknata k�ll aga �levalpool seda auto`tekki.      | IL: SELETAMINE |
(1.0) no see on nagu su- se=on suhteliselt `soodne kruiisi kohta st=et `muidu on see bee kaks nor`maalind on kolm=tuhat.        | IL: T�PSUSTAMINE |
 (.) edasi=tagasi.     | IL: T�PSUSTAMINE |
(.) ja seal on isegi `ommikus��k sees tuhande kaheksasajasel.    | IL: T�PSUSTAMINE |
(0.5) aga see vist kehtib    | IL: T�PSUSTAMINE |
Kadri kas ok`toobri `l�puni on sooduskruiis=v�.    | KYE: SULETUD KAS |
((k�simus kolleegile))   
C: jah   | KYJ: JAH |
T: oktoobri `l�puni.         | IL: T�PSUSTAMINE |
(1.0) et (2.2) ei=`tea, `midagi v�iks nagu `vaadata aga samas mulle (.) noh, ma pr�egu ma=i=tea mitte mingit `soodsat `hinda ei tule.    | IL: KOKKUV�TMINE |
v�ime koos vaadata mingeid ho`telle v�i midagi sellist.   | DIE: ETTEPANEK |
H: * mm *       | DIJ: PIIRATUD N�USTUMINE | 
(.) sest noh, lihtsalt akkasime juba `uurima=et=   | IL: P�HJENDAMINE |
T: =mhmh   | VR: NEUTRAALNE J�TKAJA |
(0.5)
H: no mis `v�imalused on [sest] (.) siis on selline `aeg kui k�igile (.) `sobib.    | IL: P�HJENDAMINE |
T: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: teine variant on `see et kus nagu ka k�iakse on need Slo`vakkiad ja `need asjad.      | KYJ: INFO ANDMINE |
(0.8) mingite `oma v�ikeste `bussidega.   | KYJ: INFO ANDMINE |
(0.5) sinna.       | KYJ: INFO ANDMINE |
(.) sest=et `sinna j�lle nagu `liinibussiga rongiga l�b j�lle: {-} nagu (.) `kalliks.    | IL: P�HJENDAMINE |
see=l�b nii mitmest kohast `l�bi nii [palju] `peatub lihtsalt see on ebamugav.   | IL: P�HJENDAMINE |
K: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.5)
T: aga kui mingit=ee sellist mikrobussivarianti saaks `see on nagu selles m�ttes hea koht et ta on `odav.    | IL: HINNANG |    
(1.0) 
K: [jah]       | VR: NEUTRAALNE J�TKAJA |
T: [kannatab] `maksta t�iesti normaalselt ja `�sti `korralik on nagu.   | IL: T�PSUSTAMINE |
(0.5)
K: Slovakkia.   | KYE: VASTUST PAKKUV |  | PPE: �LEK�SIMINE |     
T: Slovakkia.       | KYJ: JAH |   | PPJ: L�BIVIIMINE |     
H: mhmh     | VR: NEUTRAALNE J�TKAJA |  | VR: PARANDUSE HINDAMINE |
(1.0)
T: aga see on `ka muidugi `talvepoole kisub j�lle selleks `suusaasjaks `�ra v�ibolla=aga=noh       | SEE: ARVAMUS |
K: nojah   | SEJ: N�USTUMINE |
(2.5)
T: ja=noh=v�ibolla `talvel nagu:: (.) noh=ma=i=tea palju seal neid `k�etavaid `k�mpinguid ja selliseid asju n�iteks `on.    | IL: MUU |
s=l�b `sellega ka nagu `kallimaks.   | IL: HINNANG |
(2.0)
H: aga `Ungari n�iteks (.) {-} oleks   | KYE: AVATUD |
T: Ungari ma `arvan=et kellel v�ib n�iteks `vaatame pr�egu nende (1.0) `bussireiside {-} `kodulehek�lge.      | KYJ: EDASIL�KKAMINE |
 (...) ((klikkimine)) {te} muidu `kohvis�brad `olete.    | KYE: SULETUD KAS |
(.) ah, kohvi `joote.       | KYE: SULETUD KAS |
(.)
H: $ ei=soovi. $      | KYJ: EI |
K: ei.      | KYJ: EI |
H: $ ait�h. $ =   | RIE: T�NAN |
T: = $ ei=soovi. $    | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE |      
K: $ ei=soovi. $     | KYJ: N�USTUV EI |   | PPJ: L�BIVIIMINE |     
T: {-} t�ndab (.) vaatan n�iteks siin on need (1.5) `Germalo ja `Tensi kes teevad.     | KYJ: INFO ANDMINE |
 (.) `veinituur k�ll oli `Ungari oli=ja: `s�gisene.     | KYJ: INFO ANDMINE |
(...) ((klikkimine)) 
K: et reisid on muidu (1.5) k�ikidel kooli`vaheaegadel on reisid kallid.     | KYE: VASTUST PAKKUV |
(1.0)
T: `ala`ti.    | KYJ: JAH |
`k�ik mis on koolivaheaeg=ned `nii `m�rtsi oma, (.) kui n��d `see, mis `aasta l�pul on=      | IL: �LER�HUTAMINE |
K: =mhmh   | VR: NEUTRAALNE J�TKAJA |
T: on alati `kallimad kui (0.5) `muudel kuup�evadel.    | IL: �LER�HUTAMINE |
(...) ((klikkimine)) k�ik `suvised `reisid on (.) {-} (4.0) siin n�itab `s�gisel (...) ((klikkimine)) noh siin on `Lapimaale k�lla t�elisele j�uluvanale see=on=nagu `k�ik.         | KYJ: INFO ANDMINE |
(...) ((klikkimine))   
H: mis seal `Soomes muidu n�iteks noh `suusakeskustes (0.5) mis ned innad `tuleksid.     | KYE: AVATUD |
(1.0)
T: ee see oleneb n��d (0.8) t�pselt seal on nende `m�kkide j�rgi.    | KYJ: INFO ANDMINE |
(.) et kui on n�iteks kuni `kaheksa inimest, seal v�etakse `iga `asi v�etakse individu`aalselt eraldi.      | KYJ: INFO ANDMINE |
 �ldiselt see on nimodi: et kas `neljapp�evast `neljapp�evani (0.8) on need (1.0) `sealolemise ajad nagu,      | KYJ: INFO ANDMINE |
K: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: ja: (1.0) v�iks v�tta mingi `eelmise aasta `kataloogi.       | KYJ: EDASIL�KKAMINE |
see on (...) ((klikkimine)) ei=`m�leta (1.0) seal oli `ka nagu `vahepealt oli suhteliselt `soodsaid asju aga ma=i=`m�leta pr�egu neid `hindu.    | IL: P�HJENDAMINE |
{---} (...) v�tan korra need `�pilas`reisid.    | KYJ: EDASIL�KKAMINE |
(...) ((klikkimine)) {-} Leedu `Poola on �ks variant see on `kolm p�eva tuhat `kaheksak�mend `viis.    | KYJ: INFO ANDMINE |
(...) ((klikkimine)) Ungari? `kaks=tuhat `�kssada `viis.    | KYJ: INFO ANDMINE |
(...) nende bussi`reisidega on natukene `see halb asi et (1.0) kui `gruppi ei=saa `kokku sis j��b (.) k�ik `�ra.   | IL: T�PSUSTAMINE |
H: mm   | VR: NEUTRAALNE J�TKAJA |
K: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: et noh (1.5) oleneb muidugi sellest mis neil see `miinimum inimeste `arv on.       | IL: T�PSUSTAMINE |
(4.8) `Viin on muidugi t�iesti `uvitav.    | KYJ: INFO ANDMINE |
(1.5) ta=ei=anna mulle pr�egu `kuup�eva.   | KYJ: INFO PUUDUMINE |
 (1.2) Viin `kaks=tuhat viissada=`�heksa.   | KYJ: INFO ANDMINE |
 {---} Slo`vakkia kaks=tuhat kakskend=`viis.   | KYJ: INFO ANDMINE |
(...) ((klikkimine))    
K: aga `Viin on muidu (.) suhteliselt `kallis ikkagi v�rdlemisi.         | KYE: VASTUST PAKKUV |
T: `Viin on `kohapeal kallis.      | KYJ: JAH |
 (...) ((klikkimine)) Soome Rootsi `Norra.   | KYJ: INFO ANDMINE |
 (2.5) p�ris uvitav.    | IL: HINNANG |
(0.8) aga ma tahaks `kuup�eva n�ha.    | KYJ: INFO PUUDUMINE |
(...) ((T helistab:)) tere mina olen Heli Morna `Iksist.    | YA: PRAAK |
(.) a mul on selline `k�simus=et ma l�hen teie kodulehek�ljele vaatan {teie} `reise?   | YA: PRAAK |
 Soome Rootsi Norra `�pilastele.    | YA: PRAAK |
`kuup�eva nagu ei=`ole.   | YA: PRAAK |
 kas see on `talvel {-}    | YA: PRAAK |
(...) alates `m�rtsist.    | YA: PRAAK |
aga `mis oleks nagu mingit sellist=ee (.) nisuke Eu`roopa `tuur kuskil (0.8) `koolivaheaja paiku `j�ulude ajal.    | YA: PRAAK |
{---} (...) mhmh   | YA: PRAAK |
 (...) ei=no `�ltse mida `mida nagu sellel ajal `pakkuda on.   | YA: PRAAK |
 (...) mhmh   | YA: PRAAK |
(...) mhmh   | YA: PRAAK |
(...) mhmh    | YA: PRAAK |
(...) {---} aga �ks`k�ik nagu {mingist niust} Eu`roopasse kas oleks v�imalik `vaadata.    | YA: PRAAK |
(0.5) {sest} nagu mul ei=ole nagu `grupp et noh nagu:: (0.5) {-} et tahaks nagu nad kuskile `l�litada `sisse.    | YA: PRAAK |
(.) sest=et=ku=ma akkan `ise v�tma ho`telle ja asju see l�heb nagu nii `kalliks.    | YA: PRAAK |
{---} (...) ei=ole jah.    | YA: PRAAK |
mhmh   | YA: PRAAK |
(...) mhmh   | YA: PRAAK |
(...) ei=ole.    | YA: PRAAK |
mhmh   | YA: PRAAK |
(...) mhmh   | YA: PRAAK |
(...) mhmh    | YA: PRAAK |
(...) aga i`dee j�rgi kui nagu `tuleks keegi kas saaks nagu `juurde l�litada v�i    | YA: PRAAK |
(...) mhmh   | YA: PRAAK |
(...) mhmh   | YA: PRAAK |
(...) mhmh   | YA: PRAAK |
(...) mhmh   | YA: PRAAK |
(...) mhmh   | YA: PRAAK |
 (...) {-} kena.    | YA: PRAAK |
(...) ait�h=teile.   | YA: PRAAK |
 n�ge`mist. ((paneb telefonitoru �ra))   | YA: PRAAK |
 (...) see on nimodi=et=emm praktiliselt `m�rtsis akkavad k�ik ned [reisid] peale.   | KYJ: INFO ANDMINE |
H: [aa.]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: et kui `on keegi kes tellib nagu `grupi: ja see grupp on `n�us siis on v�imalik nendega nagu `kaasa l�litada ennast.       | KYJ: INFO ANDMINE |
(0.8) nii=et l�heksite nagu `juurde sinna.   | IL: SELETAMINE |
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
K: jah   | VR: NEUTRAALNE J�TKAJA |
(1.0)
T: aga=jah need on nagu j�rgmise aasta: `kavad sellep�rast ei=ole=sin (1.0) ee `juures midagi et=�ldselt aastavahetusel nagu `v�he v�etakse=aga    | IL: P�HJENDAMINE |
(1.0) * vaatan korra mis `siin on. *    | KYJ: EDASIL�KKAMINE |
(1.0) `Norra muidu `iseenesest on (1.0) p�ris kallis,    | KYJ: INFO ANDMINE |
(...) aga see on `juulikuu kava siin, sellest pole nagu suurt `kasu.    | IL: T�PSUSTAMINE |
(0.5) {-} `Taisto    | YA: PRAAK |
(...) ((klikkimine))  
H: `p�hiline on sis `see=et nagu det`sembri alguses tegelikult `selguvad need (.) p�hiasjad v�i.     | KYE: SULETUD KAS |
T: ma arvan pisut `varem.      | KYJ: MUU |
(0.5) aga `siiski jah.   | KYJ: JAH |
 (2.0) * siiski: *      | YA: MUU |
H: no`vembris.     | KYE: VASTUST PAKKUV |  | PPE: �MBERS�NASTAMINE |    
T: {---} kooli`vaheajal vaatame?        | YA: PRAAK |
(...) ((klikkimine))  
H: {---}   | YA: PRAAK |
(1.0)
T: peaks `Taistol midagi `uurima.      | KYJ: INFO ANDMINE |
(2.2) siin on nagu `k�mme pluss `�ks.      | KYJ: INFO ANDMINE |
(...) ((klikkimine)) {-} `Peterburi on siin olemas, (0.5) kaks `p�eva {-}      | KYJ: INFO ANDMINE |
(0.5) aa,    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
nad r��givad=sin esimesest kooli`vaheajast alles.      | IL: T�PSUSTAMINE |
> kakskend=viis kuni kakskend=seitse ok`toober. <   | IL: T�PSUSTAMINE |
H: mm   | VR: NEUTRAALNE J�TKAJA |
T: et `natuke `peab siis j�relikult (.) `selliste reisidega `ootama.     | IL: J�RELDAMINE |
aga meil on nagu v�imalik teha k�ll see et me teeme teile `lennupiletid ja `need asjad aga see l�heb lihtsalt niiv�rd `kalliks.=      | IL: T�PSUSTAMINE |
H: =mm   | VR: NEUTRAALNE J�TKAJA |
(0.8)
T: et=ee (.) kui on `alla `k�mne inimese ma=i=saa `grupihinda k�sida ka.=   | IL: P�HJENDAMINE |   
H: =�h�h     | VR: NEUTRAALNE J�TKAJA |
(2.5)
T: et sellel nagu ei=ole s�gavat (.) m�tet.=   | IL: �LER�HUTAMINE |
H. =mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.0) et=no- no`vembris sis nagu `uuesti uurida=v�i      | KYE: SULETUD KAS |
T: see on `t�esti niimodi [et see] {-} nagu rohkem   | KYJ: JAH |
v�i `teine variant ma v�iksin `v�tta nagu telefoni`numbri.      | DIE: PAKKUMINE |
H: [{mhmh}]       | VR: NEUTRAALNE J�TKAJA |
(.) 
T: kui midagi enne `selgub ma: noh v�in ju `k�sida eksole kas [te] olete nagu `uvitatud v�i (.) r��giks nagu `asjast=et   | IL: P�HJENDAMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(2.0)
T: kes: kes on kontakt`isik.=   | KYE: AVATUD |
H: =mm `mina.   | KYJ: INFO ANDMINE |
T: panen sis `kirja.      | DIE: PAKKUMINE |     | DIE: SOOV |
H: mhmh?    | DIJ: N�USTUMINE |
(2.0) ja: kolm seitse viis neli seitse.   | DIJ: INFO ANDMINE |
(0.5)
T: kolm seitse?     | KYE: AVATUD |     | PPE: MITTEM�ISTMINE |
H: viis? (.) neli seitse.    | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE |     
T: mhmh?    | VR: NEUTRAALNE VASTUV�TUTEADE |     | VR: PARANDUSE HINDAMINE |
(...) v�iks sis olla mingi selline: mingi kul`tuuri programm `sees eksju.    | KYE: VASTUST PAKKUV | | PPE: �MBERS�NASTAMINE |     
H: mhmh   | KYJ: JAH |  | PPJ: L�BIVIIMINE |    
K: {jah}    | KYJ: JAH | | PPJ: L�BIVIIMINE |     
T: ja nagu `suusaretki ei=ole `m�tet vist.     | KYE: VASTUST PAKKUV || PPE: �MBERS�NASTAMINE |     
(0.8)
K: mhmh    | KYJ: JAH |   | PPJ: L�BIVIIMINE |
H: jah, vist mitte eriti.     | KYJ: N�USTUV EI |  | PPJ: L�BIVIIMINE |
K: {suuska jah.}   | YA: MUU |
T: mitu `p�eva umbes,    | KYE: AVATUD |
mingi `n�dal   | KYE: VASTUST PAKKUV |
H: viis p�eva.   | KYJ: INFO ANDMINE |
(0.8)
K: noh, (.) v�ib=isegi `v�hem natuke olla.    | KYJ: INFO ANDMINE |     | SEE: ARVAMUS |
(0.5) ma arvan.      | SEE: ARVAMUS |
H: mhmh (.) nojah   | SEJ: PIIRATUD N�USTUMINE |
(1.5)
K: `n�dal oleks ikka `maksimum k�ige rohkem.=   | IL: T�PSUSTAMINE |     | SEE: V�IDE |
T: =mhmh    | VR: NEUTRAALNE J�TKAJA |
(4.5) 
H: mnjah      | SEJ: N�USTUMINE |
T: sest=et `pr�egu tundub ja �sna: �sna `vaikne olevat sin.   | IL: P�HJENDAMINE |
(2.2)
H: ((k�hatab)) (1.0) ma lihtsalt m�tlesin=et �kki tuleb nagu=noh varakult `uurima [hakata] neid asju {[seal] on nagu �ks suur grupp te[hakse]} kui kui (.) kui `k�hku ned `�ra broneeritakse.   | IL: P�HJENDAMINE |
T: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
 [mhmh]    | VR: NEUTRAALNE J�TKAJA |
[mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: v�i `teine variant=et kui te saate omal rahvast `juurde siin `Taisto �tleb et nad panevad grupi `kokku kui t�hend=nad panevad asja kokku kui on k�mme `inimest.   | YA: INFO ANDMINE |
K: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: seda=na- see on ka nagu �ks `m�tlemis[aine] v�ibolla.   | IL: �LER�HUTAMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(2.0)
H: {---}   | YA: PRAAK |
T: jah, ma `loodan=et $ nad toimivad. $        | IL: MUU |
 t�hendab see `faind hostels toimib `kindlasti.   | IL: T�PSUSTAMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: [sis] �ks on veel `hostels aga seal v�ib ka `igasugu indu olla.       | IL: MUU |
(4.5) ja=sis=on=vel=see `visit=dsh��mani on neid v�ib t�pselt samamoodi panna visit=`austria v�i ned=on=ned ee      | IL: MUU |
K: ahah   | VR: NEUTRAALNE J�TKAJA |
T: tu`rismiinfo`keskuste: (.) kodu`lehek�ljed.      | IL: MUU |
(0.5)
K: ahah=   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
H: =mhmh   | VR: NEUTRAALNE J�TKAJA |
T: `neid v�ib vaadata.   | IL: �LER�HUTAMINE |
(1.0)
H: ota need olid=sis sellisel juhul kui need `Frankfurdi=ja      | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
T: jah,    | KYJ: JAH |    | PPJ: L�BIVIIMINE |
kust leiaks nagu `odavamaid v�i `soodsamaid majutusvariante.   | IL: T�PSUSTAMINE |     | PPJ: L�BIVIIMINE |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |    | VR: PARANDUSE HINDAMINE |
K: mhmh   | VR: NEUTRAALNE J�TKAJA |    | VR: PARANDUSE HINDAMINE |
T: see mis on see `faindhostels sealt nagu peaks `p�ris `palju olema.       | IL: T�PSUSTAMINE |
(0.5) seal on isegi `Inglismaale nagu (.) e mis on `�sti kallis `majutuse poolest on isegi `Haid parki ligidale t�itsa normaalse hinnaga `asju saada.   | IL: T�PSUSTAMINE |
 (0.5) et noh ta ei=ole muidugi teab mis `tase v�ibolla kuue[kesi] `toas aga noh samas (.) tahad `minna miks `mitte eksju.   | IL: HINNANG |     | SEE: V�IDE |
K: [nojah]       | VR: HINNANGULINE J�TKAJA |
K: jah, no=muidugi.=   | SEJ: N�USTUMINE |
H: = {-} pole see nii `oluline.   | SEJ: N�USTUMINE |    | SEE: V�IDE |
T: jah, just `t�pselt    | SEJ: N�USTUMINE |
t�hendab kui `ongi nagu    | IL: T�PSUSTAMINE |
(0.8) et seda {varianti} v�ib `uurida.   | IL: KOKKUV�TMINE |
 (0.8) ja=sis v�ib n�iteks kui te leiate mingi soodsa majutuse me v�ime vaadata kuidas l�heks `piletitega.       | DIE: PAKKUMINE |
(1.0) et noh kas sis v�tame `bussiga=v�i v�i mis parasjagu mis `pakutakse.   | IL: T�PSUSTAMINE |
(.)
K: mhmh      | DIJ: PIIRATUD N�USTUMINE |
T: et �ks variant on `see=et (1.5) noh `Moskva on ju ka tegelikult `suur=   | YA: INFO ANDMINE |
aga: aga ta on v�ibola j�lle `liiga suur et sinna mi[da]gi: noh, akata organiseerima v�i `seal nagu on nimodi=et seal ei=`s�ida v�ibola �hest kohast teise nii `julgelt kui: kui v�ibla `mujale.         | SEE: ARVAMUS |
K: [jah]      | SEJ: N�USTUMINE |
H: mhmh         | SEJ: N�USTUMINE |
K: jah      | SEJ: N�USTUMINE |
(0.5)
T: v�i teil on v�tta mingi: `rongireis eksju keegi pole ju `rongiga ka sada aastat `s�it[nud] aga noh `ikkagi ta on nagu selline `teistmoodi.   | YA: INFO ANDMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(0.5)
T: aga `Venemaa on muidu `tore aga seal on just `see et �tleme hea=k�ll et kui teil `aega on et sis noh `viisa {n�d} tuleb `lisa`kulutus teha.    | YA: INFO ANDMINE |
(0.8) see on kuskil vaatame `hinnad on {---} (...) ja nendel on ka nagu `see=et neil `see eriti ei=`loe kas on (0.5) kas on `�ks v�i `kuus viisat et k�ik l�heb (1.0) ee (1.0) `kiirviisat on ka v�imalus teha aga teil ei=ole m�tet selle peale `raisata.    | IL: T�PSUSTAMINE |
(...) a Venemaal on v�hemalt `see asi et seal ei ole seda probleemi et `viisat ei=`saa.    | IL: T�PSUSTAMINE |
{-}   | YA: PRAAK |
K: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: viisad `saavad nagu k�ik.    | IL: �LER�HUTAMINE |
(...) nelisada=seitsek�mend `krooni on viisa.   | IL: T�PSUSTAMINE |
(0.5)
K: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: see viisa (.) [see on] sis kuni seitse `p�eva.    | IL: T�PSUSTAMINE |
(1.0) kohal olla.   | IL: T�PSUSTAMINE |
K: [{mhmh}]   | VR: NEUTRAALNE J�TKAJA |
(1.2)
K: no=sis=see=on=nagu (1.0) mitme=`inimesele.      | KYE: AVATUD |
T: �ks �-`�he=inimese viisa.       | KYJ: INFO ANDMINE |
(2.2) $ p�ris `palju eksju. $      | SEE: V�IDE |
mheh   | IL: PEHMENDAMINE |
K: mhmh   | SEJ: PIIRATUD N�USTUMINE |
H: mhmh   | SEJ: PIIRATUD N�USTUMINE |
(2.5)
K: no `m�tleme selle=peale.     | RIE: L�PUSIGNAAL |
H: [{mhmh}]         | VR: MUU |
T: [m�telge.]    | RIJ: L�PETAMISE VASTUV�TMINE |
ma m�tlen `ka.   | YA: MUU |
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.5)
T: ses m�ttes=et (.) noh neid ho`telle ja asju ja neid `pr�eguse hetke lende neid me saame nagu `igal ajal vaada[ta] aga [sellel] ei=`ole m�tet nagu.       | SEE: ARVAMUS |
(0.5) ma arvan.      | SEE: ARVAMUS |
H: [mhmh]      | VR: NEUTRAALNE J�TKAJA |
K: [jah]   | VR: NEUTRAALNE J�TKAJA |
K: {* mhmh *}         | SEJ: PIIRATUD N�USTUMINE |
(0.5)
H: mhmh      | VR: MUU |
(.) `vaatame.   | RIE: L�PUSIGNAAL |
T: palun     | RIJ: L�PETAMISE VASTUV�TMINE | | RIE: PALUN |
H: ait�h=teile   | RIJ: T�NAN |
T: n�gemist   | RIE: H�VASTIJ�TT |
H: n�ge`mist.   | RIJ: VASTUH�VASTIJ�TT |
K: {-} (.) head=p�eva   | RIJ: VASTUH�VASTIJ�TT |