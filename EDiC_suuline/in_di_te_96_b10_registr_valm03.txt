((96b10 telefonik�ne perearstikeskusesse))
((�htlustas Andriela R��bis 20.10.2003, VTE, VTJ parandatud 6.08.2006))

((kutsung))   | RIE: KUTSUNG |
V: M�nniku perearstikeskus,    | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |

`Maris kuuleb.   | RY: TUTVUSTUS |
H: mt tere    | RIE: TERVITUS |
ma soovik[sin:]e (.) `aega doktor `Nublu juurde.    | DIE: SOOV |
V: [tere?]   | RIJ: VASTUTERVITUS |

(0.8)
V: `m:is p�evaks te `soovite.    | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
H: �� kas `t�na teda=ei=`ole.    | KYE: SULETUD KAS |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8)
V: � on `k�ll:,    | KYJ: JAH |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.0) aga kas te soovite tulla `imikuga.         | KYE: SULETUD KAS |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
H: .hh ee no (.) `kolmeaastasega.        | KYJ: MUU |      | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(2.5)
V: (nii)=on pakkuda: (0.5) `k�mme kaks`k�mend.      | DIE: PAKKUMINE |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |

(.)
H: `k�mme kaks`k�mend.=   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
V: =jah.    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(1.0)
H: .hh no okei.   | DIJ: N�USTUMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: ja nimi on   | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: `Saabas, `Arvo.    | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(6.5) ((t�en�oliselt V kirjutab nime �les))
V: Arvo jah?   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: jah.   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
V: teeme siis `nimodi=et `k�mme kaksk�mend.       | DIJ: INFO ANDMINE |

(0.5)
H: ait�h?=   | RIE: T�NAN |
V: =ja=palun   | RIJ: PALUN |

   

 

      
