((590 telefonik�ne polikliiniku registratuuri))
((�hestas Andriela R��bis 5.07.2006))

((kutsung))   | RIE: KUTSUNG |
V: klienditeenindus   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
 Maret=kuuleb   | RY: TUTVUSTUS |
 tere   | RIE: TERVITUS |
H: .hh tere   | RIJ: VASTUTERVITUS |
 ma sooviksin laste`kliinikusse `silmaarsti juurde `aega.   | DIE: SOOV |
 (0.5) kolma`p�ev peale pool=vi- `poolt `viit `sobib.      | IL: T�PSUSTAMINE |
(...)
V: kakskend=kaks < `m�rts, > `seitseteist nul=`null `sobib.     | KYE: SULETUD KAS |  | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |   
(0.5)
H: mm jah     | KYJ: JAH |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |   
 ((paneb korraks diktofoni k�est))
V: {---}   | YA: PRAAK | 
H: Arvo Saabas.     | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(5.5)
V: mis=`aastal s�ndind    | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: .hh ��ksagend=`neli,    | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(2.5)
V: {telefon}     | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: .hh ee `neli neli kaheksa `neli, (.) null seitse `kaks.     | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |

(2.5)
V: kakskend=kaks `m�rts seitseteist `null=null ja doktor `Karin `Lumi.   | DIJ: INFO ANDMINE |
 Kirovi `kuus registratuurist `l�bi.    | IL: T�PSUSTAMINE |
aga `mis:=ee `�elge mule lapse isiku`kood palun.   | DIE: SOOV |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: .hh ee `kaks �heksa `neli,     | DIJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
V: jah   | VR: NEUTRAALNE J�TKAJA |
H: null=`seitse, (0.5) kolm=`�ks,     | DIJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
V: [jah]   | VR: NEUTRAALNE J�TKAJA |
H: [�ks] `seitse, viis `seitse.     | DIJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(2.0)
V: `Arvo on nimi.      | KYE: VASTUST PAKKUV |      | PPE: �LEK�SIMINE | 
H: jah   | KYJ: JAH |      | PPJ: L�BIVIIMINE |
(1.0)
V: .hh kakskend=kaks `m�rts seitseteist `null=null ja siis Kirovi `kuus.     | IL: �LER�HUTAMINE | 
H: ait�h?   | RIE: T�NAN |
[head]=aega   | RIE: H�VASTIJ�TT |
V: [palun]   | RIJ: PALUN |
V: {-}   | YA: PRAAK |
      

      

      
