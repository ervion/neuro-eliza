((531_a2 infotelefon))
((�hestas Andriela R��bis 15.05.2008))

((kutsung))    | RIE: KUTSUNG |
V: info`telefon    | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
Kersti    | RY: TUTVUSTUS |
tere   | RIE: TERVITUS |
(0.2)
H: tere=ommikut.   | RIJ: VASTUTERVITUS |
(0.2) .hhhh (.) `kas `teie `v�ite > mulle < (.) `kulla inimene `�elda=`mis:=`on `meil `Tartu `linnas `sellist `kus=`saab (.) .hhh `endale `karnevali kos`t��me laenutada `peale L�una`keskuse teater `Vanemuise.    | KYE: AVATUD |
(0.9)
V: * {jaa=  | VR: NEUTRAALNE VASTUV�TUTEADE |
`vaatame} * �ks=`hetk.   | KYJ: EDASIL�KKAMINE |
(2.6) ((klahvide kl�bin))
H: .mt=hhhh   | YA: MUU |
(1.8)
V: .mt (0.2) jah (.) meil=on `Karneval `m�rgitud ja on veel=`Linnupuu `lastepere `teater aga siin on `Hansa=kos`t��mid.   | KYJ: INFO ANDMINE |
(0.4)
H: .hh ja `kus need `Ansa=kos`t��mid `asuvad.   | KYE: AVATUD |
V: .h (0.2) `Narva `maantee sada=`�ks= `Tartu.   | KYJ: INFO ANDMINE |
(0.3)
H: .hh `�ks mo`ment ma > kirjutan < `�lesse.   | DIE: PALVE OODATA |
(0.3)
V: * jah *   | DIJ: N�USTUMINE |
(3.0)
H: eem `tee?    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) .hh `sada=�ks.   | VR: NEUTRAALNE VASTUV�TUTEADE |
 nii, `   | YA: JUTU PIIRIDE OSUTAMINE |
telefon `on `teil.   | KYE: JUTUSTAV KAS |
(0.2)
V: Narva `maantee oli jah.     | IL: �LER�HUTAMINE |
(.)
H: jah.      | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.2)
V: .hhh (0.4) ee (0.2) �tlen `teile `lahtioleku `p�evad ka:.    | YA: INFO ANDMINE |
`esmasp�ev `neljap�ev `reede, (0.5) `seitseteist kuni `�heksa`teist.   | YA: INFO ANDMINE |
H: .h `esmasp�ev `neljap�ev (.) .hh ee= `seitse`teist (0.3) kuni `�heksa`teist [jah.]   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
V: [ja] `reede oli `ka jah.    | KYJ: MUU |     | PPJ: L�BIVIIMINE |
H: ahah,    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS | | VR: PARANDUSE HINDAMINE |
`ree[de] jah=   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
V: [jah]    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
V: =.hh (.) ee= `laua`telefon v�i mo`biil.   | KYE: ALTERNATIIV |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.2)
H: ee= `andke `laud ja mo`biil.    | KYJ: ALTERNATIIV: M�LEMAD |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V: `seitse `ne[li]   | KYJ: INFO ANDMINE |
H: [.hh]    | YA: MUU |
V: `sei[tse]   | KYJ: INFO ANDMINE |
H: [`seit]se `neli (0.2) `s:eitse   | VR: NEUTRAALNE J�TKAJA |
(0.4)
V: `neli `kaheksa   | KYJ: INFO ANDMINE |
H: `neli kaheksa   | VR: NEUTRAALNE J�TKAJA |
(.)
V: `kuus `kaheksa.   | KYJ: INFO ANDMINE |
(0.2)
H: `kuus `kaheksa.    | VR: NEUTRAALNE VASTUV�TUTEADE |
mo[`biil]   | DIE: SOOV |
V: [j:]a: (.) mo`biil on `viis `viis   | DIJ: INFO ANDMINE |
(.)
H: * `viis viis*=jah   | VR: NEUTRAALNE J�TKAJA |
(.)
V: `kuus `�ks   | DIJ: INFO ANDMINE |
(0.5)
H: j:ah   | VR: NEUTRAALNE J�TKAJA |
(0.4)
V: `kolm `viis    | DIJ: INFO ANDMINE |
(1.4)
H: j:ah    | VR: NEUTRAALNE J�TKAJA |
(0.2)
V: `kolm `kuus.=   | DIJ: INFO ANDMINE |
H: =.hh    | YA: MUU |
(1.2) 
H: .hhh nii    | YA: JUTU PIIRIDE OSUTAMINE |
`Narva `maantee sada=`�ks `kus:: `pool > ta siis on <   | KYE: AVATUD |
kas `kohe .hhh (0.2) jumal=`oi[dku.]    | KYE: MUU |
V: [ee]=N:arva `maanteelt `ikka: �tleme siit `alt `linnast `akkab `pihta.   | KYJ: INFO ANDMINE |
(0.8) ((telefoni helin))
H: ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(1.0) 
H: .hh (.) `alt `linnast `pihta l�heb `kuskil sinna `�lesse=sis.      | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
(0.2)
V: `jah       | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(.) `jah   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
 (0.2) sest `Narva `maantee=.hh `akkab=ee:: `alt `linnast jah.     | IL: �LER�HUTAMINE |     | PPJ: L�BIVIIMINE |
H: jah    | VR: NEUTRAALNE VASTUV�TUTEADE |     | VR: PARANDUSE HINDAMINE |
`jah    | VR: NEUTRAALNE VASTUV�TUTEADE |     | VR: PARANDUSE HINDAMINE |
> `ma just `sellep�rast `k�sisingi. <    | IL: P�HJENDAMINE |
 .hh ai `t�h    | RIE: T�NAN |
k�i[ke `hea]d.   | RIE: SOOVIMINE |
V: [palun]   | RIJ: PALUN |
(0.2)
V: * `k�ike=ead. *     | RIJ: VASTUSOOVIMINE |
 
 
