((563_4 infotelefon))
((�hestas Andriela R��bis 25.11.2007))

((numbri valimine))
((automaatvastaja teade: tere. olete helistanud numbriinfo tasulisele numbrile. k�neminuti hind helistades lauatelefonilt on neli viisk�mmend ja mobiililt kuus viisk�mmend.))
((kutsung))   | RIE: KUTSUNG |
V: Sille kuuleb   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
 tere   | RIE: TERVITUS |
H: .hhhh �� tere    | RIJ: VASTUTERVITUS |
ma sooviksin teada: mis kell `t�na `Tartust Tallinnasse vel `�htupoole l�heb `rongi, .hh et umbes poole `viiest,   | KYE: AVATUD |
(0.9)
V: nii?    | VR: NEUTRAALNE VASTUV�TUTEADE |
kohe=vaatame?   | KYJ: EDASIL�KKAMINE |
 (1.1) l�heb kuus`teist viisk�mend=`neli, kiir`rong.   | KYJ: INFO ANDMINE |
H: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.7) ja=sis (.) `j�rgmine     | KYE: AVATUD |
(0.6)
V: ja: `viimane on `tavarong kaheksa`teist kolm`teist.   | KYJ: INFO ANDMINE |
(0.3)
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
kaheksa`teist kolm`teist=ja   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
V: jah   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.5)
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |     | VR: PARANDUSE HINDAMINE |
ja `m�lemal on=sis `kaheksakend krooni=v�i,   | KYE: SULETUD KAS |
(0.3)
V: ei,    | KYJ: EI |
sellel `kiirrongil on:=e `teises klassis `kaheksak�mend krooni ja `tavarongis on: seitsek�mend=`viis krooni pilet.   | IL: T�PSUSTAMINE |
H: ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
see `tavarong on see   | KYE: AVATUD |    | PPE: MITTEM�ISTMINE | 
V: e `kaheksateist kolm`teist.   | KYJ: INFO ANDMINE |    | PPJ: L�BIVIIMINE | 
H: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |    | VR: PARANDUSE HINDAMINE |
 kaheksateist kolmteist    | VR: NEUTRAALNE VASTUV�TUTEADE |    | VR: PARANDUSE HINDAMINE |
.hhhh aga: `mis kell l�heb homme `hommikul   | KYE: AVATUD |
(0.9)
V: homme `ommikul l�heb `seitse kolmk�mend=`kaks kiirrong,   | KYJ: INFO ANDMINE |
H: mhm[h]   | VR: NEUTRAALNE J�TKAJA |
V: [j]a: `tavarong l�heb `kaheksa `k�mme.   | KYJ: INFO ANDMINE |
(0.3)
H: mhmh mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) `s�iduaeg on umbes kolm `tundi=ja   | KYE: VASTUST PAKKUV |
(1.0)
V: ee t�hendab=se `kiirrongil on `kaks tun[di a]ga `tavarongil on `kaks tundi `viisk�mend minutit.=   | KYJ: MUU |
H: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
H: =mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
.hhhh ja `�li�pilastel on se `hinna `soodustus sis kolkend prot`se[nti] nagu `ka jah   | KYE: VASTUST PAKKUV |
V: [jah]   | KYJ: JAH |
V: [jah]   | KYJ: JAH |
H: [.hhh] a=kas=ee `homsest need `hinnad muidu `t�usevad ka v�i=`t�use.   | KYE: ALTERNATIIV |
(0.8)
V: < no: `hetkel ei ole > selle kohta veel    | KYJ: INFO PUUDUMINE |
(0.3) * �ks=moment * ma kohe `vaatan < `homse kohta t�pselt >    | KYJ: EDASIL�KKAMINE |
(6.5) mhmh?       | VR: MUU |
homsest `ongi .hhhh ee-dee `teises klassis on `�heksak�mend krooni ja n��d see `tavaklassis on `kaheksak�mend=`viis krooni pilet.      | KYJ: ALTERNATIIV: �KS |
H: mhmh=mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.3) nja see oli se `kaheksa `k�mme jah, se [`tava]klass   | KYE: VASTUST PAKKUV |      | PPE: �LEK�SIMINE |
V: [jah]   | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |    | VR: PARANDUSE HINDAMINE | 
V: [.jah]   | VR: MUU |
(0.8)
H: no `selge siis jah,      | RIE: L�PUSIGNAAL |
 ai[t�h teile]   | RIE: T�NAN |
V: [jah,       | RIJ: L�PETAMISE VASTUV�TMINE |
ai]t�h    | RIJ: T�NAN |
k�i[ke=head]   | RIE: SOOVIMINE |
H: [n�gemist]   | RIE: H�VASTIJ�TT |
     

