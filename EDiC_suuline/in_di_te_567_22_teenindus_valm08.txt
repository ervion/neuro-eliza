((in_di_te_567_22_teenindus))
((�hestas Andriela R��bis 29.05.2008))
((kutsung))   | RIE: KUTSUNG |
V: jaa?   | RIJ: KUTSUNGI VASTUV�TMINE |
(0.3)
H: tere    | RIE: TERVITUS |
mulle `anti teie `number.    | YA: INFO ANDMINE |
tahaksin [k�sida] infot Tukka `talu kohta.   | DIE: SOOV |
V: [jah]   | VR: NEUTRAALNE J�TKAJA |
(.)
V: jah.   | VR: NEUTRAALNE J�TKAJA |
H: .hhh kas te `olete aastavahetuseks `vaba veel.      | KYE: SULETUD KAS |
(0.6)
V: @ �aah?    | VR: MUU |
((tooniga: hiljaks j�id!)) ei: `ole enam. @   | KYJ: EI |
H: aa       | VR: HINNANGULINE INFO OSUTAMINE UUEKS |
((pettunult)) (.) .hh a kui `suur [see]   | KYE: AVATUD |
V: [kui] kui (0.3) `kui suur `kogus teid n�d `huvitas.   | KYE: AVATUD |      | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.6)
H: ee-ve `mis kogus.   | KYE: AVATUD |     | PPE: MITTEM�ISTMINE |
(0.4)
V: nagu `kuis palju `inimesi teil `on.   | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |     | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: no kuskil `k�mne ringis.   | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8)
V: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
et meil on jah selles m�ts=et (1.0) et et et=et=et=hhhhhh et on `on bron`eering=aga ma=i=ole=nd `t�psustand.    | KYJ: INFO ANDMINE |
v�ibolla `v�ib veel midagi `vabaneda.   | KYJ: INFO ANDMINE |
(0.5)
H: .hh a palju see `hind teil `on muidu.   | KYE: AVATUD |
(1.1)
V: `�ks p�ev v�i `kaks.   | KYE: ALTERNATIIV |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
H: ee no kolgend=�ks `�htu ja j�rgmine p�ev l�hme `�ra.   | KYJ: MUU |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.9)
V: ahah,    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
no=�ht- no=`�hte p�eva ei `m��gi `v�lja.   | KYJ: INFO ANDMINE |
H: aa    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
kaks `p�eva jah     | KYE: VASTUST PAKKUV | | PPE: �MBERS�NASTAMINE |
(0.4)
V: kaks p�eva jah    | KYJ: JAH |      | PPJ: L�BIVIIMINE |
(.) sest `aasta`vahetus on teadup�rast {-} (.) �he `korra=ja   | IL: P�HJENDAMINE |     | PPJ: L�BIVIIMINE |
(0.4)
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE | | VR: PARANDUSE HINDAMINE |
ja `palju se sis `on, (.) hind.   | KYE: AVATUD |
(0.3)
V: �� (.) `see oleks `kolm.   | KYJ: INFO ANDMINE |
(0.4)
H: kolm=`tuhat.   | KYE: VASTUST PAKKUV |   | PPE: �MBERS�NASTAMINE |
(0.5)
V: jah. | KYJ: JAH |      | PPJ: L�BIVIIMINE |
H: ja `inimesi mahub palju   | KYE: AVATUD |
(0.5)
V: ��:::::: selles m�ts=et se=on `mitu `maja et=noh    | KYJ: INFO ANDMINE |
(0.9)
H: no mingisse k�ige `suuremasse,   | KYE: AVATUD |
(0.9)
V: kuidas   | KYE: AVATUD |     | PPE: MITTEM�ISTMINE |
H: k�ige `suuremasse.    | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |    | KYE: AVATUD |
no ma tahan �ldse nagu: (.) `hindade kohta ja=nimodi=e:t,      | IL: T�PSUSTAMINE |
(1.6)
V: a k�ige `suuremasse=v�.     | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: jah   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
 (0.5) v�i=[noh]   | IL: MUU |     | PPJ: L�BIVIIMINE |
V: > [k�ige] `suurem on ka- < (.) k�ige `suurem mahutab kaks`kend.   | KYJ: INFO ANDMINE |
(0.4)
H: kaksk�mend.    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.9) ja see on kolm `tuhat.     | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(0.5)
V: jah.   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.3)
H: aa.    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |    | VR: PARANDUSE HINDAMINE |
(0.6) h�sti normaalne `hind=ju.   | SEE: V�IDE |
(0.7)
V: no ma arvan `k�ll.   | SEJ: N�USTUMINE |
H: sellep�rast `ongi `kinni juba jah.    | KYE: VASTUST PAKKUV |
mheh   | YA: MUU |
V: jah, (.) ilmselt `k�ll.   | KYJ: JAH |
(0.3)
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) aga: (.) no: `arvatavasti makstakse `�ra=et v�ibla et see ei `vabane [{---}]   | SEE: ARVAMUS |
V: [no ma: u]sun `k�ll jah,   | SEJ: N�USTUMINE |
H: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.3) no kahju.    | VR: HINNANGULINE VASTUV�TUTEADE |
(.) ait�h teile.   | RIE: T�NAN |
V: jaa?      | VR: MUU |
H: n�gemist   | RIE: H�VASTIJ�TT |
 
