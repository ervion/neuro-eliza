((in_di_te_565_2, juuksurisalong))
((�hestas Andriela R��bis 23.10.2008))
((kutsung))   | RIE: KUTSUNG |
V: {Ilu}hoov,   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
`tere.   | RIE: TERVITUS |
(.)
H: `jaa,   | VR: MUU |
 `tere   | RIJ: VASTUTERVITUS |
 ma `soovin `juuksuri aega `kinni panna.    | DIE: SOOV |
(0.4)
V: `jaa,   | VR: NEUTRAALNE VASTUV�TUTEADE |
 `kelle=juurde.   | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: ��=`jah n��d=`ongi, et ma `nime ei `m�leta,   | KYJ: INFO PUUDUMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
aga ma v�in `kirjeldada teda ja `kus=`kohas ta `t��tab.=     | DIE: PAKKUMINE |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: =`jaah?      | DIJ: N�USTUMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: et see on kui sinna `juuksurite `ruumi `sisse `minna `sis: ee, (.) `paremat `k�tt `akna `all on see `t��koht,    | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V: `mhmh=   | VR: NEUTRAALNE J�TKAJA |
H: =`aga=s, ��=`seal vist `on (.) mingi `noorem ja `vanem naine,  [{ * et=et * }]   | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: [`jah.]     | VR: NEUTRAALNE J�TKAJA | 
H: seal, et=see `vanem, `vanema juurde.=   | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: =`ahah?   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 `Maire juurde.    | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
(.)
H: `Maire, jah  $ `Ma:ire.= $     | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
V: =`mhmh.    | VR: NEUTRAALNE VASTUV�TUTEADE |    | VR: PARANDUSE HINDAMINE | 
H: $ pean `meelde j�tma=h.= $   | YA: MUU | 
V: =`heleda peaga.   | KYE: VASTUST PAKKUV |    | PPE: MUU | 
H: .h `heleda peaga [`jah.]   | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
V: [`mhmh?]   | VR: NEUTRAALNE VASTUV�TUTEADE |     | VR: PARANDUSE HINDAMINE |
 (.) .hhhhh [`n:ii]   | VR: NEUTRAALNE PIIRITLEJA | 
H: [jaa]=   | VR: MUU | 
V: =`mida `teha oleks [vaja.]   | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: [mm] `l�ikus.    | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V: .hhhhhhh < ��= `l�ikuse `aega [oleks] >   | DIJ: INFO ANDMINE | 
H: [`aga] ma tahaks `tegelikult=� no`vembrisse.   | DIE: SOOV |
(0.3)
V: `jaah?   | VR: NEUTRAALNE J�TKAJA |
(.)
H: et `peale: `seal `seitsmendast, `sellest `n�dalast.   | IL: T�PSUSTAMINE | 
V: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.3) .hhhhhh ((keerab lehti)) `nii,   | VR: NEUTRAALNE PIIRITLEJA |
 `vaatame `kohe=h.   | DIJ: EDASIL�KKAMINE |
 (5.0) ((keerab lehti)) `seitsmendal on n��d `Maire `hommiku=poole, `kaheksast `poole `kolmeni.   | DIE: PAKKUMINE |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.7) ((keerab lehti)) `teisip�eval `on (.) `poole `kolmest `�heksani,    | DIE: PAKKUMINE |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 ((keerab lehti))
(1.1)
H: {* vaatab *} `teisip�ev `teisip�ev, ei (0.9) `t:eisip�ev ei `sobi.   | DIJ: MITTEN�USTUMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: mhmh=      | VR: NEUTRAALNE J�TKAJA |
H: =`esmasp�eval oli `poole `kolmeni=jah.    | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(.)
V: `jah.    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.7)
H: `ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |     | VR: PARANDUSE HINDAMINE |
 et `sis=ma (1.3) `kell `kaksteist `�kki.      | DIJ: N�USTUMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V: `jaah?   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.7) ja kuidas `nimi `on.   | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: mm:: `Siivi.    | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8)
V: `Siivi.    | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(.)
H: `jah.   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.4)
V: [mhmh.]   | VR: NEUTRAALNE VASTUV�TUTEADE |   | VR: PARANDUSE HINDAMINE | 
H: [`aitab] `sellest `jah.    | KYE: VASTUST PAKKUV |
(0.3)
V: `jah.   | KYJ: JAH |
 [.hhh] siis on `seitsmes no`vember `kell `kaks`teist ja `Maire `juures.   | DIJ: INFO ANDMINE | 
H: [mhmh]     | VR: NEUTRAALNE J�TKAJA |
H: `jah.     | VR: NEUTRAALNE J�TKAJA |
V: ja `l�ikus.=   | IL: T�PSUSTAMINE | 
H: ={ee} (.) a `kas `teil=`on=�� (0.4) kos`meetiku=`teenust `pakutakse `ka `seal.=   | KYE: SULETUD KAS |    | TVE: PAKKUMINE | 
V: =`jaa.   | KYJ: JAH |     | TVJ: VASTUV�TMINE |
(.)
H: mhmh,   | VR: NEUTRAALNE VASTUV�TUTEADE |
 .hhhh et=ee (1.0) .hh et kui ma `tahaks � `n�onaha `puhastust,       | KYE: AVATUD |
(0.3)
V: `mhmh=   | VR: NEUTRAALNE J�TKAJA |
H: =et=ee (0.5) `kaua `see (.) protse`duur `�ldse `aega `v�tab.   | KYE: AVATUD | 
V: .hhhhh ��: kas `koos mas`saashiga v�i `ilma.=   | KYE: ALTERNATIIV |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: =`ilma.   | KYJ: ALTERNATIIV: �KS |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: * `mhmh, *   | VR: NEUTRAALNE VASTUV�TUTEADE |
 `siis on `tund `aega.   | KYJ: INFO ANDMINE | 
H: mhmh,    | VR: NEUTRAALNE VASTUV�TUTEADE |
.hhh `aga (0.7) .hh et ma ei `tea,   | YA: MUU |
(1.1)
V: kas `samal    | KYE: SULETUD KAS |
(.)
H: ma [`just m�tlen=et,]      | YA: MUU |
V: [samal `p�eval.]   | KYE: SULETUD KAS | 
H: kell `kaksteist (1.9) a=mm       | YA: MUU |
(0.3)
V: `et=`siis `tegelikult `parem `on kui `enne kos`meetikus `�ra [`k�ia.]   | SEE: V�IDE | 
H: [jaa] ma `m�tlengi.=   | SEJ: N�USTUMINE | 
V: =siis l�heb `pea `mustaks.    | IL: P�HJENDAMINE |
(1.1)
H: et `sis `tegelikult, oleks `vaja `vastupidi need ajad `panna.    | DIE: SOOV |
(0.4)
V: `mhmh,     | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.4) et `sis: `kell [`kaksteist,]   | KYE: VASTUST PAKKUV |    | PPE: �MBERS�NASTAMINE |
H: [`kell `kaksteist,]      | YA: MUU |
(.)
V: [kos`meetikusse]   | KYE: VASTUST PAKKUV |    | PPE: �MBERS�NASTAMINE | 
H: [`jah, * kos`meetik. * ]   | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
V: ja `kell: (0.4) `�ks `siis `juuksurisse.=   | KYE: VASTUST PAKKUV |    | PPE: �MBERS�NASTAMINE | 
H: =`jah.    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.3)
H: [ * mhemhe * ]      | YA: MUU |
V: [`mhmh?]   | VR: NEUTRAALNE VASTUV�TUTEADE |     | VR: PARANDUSE HINDAMINE |
(.) `aga `nii `saab `ka.       | DIJ: MUU |
 (0.3)
H: `aga `mis=`see `see `innakiri on kos`meetiku juures, et `palju `see `maksab.    | KYE: AVATUD |
(0.5)
V: mm:: `�ks mo`ment.   | KYJ: EDASIL�KKAMINE | 
H: * mhmh. *     | VR: MUU |
(1.9) ((�eldakse tasakesi k�rvalt ette))
V: .hhhh ee `puhastus on `kaksada `�heksak�mmend.=   | KYJ: INFO ANDMINE | 
H: =mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (2.3) {ait�h}    | RIE: T�NAN |
(0.3) .hhh `ja: (0.8) `ja `pal- (0.5) ja=`juuksuris, `palju `l�ikus on (.) [�� `poolpikad `juuksed.]   | KYE: AVATUD | 
V: [{--}] siis on `pesta `ka [`vaja, `poolpikk]      | KYE: VASTUST PAKKUV |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: [ * mhmh * ]     | KYJ: JAH |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: on `sadaseitsekend `viis,=   | KYJ: INFO ANDMINE |
H: * =mhmh. *   | VR: HINNANGULINE J�TKAJA |
V: `koos `pesu ja `kuivatusega.       | IL: T�PSUSTAMINE |
(.)
H: * `mhmh. *   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.7) et `sis seitse (.) `seitsmes no`vember (.) kell [`kaksteist kos`meetik]   | KYE: VASTUST PAKKUV |    | PPE: �MBERS�NASTAMINE | 
V: [`kell `kaksteist on siis kos`meetik ja]   | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
H: ja kell `kolmteist `juuksur.   | KYE: VASTUST PAKKUV |    | PPE: �MBERS�NASTAMINE | 
V: `jah.   | KYJ: JAH |   | PPJ: L�BIVIIMINE |
(0.3) `mhmh?    | KYJ: JAH |   | PPJ: L�BIVIIMINE |
(0.3)
H: [`olgu,   | VR: NEUTRAALNE PIIRITLEJA |
 `t�nan.    | RIE: T�NAN |
`jah.]   | VR: MUU |
V: [`nii,       | VR: NEUTRAALNE PIIRITLEJA |
`kohtumiseni.]    | RIE: H�VASTIJ�TT |
(0.3)
H: `n�gemist.   | RIJ: VASTUH�VASTIJ�TT |
      

