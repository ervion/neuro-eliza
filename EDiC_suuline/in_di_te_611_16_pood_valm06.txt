((611_16 telefonik�ne prillipoodi))

((�hestas Andriela R��bis 10.01.2007))

((kutsung))   | RIE: KUTSUNG |
V: `Hiina optik   | RIJ: KUTSUNGI VASTUV�TMINE |   | RY: TUTVUSTUS |
tere   | RIE: TERVITUS |
H: .hh tere.    | RIJ: VASTUTERVITUS |
mis `hinnaga v�iks saada miinus `�heksa `fotokroom p�ikese`prillid.   | KYE: AVATUD |
(0.6)
V: `klaasi.   | KYE: VASTUST PAKKUV | | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4)
H: �� `plastik.   | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: jah,   | VR: NEUTRAALNE VASTUV�TUTEADE |
 `�ks moment   | KYJ: EDASIL�KKAMINE |
(24.1)
V: tuhat `seitsesada `seitsegend tuhat `�heksa=`neligend noh, kuskil tuhande `seitsme`sajast akkavad `hinnad.   | KYJ: INFO ANDMINE |
(0.5)
H: �ks `klaas.   | KYE: VASTUST PAKKUV |
(.)
V: `kaks klaasi.      | KYJ: MUU |
(0.9)
H: �� (0.3) kuidas=se nii `odav saab olla.   | KYE: AVATUD |
(0.4)
V: no (0.4) `saab.     | KYJ: MUU | 
H: $ `teises firmas mulle �eldi `�ks klaas �le kahe `tuhande. $   | SEE: V�IDE |
V: $ no `igasuguseid igal `firmal on iga > `igasus=`hinnad   | SEJ: MUU |
 < mis `firmas se `oli. $   | KYE: AVATUD |
H: ee se=oli `Suurlinna optika.=   | KYJ: INFO ANDMINE |
V: =ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(1.3)
H: .hh [(0.5) e]     | YA: MUU | 
V: [ei, seal on] kuni   | YA: MUU |
 (0.4) ot �ks mo`ment,   | DIE: PALVE OODATA |
 `�heksa oli jah?   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE |
H: jah   | KYJ: JAH | | PPJ: L�BIVIIMINE |
(19.5)
V: kui te tahate `�hendatud sis on `kaks=tuhat `nelisada.   | IL: T�PSUSTAMINE |     
H: .hh jah, ta peaks olema �henda[tud] `plastik.     | IL: MUU | 
V: [jah]     | VR: NEUTRAALNE J�TKAJA | 
V: jah   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) ja siis no muidugi `k�ik peab `seda vaatama missugune `raam on mis `silmavahe on, .hh et noh=et see e`fekt ikka `tuleb.      | IL: T�PSUSTAMINE |
 alati noh ku=on �sti v�ike `raam, .hh sis=ei `peagi ta nii �hendatud `olema sest `miinus on `keskelt nigunii `�hem.       | IL: T�PSUSTAMINE |
(1.2) noh seda peab nagu `kohapeal sin `vaatama.   | IL: KOKKUV�TMINE |
H: ee kas teil `silmaarsti vastuv�ttu ka `on.   | KYE: JUTUSTAV KAS |
V: `homme on kella `k�mnest `�heni silmaarst meil.   | KYJ: INFO ANDMINE |
H: < aga kas: > `kolmap�eva (0.4) [kolmap�eviti]   | KYE: JUTUSTAV KAS |
V: [kolmap�eval=on] kolmap�eval=on kella `k�mnest (1.0) `�heni.   | KYJ: INFO ANDMINE |
(1.8)
H: ee::::: `selge?   | VR: NEUTRAALNE PIIRITLEJA |
(0.3)
V: jah.   | VR: MUU |
 (1.0) `vahel on ka `�htupoole aga ma=i `oska n�d `�elda kas=n�d `j�rgmine kolmap�ev on `�htupoole `ka.   | IL: T�PSUSTAMINE |
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
selge.   | VR: NEUTRAALNE PIIRITLEJA |
 eks ma `helistan sis `uue[sti.]    | DIE: PAKKUMINE |
V: [jah,]   | DIJ: N�USTUMINE |
 aga missugust `raami    | KYE: AVATUD |
teil raam on `olemas muidu.    | KYE: SULETUD KAS |
H: ei ole.   | KYJ: EI |
V: ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
.hh no vat sis peame `vaatama sit `raami ja sis saame ned `m��tmised teha {---} oleks nagu `sobiks.   | YA: INFO ANDMINE |
H: noh ta peaks olema v�imalikult `kerge.        | SEE: V�IDE |
 mul �hed `klaasist praegu `on=[aga (.) $ ag(h)a jah $]     | IL: T�PSUSTAMINE | 
V: [`klaas on {-} jah ] `klaas on kahjuks jah, mineraal on `raske,    | SEJ: N�USTUMINE |
aga ma �tlen=et seda .hh et peab vaatama seda: jah `�hendus`astet ja kas `aasf��rilist=v�i .hh noh=et missugune annab e`fekti {siia}.      | SEE: ARVAMUS |
 (.) et ega `alati ei pruugi se `k�ige kallim ka k�ige `parem olla.    | SEE: V�IDE |
oleneb `k�igest, missugune `raam=ja (.) mis `silmavahe=ja   | IL: T�PSUSTAMINE |
(0.5)
H: jah   | SEJ: N�USTUMINE |
V: sellest k�igest `oleneb.   | IL: KOKKUV�TMINE |
 (0.5) `alati ei pea `k�ige kallimat `v�tma.   | IL: �LER�HUTAMINE |
(.)
H: ait�h teile.   | RIE: T�NAN |
V: palun?   | RIJ: PALUN |
H: n�gemist?   | RIE: H�VASTIJ�TT |

   

 

      
