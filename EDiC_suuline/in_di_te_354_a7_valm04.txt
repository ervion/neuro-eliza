((354a7 telefonik�ne))
((�htlustas Andriela R��bis 11.10.2004))

((kutsung))   | RIE: KUTSUNG |
V: Kiisa puhkek�la kuuleb   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
 tere   | RIE: TERVITUS |
H: mt=.hh tervist.   | RIJ: VASTUTERVITUS |
 (.) ee sooviks infot teie (.) `majutuskoha `kohta.   | DIE: SOOV |
V: jaa palun?        | VR: NEUTRAALNE J�TKAJA |
H: et=e kuskohas te `t�psemalt `asute.  | KYE: AVATUD |
(0.5)
V: L��ne`maal?   | KYJ: INFO ANDMINE |
(1.8)
H: see on kuskil `mere ligidal.      | KYE: JUTUSTAV KAS |
V: ikka=ikka, mere `��res.   | KYJ: JAH |     
(.)
H: ahah       | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(.) mis:=selle `koha nimi t�psemalt on.   | KYE: AVATUD |
V: `t�psem kohanimi se=on postiaadress on `Kapsi, aga siin on `Vessiku (.) on: (0.5) `sadam l�hedal `kuus kilomeetrit {---}.   | KYJ: INFO ANDMINE |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
V: `Haapsalu l�hedal.     | IL: T�PSUSTAMINE |
H: `Haapsalu l�hedal,   | VR: NEUTRAALNE VASTUV�TUTEADE |
 `kui palju Haapsalust kilomeetreid   | KYE: AVATUD |
V: `Haapsalust tuleb umbes `kolmkend `kilomeetrit {---}   | KYJ: INFO ANDMINE |
H: ahah.      | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) ja ja `olete te avatud ka `praegusel: ajal ja [`talvel.]   | KYE: SULETUD KAS |
V: [jaa]   | KYJ: JAH |
 (.) jaa   | KYJ: JAH |
H: mhmh=   | VR: NEUTRAALNE J�TKAJA |
V: =aasta`ringselt.   | IL: T�PSUSTAMINE |
H: .hh milliseid `v�imalusi te veel `pakute.   | KYE: AVATUD |
 on seal mingid `konverentsi`saale=v�i   | KYE: JUTUSTAV KAS|
V: ikka.   | KYJ: JAH |
(0.5) ikka {-} konverentsisaalid.    | KYJ: JAH |
k�ige `suurem mahutab sada=`viiskend {inimest}     | KYJ: INFO ANDMINE |
(0.5)
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
(0.5)
V: sis=on `v�iksem saal `kolmek�mnene=ja sis on {-} `saal=ja sis on konverentsi`maja {---}     | KYJ: INFO ANDMINE |
H: {ahah}    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.5) ja on seal ka mingisuguseid `sporditegemise v�imalusi.   | KYE: JUTUSTAV KAS |
V: ikka on.    | KYJ: JAH |
(1.0) `palli {m�ngida saab ja `piljardit siis=ja `keeglit=ja} v�i vabandust=e (.) ee seda `sulgpalli=ja   | KYJ: INFO ANDMINE |
(0.8)
H: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) kas mingi `j�usaal v�i midagi [sellist]   | KYE: JUTUSTAV KAS |
V: [ei.]   | KYJ: EI |
 (.) {`sellist asja ei ole.}   | KYJ: EI |
H: mh[mh]   | VR: NEUTRAALNE J�TKAJA |
V: [aga] muidu (0.5) `saunakompleks on selline (0.5) kahe: `sauna ja `aurusauna ja mulli`vanniga.   | KYJ: INFO ANDMINE |
H: ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.8) jajah.   | VR: NEUTRAALNE PIIRITLEJA |
 .hh ja ja mis `hinnas se `majutus seal on.   | KYE: AVATUD |
V: ((ohkab)) no: meil on eri:=hh suurusega `maju k�ige `v�iksem: `kahe m:agamistoaga maja maksab praegu (.) kuussada `�eksand `krooni, (.) [`neljane] on tuhat `�kssada kaks`k�mend,    | KYJ: INFO ANDMINE |
H: [��p�ev]   | KYE: VASTUST PAKKUV |      | PPE: �MBERS�NASTAMINE |
V: `kahe magamistoaga maja `kuuele on tuhat viissada `�heksak�mend,    | KYJ: INFO ANDMINE |
(.) {---}   | YA: PRAAK |
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.5) ja: `on seal mingid `piirangud ka kui `kauaks neid saab broneerida.   | KYE: JUTUSTAV KAS |
V: {-}      | YA: PRAAK |
H: ahah       | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
.hh kas teil on mingisugune (.) `veebiaadress ka olemas kus saab (.) [infot.]   | KYE: JUTUSTAV KAS |
V: [jaa?]    | KYJ: JAH |
(.) {-}   | YA: PRAAK |
H: `mis see `oleks.   | KYE: AVATUD |
V: �tlen kohe?    | KYJ: EDASIL�KKAMINE |
(0.5) `kolm dabl`juud,   | KYJ: INFO ANDMINE |
(0.8)
H: vevevee      | VR: NEUTRAALNE J�TKAJA |
(0.5)
V: ee siis `punkt,   | KYJ: INFO ANDMINE |
H: jah   | VR: NEUTRAALNE J�TKAJA |
V: Kiisa `punkt,   | KYJ: INFO ANDMINE |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
V: ee   | KYJ: INFO ANDMINE |
(0.5)
H: mhmh.   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.5) ja: ja bro`neerida siis saab sellelsamal `telefonil [kui on soovi.]   | KYE: SULETUD KAS |
V: [jah, | KYJ: JAH |
{-}]     | YA: PRAAK |
(1.2)
H: no selge.   | VR: NEUTRAALNE PIIRITLEJA |
 siis ait�h.   | RIE: T�NAN |
V: jaa palun?   | RIJ: PALUN |
H: head=� `p�eva.      | RIE: H�VASTIJ�TT |
V: ait�h      | RIJ: MUU |


   

 
   

 
   

 
