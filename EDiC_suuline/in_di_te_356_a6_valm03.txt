((356a6
telefonik�ne reisib�roosse))
((�htlustas Andriela R��bis 18.11.2003))

((kutsung))   | RIE: KUTSUNG |
V1: Iks=reis   | RIJ: KUTSUNGI VASTUV�TMINE |   | RY: TUTVUSTUS |

H: tere.   | RIE: TERVITUS |
V1: tere?   | RIJ: VASTUTERVITUS |

H: mind huvitaks `n�dalal�pureis `Londonisse.    | DIE: SOOV |
(0.5) `pakute neid.   | KYE: SULETUD KAS |
V1: �hendan (.) ee spetsia`listile.      | KYJ: KEELDUMINE |   | RY: �LEANDMINE |

H: jah,    | VR: NEUTRAALNE VASTUV�TUTEADE |
ait�h?   | RIE: T�NAN |


((ootemuusika, kutsung))   | RIE: KUTSUNG |

V2: `Iks=reis=   | RIJ: KUTSUNGI VASTUV�TMINE |   | RY: TUTVUSTUS |

`Reelika=   | RY: TUTVUSTUS |
tere.   | RIE: TERVITUS |

H: tere.   | RIJ: VASTUTERVITUS |
 ee `mind huvitaks `n�dalal�pureis `Londonisse.   | DIE: SOOV |
V2: nii,    | VR: NEUTRAALNE PIIRITLEJA |
millal.   | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |   | KYE: AVATUD |

H: ee (.) ta v�iks olla n��d=eeh see kaksk�mend=`kolm kuni kakskend=`viis november, v�i=siis `kolmk�mmen:d november kuni `esimene detsember. | KYJ: INFO ANDMINE |
et kus[kil seal.]    | KYJ: INFO ANDMINE |
V2: [mhmh] | VR: NEUTRAALNE VASTUV�TUTEADE |
�ks hetk,  ma `�hendan ennast=ee lennubroneerimiss�s`teemiga kohe sis `vaatame. | DIE: PALVE OODATA |  
H: jah.   | DIJ: N�USTUMINE |


((ootemuusika, kutsung))   | RIE: KUTSUNG |

V2: `Iks=reis    | RIJ: KUTSUNGI VASTUV�TMINE |   | RY: TUTVUSTUS |

`Reelika=kuuleb.    | RY: TUTVUSTUS |
(.) hallo   | KKE: ALGATUS |

H: tere?   | KKJ: KINNITAMINE |    | RIE: TERVITUS |
V2: nii. | VR: NEUTRAALNE PIIRITLEJA |  
n��d hakkame seda juttu sis kohe `r��ki[ma] eksole.    | DIE: ETTEPANEK |
H: [ahah] | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
H: jah. | DIJ: N�USTUMINE |
V2: �ks hetk ma teen siin `l�petan �he asja mis mul siit eest `lahti praegu panen selle kohe omal kohe siit eest kinni `�ra, siis on ta meil (0.8) eest `�ra siis ma kohe vaatan [teile] seda `lendu. | DIE: PALVE OODATA | 
te olete `mitmekesi minemas. | KYE: AVATUD |
H: [mhmh]    | DIJ: N�USTUMINE |
H: kahekesi. | KYJ: INFO ANDMINE |
V2: ja mismoodi se `majutus v�iks teil v�lja `n�ha. | KYE: AVATUD |
H: mt=.hh ee mida te `pakute. | KYE: AVATUD | 
(.) ma=i=`tea= | KYJ: INFO PUUDUMINE | 
ei: ei ole veel kunagi `k�inud $ nimodi. $ | IL: P�HJENDAMINE |
V2: $ ah t�hendab nii. $ | VR: HINNANGULINE VASTUV�TUTEADE | 
siis k�sin nimodi=  | YA: EELTEADE |
kuidas=te nagu arvate mis tasemel see ma- .hh selles m�ttes ma k�sin kuidas ta v�iks `v�lja n�ha noh=et=mis .hh et=et mitme `t�rniga see ho`tell v�iks `olla teil. | KYE: AVATUD |
et=kas k�ige `odavamat otsime v�i | KYE: JUTUSTAV KAS |
H: ee no=�tleme=et kuskil siukest `keskmist v�iks sis `olla. | KYJ: INFO ANDMINE |
et mitte | KYJ: INFO ANDMINE |
(.) 
V2: ahah | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(.)
H: ja=mitte=ka k�ige `kallimat. | KYJ: INFO ANDMINE | 
on teil midagi `sellist. | KYE: SULETUD KAS |
V2: noh `ikka on `�htteist nimodi. | KYJ: JAH |
(.) .hh ja akkame sis aga otsast `vaatama.    | YA: JUTU PIIRIDE OSUTAMINE |
k�igepealt se `lennuaja oli meil sis vaatame sis kakskend=`kolm kuni kakskend=`viis=v�i. | PPE: �LEK�SIMINE | | KYE: VASTUST PAKKUV |
H: .hh ja=ma p:r�egu=vel nii `t�pselt ma=ei=`julgeks �ra `�elda.  | PPJ: L�BIVIIMINE |  | KYJ: INFO PUUDUMINE | 
[ma lihtsalt] | IL: P�HJENDAMINE |
V2: [�hes�naga me] lihtsalt `vaatame kuidas lennu`kohti on, [tee]me `nii.    | DIE: ETTEPANEK |
H: [jah] | DIJ: N�USTUMINE |   | DIE: SOOV |
H: jah. | DIJ: N�USTUMINE |   | DIE: SOOV |
V2: .hh * nimodi v�tan lahti omal=ned * (1.8) paberid sin `ka k�ik eest, | DIJ: EDASIL�KKAMINE |
.hh ja `miinimum on siis `kolm `��d peaks olema `kohal. | YA: INFO ANDMINE |
H: jaa? | VR: NEUTRAALNE J�TKAJA |
V2: .hh nii=et kui te kakskend=`kolm kuni kakskend=`viis siis on juba `kaks ��d kohal. | IL: J�RELDAMINE | 
peab v�tma nagu `�he �� kuskilt `juurde. | IL: J�RELDAMINE |
H: et siis peaks nagu �tleme=et kahek�mne siis `teisest �kki v�i=v�i.  | KYE: JUTUSTAV KAS | 
(.) [kuidas see k�ib.] | KYE: AVATUD |
V2: [ee] `kahek�mne=teisest v�ib `k�ll minna. | KYJ: JAH | 
p�him�tteliselt `k�ll=jah. | IL: �LER�HUTAMINE |
H: mhmh | VR: NEUTRAALNE J�TKAJA |
V2: .hh ma korraks `v�rdlesin vanasti oli nagu natuke `teistmoodi see. | IL: T�PSUSTAMINE |
(1.5) ikka `kolm ��d `kohal jah. | IL: �LER�HUTAMINE |
H: mhmh | VR: NEUTRAALNE J�TKAJA |
V2: .hh v�tan siis kakskend=`kaks, (0.8) ja (0.5) < no`vember (0.5) ja `London? | DIE: PAKKUMINE |
(.) ja siis v�taks selle kohe > (.) Est�unian `Airiga | DIE: PAKKUMINE |
nii=on otse`lend meil. | IL: P�HJENDAMINE |
(.)
H: mhmh | DIJ: N�USTUMINE |  | DIE: SOOV |
V2: niimoodi, | YA: JUTU PIIRIDE OSUTAMINE | 
siin on kohti `k�ll. | DIJ: INFO ANDMINE |
see l�b `kuusteist viisteist v�lja see on seitseteist `viisteist `kohal. | DIJ: INFO ANDMINE |
H: mhmh | VR: NEUTRAALNE J�TKAJA |
V2: .hh ja kui n�d sellega=sis meil `tagasi tulla kaksk�mend ee kaks `minna sis {ta saab} kakskend=viis `tagasi. | DIJ: INFO ANDMINE |
H: jah. | VR: NEUTRAALNE J�TKAJA |
(0.5)
V2: no`vember, (0.5) `siin praegu on `ka kohti. | DIJ: INFO ANDMINE |
ja `vaatame `j�rgmise n�dala ilusasti `ka=vel [�le] omal siis et kol- kakskend=`�heksa minna. | DIE: ETTEPANEK |
H: [mhmh] | DIJ: N�USTUMINE |  | DIE: SOOV |
H: jah, [ilmselt `k�ll.  | DIJ: N�USTUMINE |  | DIE: SOOV |
mhmh] | DIJ: N�USTUMINE |  | DIE: SOOV |
V2: [kakskend=`�heksa] no`vember (.) `Londonisse? | DIJ: INFO ANDMINE |
(3.2) `on kohti, ja et=ee nelja `p�eva p�rast sis on `mandi, �ks (.) p�ev `tagasi on=meil (.) on sis nii ja siin on kohti `k�ll `jah. | DIJ: INFO ANDMINE |
H: mhmh | VR: NEUTRAALNE J�TKAJA |
V2: nii=et `lennukohtadega ei ole nagu prob`leeme. | IL: KOKKUV�TMINE |
H: jah. | VR: NEUTRAALNE J�TKAJA |
V2: `majutusest on n�d pakkuda ma=i=tea=kas=te meie `kodulehek�ljele `p��sete ligi `v�i ei p��se. | KYE: SULETUD KAS |
(.)
H: ee n�d `hetkel k�ll ei `p��se aga `iseenesest on [see `v�imalik.] | KYJ: MUU KAS-VASTUS |
V2: [iseenesest on] see `niimoodi, .hh et `odavaim (.) t�hendab n��d ma �tlen lihtsalt niivisi=et siin `katalogis `kirjas on me iga [kord] k�sime k�ik hinnad asjad `�le, aga [`odavaim] variant on meil siis | DIJ: INFO ANDMINE |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
 [mhmh] | VR: NEUTRAALNE J�TKAJA | 
V2: `neli=tuhat `viissada �heksak�mmend=`viis `krooni. | DIJ: INFO ANDMINE |
H: mhmh? | VR: NEUTRAALNE J�TKAJA |
V2: .hh niimoodi | YA: JUTU PIIRIDE OSUTAMINE |
(.) ja sinna sisse k�ib sis=se hotell `Tavistok, mis on `kahe t�rniga ja hommikus��gid `ka.    | DIJ: INFO ANDMINE |

H: jaa? | VR: NEUTRAALNE J�TKAJA |
V2: .hh ja=sis nii=`ongi. | IL: KOKKUV�TMINE |
ja=`lennupilet on juba `ka seal sees.   | DIJ: INFO ANDMINE |

H: mhmh. | VR: NEUTRAALNE J�TKAJA |
V2: .hh ja vat `lennujaamamaks on siia juurde veel pane`mata. | DIJ: INFO ANDMINE |
t�heb [see=on] see=on | IL: T�PSUSTAMINE |
H: [ahah]  | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V2: see=v�ib `t�usta meil on kataloogis kirjas `�heksasada `krooni, aga see `katalog sai valmis ennem kui .hh enne �heteistk�mnendat [sep`tembrit.] | IL: T�PSUSTAMINE |
H: [ahhaa.] | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
mhmh= | VR: NEUTRAALNE J�TKAJA |
V2: =nii, | YA: JUTU PIIRIDE OSUTAMINE |
ma `kohe t�psustan selle len[nu- (.)] `lennujaamamaksud `�ra. | DIE: PALVE OODATA |
H: [jaa?] | VR: NEUTRAALNE J�TKAJA |
H: mhmh | DIJ: N�USTUMINE |
(1.2)
V2: ma just tean=et sealt=ned `t�usid irmsasti, (1.0) `turvalisuse`maksud. | IL: P�HJENDAMINE | 
H: jah | VR: NEUTRAALNE J�TKAJA |
V2: kas=se muidugi turvalisemaks [`t(h)eeb=aga (.)] aga `maksu peab ometi `koguda.   | SEE: ARVAMUS |
H: [mhemhe] | VR: HINNANGULINE J�TKAJA |
H: jajah | SEJ: N�USTUMINE |
V2: niimodi siit (.) {-} > ma v�tan kohe (.) selle `arvuti ja kohe vaatan. < | DIE: PALVE OODATA |
H: mhmh | DIJ: N�USTUMINE |
(8.0)
V2: hallo, | KKE: ALGATUS |
nii[moo]di, | YA: JUTU PIIRIDE OSUTAMINE |
liidan `kokku k�ik=ned suured `numbrid [siin] | DIE: PALVE OODATA |
H: [jah] | KKJ: KINNITAMINE |
[mhmh] | DIJ: N�USTUMINE |
V2: .hh kaks=kaks=`viis. | YA: MUU |
pluss: `viissada=`�ks. | YA: MUU |
pluss sada=kuuskend (.) `kolm. | YA: MUU |
.hh jah, p�him�tselt ta j��bki niimodi `�heksasaja krooni piirile `�nneks. | DIJ: INFO ANDMINE |
H: ahhaa. | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V2: siis=ma: siis=on siis=on asi `�ige. | IL: HINNANG |
H: mhmh [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: [.hh] niimodi | YA: JUTU PIIRIDE OSUTAMINE |
ja=sis siia `juurde saan mina `pakkuda veel igasuguseid `asju teile.  | DIE: PAKKUMINE |
H: jaa see=mind `uvitabki= | DIJ: N�USTUMINE |
et=et kas seal on nagu mingi prog`ramm ka `ette n�h[tud v�i] | KYE: JUTUSTAV KAS |
V2: [.hh] selles m�ttes prog`rammis t�ndab selles m�ttes prog`ramm {-} selle �tleme mingisuguseid `etendusi v�i asju seda v�in mina teile `tellida.  | KYJ: INFO ANDMINE |
 ned `piletid maksavad noh nimodi �tleme .hh viie`s:ajast ma n��d ei=oska `�elda=�tleme .hh viiesajast tuhande `viiesajani umbes [on ned] pileti`hinnad on igasugused noh ned | KYJ: INFO ANDMINE |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: `muusikalid `K�ts=ja: | KYJ: INFO ANDMINE |
H: jah= | VR: NEUTRAALNE J�TKAJA |
V2: =M:amma=`Mia=ja mis nad seal k�ik t�pselt `on, [.hh] ja `siis on veel varjant=et | KYJ: INFO ANDMINE |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: v�tate n�iteks meie=k��st `London visiter `tr�velkaardi. | KYJ: INFO ANDMINE |
H: mhmh | VR: NEUTRAALNE J�TKAJA |
V2: t�ndab see on `transpordi`kaart, .hh mis siis ee (.) h�lmab Londoni noh �his`transporti=ja [`mille] `alusel saab=ka .hh soodus- (.) t�heb soodustusi | IL: SELETAMINE |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: hotellides=ja `muuseomides v�i vabandust `restoranides=ja `muu[seomides.] | IL: SELETAMINE |
H: [mhmh] mhmh | VR: NEUTRAALNE J�TKAJA |
V2: .hh ja=sellel sellel sellel=on=sis olemas omad hinnad et s�ltub mitme p�evaks `minna kuidas t�pselt `v�tta, [.hh] ja=siis on v�imalik v�tta nimodi=et `kaks | KYJ: INFO ANDMINE |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: �he hinnaga (.) seda `K�tvik `ekspressi `piletit t�hendab kuna K�tviki `lennujaam .hh j��b=ju `linnast `eemale, | KYJ: INFO ANDMINE |
H: jaa? | VR: NEUTRAALNE J�TKAJA |
(0.5)
V2: noh nii nagu=nad `k�ik j��vad, | IL: T�PSUSTAMINE |
H: mhmh= | VR: NEUTRAALNE J�TKAJA |
V2: =.hh siis selleks=et `linna `saada, on vaja ka mingisugust `transporti ja=kui `muidu on see `�hesuuna`pilet v�i �k- �ks `pilet `�hele [inim]esele mingi | KYJ: INFO ANDMINE |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: `viiesaja=krooni ringis sis n�d on v�imalus .hh mingi `viie poole `saja eest saada `kahele sest [ta=on] edasi=`tagasi.  | KYJ: INFO ANDMINE |
se=on .hh ma=tlen ma=r��gin `peast ja=ma ma | IL: T�PSUSTAMINE |
H: [ahah] | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V2: v�ibolla noh et see asi neil on `talvehooajaks on ta `muutunud, aga=noh {-} suurus`j�rk on siiski `sama. | IL: T�PSUSTAMINE |
H: jah (.) mhmh | VR: NEUTRAALNE J�TKAJA |
V2: ma nimodi `peast lihtsalt r��gin mis=sin nagu `saada `on | IL: �LER�HUTAMINE |
et noh need on ja reisi`kindlustus paariks `p�evaks on mingi noh alla `saja=krooni `inimene. | YA: INFO ANDMINE |
H: ee siis mind uvitab=| YA: EELTEADE |
et kas mingeid `soodustusi ka `on  | KYE: JUTUSTAV KAS |
et ku- kuna ma `ise olen `�li�pilane= | IL: P�HJENDAMINE |
et=et kas kas nagu | KYE: MUU |
V2: $ vat `see asi `siin pr�egu ei `maksa. $ | KYJ: EI |
[mhe]  | YA: MUU | 
H: [ahhaa.]  | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V2: $ `see ei maksa `sellep�rast $ .hh et meil on niikui`nii lennupiletite v�i lennu- lennupakettide {koosseisus}pakutud `t�esti p�ris soodsad `hinnad, | IL: P�HJENDAMINE |
H: mhmh mhmh= | VR: NEUTRAALNE J�TKAJA |
V2: =ja kui te muidu hakkate Londonisse `lendama, te maksate rohkem kui=sele kui=se pa`keti hind [siin on. | IL: P�HJENDAMINE |   | SEE: V�IDE |

eksole. | KYE: VASTUST PAKKUV |
{-} jaa, jaa]   | SEJ: N�USTUMINE |

H: [jah, ilmselt jah. | KYJ: JAH |
see `lennukipilet tuleb juba `kallim.] | SEE: V�IDE |
V2: .jaa. | SEJ: N�USTUMINE |
ja kuna t�hendab [ka need] `tr�velkaardid k�ik on juba niikui`nii nii�elda | IL: P�HJENDAMINE |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: sellise kokkuleplusele `mindud ja tehtud [nagu] sooduspa`ketid, siis `sealt enam | IL: P�HJENDAMINE |
H: [jah] | VR: NEUTRAALNE J�TKAJA |
V2: `alla ei `saa. | IL: P�HJENDAMINE |
H: mhmh mhmh | VR: NEUTRAALNE J�TKAJA |
(0.8)
V2: `nii, | YA: JUTU PIIRIDE OSUTAMINE |
t�hendab=et allahindlust $ mina siit ei anna m(h)idagi. $ | IL: KOKKUV�TMINE |
[mhemhe] | IL: PEHMENDAMINE |
H: [selge.] | VR: NEUTRAALNE PIIRITLEJA |
V2: jah, | VR: MUU |
a[ga]=n�d aga=et=et mis=ma mis=ma `veel �tlen teile.  | KYE: AVATUD |
H: [jah.] | VR: MUU |
H: .hh ee (.) r��giks veel sellest=ee sellest `majutusest. | KYJ: INFO ANDMINE |  | DIE: SOOV |
V2: nii.= | VR: NEUTRAALNE J�TKAJA |
H: =et=ee (.) noh mis ned `m:u[gavused seal=sis `on=v�i=v�i.] | KYE: AVATUD |
V2: [`k�ik k-k-`k�ik] on `kesklinna �tleme `kesklinna `piirkonnas. | KYJ: INFO ANDMINE |
`kesklinn on oluliselt kindlasti `suurem kui on `meie kesklinn [eksole. | IL: T�PSUSTAMINE |
.hh] | YA: MUU |
H: [mhmh mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: ja: ma:=n�d (.) ja ja siis ma n�iteks n�d=kohe loen teile `ette, mis `kirjad meil siin `on. | KYJ: EDASIL�KKAMINE |
(.) Pariis=Viin (0.5) `London? | KYJ: EDASIL�KKAMINE |
`Tavistokil on muidu oma `kodulehek�lg `ka nii=et [kui te] l�hete `meie kodulehek�ljele netis, | IL: T�PSUSTAMNE |
H: [ahah] | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
H: et=sis `saab [sealt edasi.] | IL: J�RELDAMINE |
V2: [.hh siis=te] leiate selle ilusti �les otsite `Londoni, vaatate `Tavistoki ho`tell, | IL: SELETAMINE |
see on kahe`t�rniline? | KYJ: INFO ANDMINE |
H: mhmh | VR: NEUTRAALNE J�TKAJA |
V2: .hh niimodi | YA: JUTU PIIRIDE OSUTAMINE |
`toad=hh {-} see=on viiek�mne=`kolmandal aastal ehitatud `kolmesaja kahek�mne=`�heksa toaga ho`tell asub `Tavistok Sku��r `��res? | KYJ: INFO ANDMINE |
(.) .hh `l�him metroopeatus on {-}, | KYJ: INFO ANDMINE |
see ei=�tle `midagi=ku kaarti ees ei `ole, | IL: HINNANG |
H: mhmh | VR: NEUTRAALNE J�TKAJA |
V2: .hh `toad mahutavad �he lisa`voodi, `tee ja kohvivalmistamise `vahendid saab `soovi korral hotelli `vastuv�tulauast tuppa `tellida. | KYJ: INFO ANDMINE |
H: mhmh | VR: NEUTRAALNE J�TKAJA |
V2: noh niisugune. | IL: KOKKUV�TMINE |
(.) [mitte]`midagi�tlev `jutt p(h)�him�tteliselt.  | SEE: ARVAMUS |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
H: $ {ega vist `k�ll.} $ | SEJ: N�USTUMINE |
V2: .hh aga ma �tlen=ta selle kodulehe=pe- lehek�lje [`peal] seal=ta on sis nagu | IL: �LER�HUTAMINE |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: enamv�hem `olemas se=`asi. | IL: �LER�HUTAMINE |
H: jah. | VR: NEUTRAALNE J�TKAJA |
(.)
V2: .hh noh=t�endab=ma (.) kuna nad on t�esti valida �t- �tleme=et < `kesklinna `piirkonda valitud [et=sis] ma usun=et=noh ta peaks nagu `asukohalt olema | SEE: ARVAMUS |
H: [mhmh] | VR: NEUTRAALNE J�TKAJA |
V2: `soodne.  | SEE: ARVAMUS | 
> ja=kui=te l�hete sinna `ennek�ike justnimelt noh `vaatama seda `linna.   | IL: T�PSUSTAMINE | 

H: jah.   | SEJ: N�USTUMINE |
V2: .hh et=et noh    | YA: MUU |

(.) nii.     | YA: JUTU PIIRIDE OSUTAMINE |

> lepime nimodi `kokku=et=kui=te=n��d tahate midagi `tellida? < | DIE: ETTEPANEK |
[siis=te] m�tlete selle asja `l�bi?   | DIE: ETTEPANEK |

H: [jah]   | VR: NEUTRAALNE J�TKAJA |

H: mhmh=   | VR: NEUTRAALNE J�TKAJA |
V2: =ee < ja siis > vaatate seda netilehek�lje=pealt pilti `juurde?   | DIE: ETTEPANEK |

H: mhmh   | VR: NEUTRAALNE J�TKAJA |
V2: .hh ja siis te v�ite mulle n�iteks `meili saata. | DIE: ETTEPANEK |
H: jah.    | DIJ: N�USTUMINE |
(.) et `meiliaadress on:=teil mingi: [`�ldine=v�i=v�i] | KYE: JUTUSTAV KAS |
V2: [.hh {-}] `�ldise pealt on=ta=jah=et=`�ldine | KYJ: JAH | 
aga kui te paneksite `minu peale | KYJ: INFO ANDMINE | | DIE: ETTEPANEK |
st=noh niikuinii mina=m mina `teen selliseid `asju=sin. | IL: P�HJENDAMINE |
H: jah. | DIJ: N�USTUMINE | | DIE: SOOV |
V2: neid noh=neid linnapa`kette | IL: P�HJENDAMINE |
ja minu meili`aadress on `Reelika k�ib kahe `eega? | DIJ: INFO ANDMINE |
H: jaa? | VR: NEUTRAALNE VASTUV�TUTEADE | 
`v�ikse t�hega. | PPE: �MBERS�NASTAMINE | | KYE: VASTUST PAKKUV |
V2: just`nimelt. | PPJ: L�BIVIIMINE | | KYJ: JAH |
[ja:] �t iks reis punkt ee-`ee, | DIJ: INFO ANDMINE |
sest `�ldine on meil `info �t iks=[reis.] | IL: T�PSUSTAMINE |
H: [mhmh?] | VR: PARANDUSE HINDAMINE | | VR: NEUTRAALNE J�TKAJA |
[ahhaa.] | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
jah. | VR: NEUTRAALNE J�TKAJA |
V2: .hh sealt j�uab=ta noh niigusnii minu `lauale aga selles=m�ttes=et mis ta=s `teistele l�heb [{---}]  | IL: P�HJENDAMINE |   | SEE: V�IDE |

H: [jah,    | SEJ: N�USTUMINE |
siis saab kohe `otse.]     | SEE: V�IDE |
V2: .hh jah,   | SEJ: N�USTUMINE |
 ja sis kui {-} tahate midagi=sit `tellida v�i `asja sis pange `juurde=t�hendab=oma .hh niuke `nimed kuu`p�evad. | DIE: ETTEPANEK |
[ja] mis varianti ho`telli, ja:    | DIE: ETTEPANEK |

H: [mhmh]   | DIJ: PIIRATUD N�USTUMINE |
V2: siis l�kitage ta `mulle pange `telefon ka alla siis me saame heasti `suhelda.   | DIE: ETTEPANEK |

H: mhmh   | DIJ: PIIRATUD N�USTUMINE |

(.)
V2: eksju.   | KYE: VASTUST PAKKUV |

H: ja=ja nii=et=siis neid `pileteid j�tkub veel eksole.    | KYE: T�PSUSTAV |   | KYE: VASTUST PAKKUV |

neid [`lennupileteid just.]   | IL: T�PSUSTAMINE |
V2: [`pr�egu neid `lennupileteid] oli `k�ll `jaa,    | KYJ: JAH |
[aga no muidugi mida] parem varem me selle asja `�ra teeme seda   | KYJ: INFO ANDMINE |

H: [sellega pole]   | YA: MUU |
V2: `parem ta `on, sest=et=noh=et `t�histada saab seda nii�elda `enne .hh enne lennu `v�ljumist igal `ajal a=kui=ta=on juba `v�lja tr�kitud=on [siis=ju] .hh selle peab   | KYJ: INFO ANDMINE |

H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
V2: {juba} `n�dal=aega ennem `v�lja ostma muidu l�heb=    | KYJ: INFO ANDMINE |

H: =mhmh=   | VR: NEUTRAALNE J�TKAJA |
V2: =muidu l�b `jamaks.   | KYJ: INFO ANDMINE |

H: jah.   | VR: NEUTRAALNE J�TKAJA |
V2: t�hendab see tuleb v�lja osta �tleme kas=sis vastavalt kas=sis .hh mingi `neli`teist v�i kakskend=`�ks, j�taks `�he p�eva igaks juhuks siis [`varuks.]  | KYJ: INFO ANDMINE |
H: [mhmh]    | VR: NEUTRAALNE VASTUV�TUTEADE |
aga `kui n��d `midagi peaks juhtuma et ei=saa `minna, kas siis mingeid garan`tiisid on=[et=et saab mingi osa `tagasi=v�i]     | KYE: JUTUSTAV KAS |
V2: [.hh t�hendab kui teil on `pilet] `v�lja `ostetud, ja siis=te tahate selleks=et mine=`tea mis �kki `juhtub, [siis] ma soovitan teil `teha peale reisi `katkemise   | KYJ: INFO ANDMINE |

H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
V2: `kindlustuse.    | KYJ: INFO ANDMINE |
see lisab [.hh] reisikindlus-=mingi `viiskend `krooni `peale.  | KYJ: INFO ANDMINE |

H: [aa]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V2: [{-}] `see nagu katab `�ra teie selle paketi `hinna, [.hh] ja=kui=teil=on: n nii�elda   | KYJ: INFO ANDMINE |

H: [ahah]    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
[mhmh]   | VR: NEUTRAALNE J�TKAJA |
V2: `m�juv `p�hjus, siis te saate selle raha `tagasi.   | KYJ: INFO ANDMINE |
 [{-}] kindlustuse `kaudu,   | KYJ: INFO ANDMINE |

H: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V2: mitte `meie=k��st aga=siis [juba] kindlustus`firmaga,    | KYJ: INFO ANDMINE |
sest teil on=nagu   | IL: P�HJENDAMINE |

H: [jah.]   | VR: NEUTRAALNE J�TKAJA |
V2: .hh meie kaudu `s�lmitud nendega omaette siuke `leping, et ne[mad] `vastutavad   | IL: P�HJENDAMINE |

H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
V2: selle=eest=et teie=oma (.) rahast `ilma ei=j�eks.   | IL: P�HJENDAMINE |

H: selge.    | VR: NEUTRAALNE PIIRITLEJA |
(.) ja siis on: `t�isprotsent v�i on mingi: `osa.   | KYE: T�PSUSTAV |    | KYE: ALTERNATIIV |
V2: .hh [ee see peaks] olema (.) t�hendab vat `siis hakkavad siis hakkab juba nii�elda   | KYJ: MUU |

H: [{see tagasisaamine}]   | IL: T�PSUSTAMINE |
V2: juba see `kindlustus hakkab `t��[le.]   | KYJ: MUU |

H: [aa,]    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
siis=on nagu `nendepoolne.   | KYE: T�PSUSTAV |    | KYE: VASTUST PAKKUV |
V2: jah=et t�hendab=et=et `kui on ikka nii=et t�esti kas teil `reisikaaslane haigestus=v�i[teie] `ise v�i [teie] `l�hikondlastega v�i teie `varale juhtus    | KYJ: INFO ANDMINE |

H: [jah]    | VR: NEUTRAALNE J�TKAJA |
[mhmh]   | VR: NEUTRAALNE J�TKAJA |
V2: mingisugune `kahju mist�ttu teie ei `saa minna reisile, [.hh] siis muidugi nad   | KYJ: INFO ANDMINE |

H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
V2: kompen`seerivad seda.   | KYJ: INFO ANDMINE |
 ja `�ldjuhul ikkagi t�iel `m��ral kui {see `asi on t�b} [`reis] j�i `�ra ja raha on vaja `tagasi saada.    | KYJ: ALTERNATIIV: �KS |
[mitte] mingisugune noh .hh {-}   | IL: T�PSUSTAMINE |

H: [mhmh]    | VR: NEUTRAALNE J�TKAJA |
[mhmh]   | VR: NEUTRAALNE J�TKAJA |
V2: kindlustuse summa pealt l�heb siis `maha kuna kindlustus on juba siis juba `j�us ju.   | IL: T�PSUSTAMINE |

H: jah.   | VR: NEUTRAALNE J�TKAJA |
V2: .hh aga kui on niuke=lugu noh=et `eksam tuleb ja mina ei `teadnud siis $ ei=`loe. $  | KYJ: INFO ANDMINE |
 [hehe] | IL: PEHMENDAMINE |   

H: [$ ei muidugi, l(h)oomu`likult. $]   | VR: HINNANGULINE VASTUV�TUTEADE |
V2: $ see ei=`loe. $   | IL: �LER�HUTAMINE | 

H: selge see.    | VR: HINNANGULINE VASTUV�TUTEADE |
(.) mt igal=juhul suur `t�nu.    | RIE: T�NAN |
ma siis [`m�tlen] `j�rele=ja=   | RIE: L�PUSIGNAAL |
V2: [jah]   | RIJ: L�PETAMISE VASTUV�TMINE |
V2: =vaadake se=asi seal `j�rele ja=sis pange=`kirja.   | DIE: ETTEPANEK |

H: jah.   | DIJ: N�USTUMINE |
V2: `teeme [`nii.]   | RIE: L�PUSIGNAAL |
 kena | RIE: L�PUSIGNAAL |
`n�ge[mist.] | RIE: H�VASTIJ�TT |
H: [jah]    | RIJ: L�PETAMISE VASTUV�TMINE |
[ jah]    | RIJ: L�PETAMISE VASTUV�TMINE |
H: ait�h, | RIE: T�NAN | 
n�ge`mist. | RIJ: VASTUH�VASTIJ�TT |



7


   

 
   

 
   

 
   

 
   

 
   

 
   

 
   

 
   

 

