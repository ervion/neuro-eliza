((373a6 telefonik�ne reisib�roosse))
((�htlustas Andriela R��bis 2.12.2003))

((kutsung))   | RIE: KUTSUNG |
V: Euroreisib�roo   | RIJ: KUTSUNGI VASTUV�TMINE |    | RY: TUTVUSTUS |

 tere=p�evast?   | RIE: TERVITUS |
H: tere p�evast.   | RIJ: VASTUTERVITUS |
V: tere   | RIJ: VASTUTERVITUS |
H: mul on selline `k�simus,    | YA: EELTEADE |
kas teie b�roo `Kreekasse ka reise korraldab.   | KYE: JUTUSTAV KAS |
V: .hh ee te m�tlete `bussireise v�i: > `lennureise. <   | KYE: ALTERNATIIV |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: bussi?   | KYJ: ALTERNATIIV: �KS |
V: `ise me n��d `Kreekasse: `korraldame reise  .hh n�iteks `eelmine `suvi oli meil `reis Kreekasse, .hh aga: meil on k�ik need `bussireisid,     | KYJ: INFO ANDMINE |
H: jaa?   | VR: NEUTRAALNE J�TKAJA |
V: ja kui te n��d soovite midagi `Kreekast siis me: `vahendame ainult neid {-} Aurinko Matkad Vrissi, .hh ja=ni=`edasi et kui `sealt midagi vaadata neid `puhkusereise `lennukiga.   | KYJ: INFO ANDMINE |
H: lennukiga.   | PPE: �LEK�SIMINE |     | KYE: VASTUST PAKKUV |

 ((imestunult))
V: mhmh   | PPJ: L�BIVIIMINE |     | KYJ: JAH |
H: ahah    | VR: PARANDUSE HINDAMINE |  | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(.) a kas te oskate �elda palju selline `reis kuskil ma=i=tea mis=se mis=se `kestvus on kas `kaks n�dalat v�i `kolm v�i mm palju see `maksma l�heb.   | KYE: AVATUD |
V: p�him�tteliselt on nagu: valida kas `�ks n�dal v�i `kaks n�dalat, .hh   | KYJ: INFO ANDMINE |
H: jah   | VR: NEUTRAALNE J�TKAJA |
V: ja: m pr�egu no �tleme `keskmine `hind on: oleneb n��d ho`tellist eksole see on kuskil .hh `�heksa (.) `tuhat (.) keskmine `hind.   | KYJ: INFO ANDMINE |
H: n�dal.   | PPE: �MBERS�NASTAMINE |     | KYE: VASTUST PAKKUV |
V: jaa.   | PPJ: L�BIVIIMINE |     | KYJ: JAH |
(0.5)
H: mhmh    | VR: PARANDUSE HINDAMINE |   | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.5) ja: mis mis mis=mis `linnu sis seal k�lastatakse v�i v�i=mis=se (.) prog`ramm nagu umbes `on.   | KYE: AVATUD |
V: .hh e t�hendab see on n�t seda peab n�d konkreetselt `vaatama noh (.) ma `�tlen pr�egu=et=et seda peab vaatama    | KYJ: INFO PUUDUMINE |
H: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: [.hh] kas {-} v�i `Aurinkol, aga need on `p�him�tteliselt `puhkuse`reisid.   | KYJ: INFO ANDMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
V: [et] kui te n�iteks sinna lendate `kohale te l�hete ho`telli, .hh ja `puhkate n�dal aega seal: kuskil `rannas eksole ja `kohapealt on v�imalik tellida sis ekskursi`oone,    | KYJ: INFO ANDMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
V: [.hh] aga neid on `praegu siin noh sis ma peaks lihtsalt `vaatama `mis ekskursioone seal `pakutakse ja `millal te tahate minna ja noh `pr�egu momendil ma ei oska teile `�elda seda.   | KYJ: INFO PUUDUMINE |
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) aga kuidas teil I`taaliaga on.    | KYE: AVATUD |     | TVE: PAKKUMINE |

kas n- (0.5) kas nagu on `sama olukord.   | KYE: JUTUSTAV KAS |

(.)
V: .hh n:oh (.) > p�him�tteliselt `k�ll jah <    | KYJ: JAH |     | TVJ: VASTUV�TMINE |
on j�llegi v�imalik `praegu n�iteks tellida: j�llegi kas Aurinkolt v�i {-}  | KYJ: INFO ANDMINE | 

 .hh et `meil nagu `endal ei=ole `pakkuda Itaalia `reisi.   | IL: P�HJENDAMINE |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
V: {momendil}   | IL: T�PSUSTAMINE |
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) aga need on siis k�ik `lennureisid �hes�n[aga.]   | PPE: �LEK�SIMINE |     | KYE: VASTUST PAKKUV |
V: [jaa] need on k�ik lennu`reisid, | PPJ: L�BIVIIMINE |     | KYJ: JAH |
puhkuse`reisid.   | PPJ: L�BIVIIMINE |     | IL: T�PSUSTAMINE |
H: mhmh  | VR: PARANDUSE HINDAMINE |  | VR: NEUTRAALNE VASTUV�TUTEADE |
 (1.2) nojah    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) aga mis=mis `teil nagu: see mis nagu `bussi reisidest k�ige sellisem (.) `l�hedasem sellele v�iks [`olla sis]   | KYE: AVATUD |     | TVE: PAKKUMINE |
V: [.hh no] `praegu n�iteks ee jaanuar `veebruar me pakume `bussireisidest=ee Sankt `Peterburi ja: Sereena v-`veeparki.    | KYJ: INFO ANDMINE |  | TVJ: VASTUV�TMINE |
 ja ongi `k�ik.   | IL: KOKKUV�TMINE |
H: ahhaa    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
selge.   | VR: NEUTRAALNE PIIRITLEJA |
V: jah et `suve poole kindlasti `tekib sinna meile Pa`riis ja .hh ja I`taaliat v�ibolla tekib, .hh aga `pr�egu `k�ll mitte.   | KYJ: INFO ANDMINE |

(0.5)
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.2) nojah     | VR: NEUTRAALNE VASTUV�TUTEADE |
mhe $ ait�h   | RIE: T�NAN |
 eks=ma=sis `helistan veel millalgi t(h)eile. $   | RIE: L�PUSIGNAAL |
V: jaa?     | RIJ: L�PETAMISE VASTUV�TMINE |
n�gemist.=   | RIE: H�VASTIJ�TT |
H: =n�gemist.   | RIJ: VASTUH�VASTIJ�TT |


   

 

