((200a5 teek�simine))
((�htlustas Andriela R��bis 28.12.2003))

A: kuidas ma saaksin `Mustam�ele.    | KYE: AVATUD |
(1.0)
B: `Mustam�ele.   | KYJ: EDASIL�KKAMINE |
 te peate minema (.) `kaubamaja juurde.   | KYJ: INFO ANDMINE |
A: mhmh    | VR: NEUTRAALNE J�TKAJA |
(2.2)
B: k�ige parem on `minna ma m�tlen=n�d,    | KYJ: EDASIL�KKAMINE |
(1.2) siin on se `Stokman, ta v�ite `siit ka minna et l�hete siit n�d `otse, (.) eksole.   | KYJ: INFO ANDMINE |
A: mhmh   | VR: NEUTRAALNE J�TKAJA |
B: ja siis `p��rate �ra p��rate Narva `maanteni sealt, ja l�hete m��da trammiliini=m��da `otse kuni `kauba`majani.   | KYJ: INFO ANDMINE |
A: jah   | VR: NEUTRAALNE J�TKAJA |
B: kaubamaja `teate kus on.   | KYE: SULETUD KAS |    | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
A: jaa?   | KYJ: JAH |
B: no vot ja sis (.) `kaubamaja juurest t�hendab `eespoolt l�heb ja `tagant l�hevad `ka need `trollid,   | KYJ: INFO ANDMINE |
A: mhmh   | VR: NEUTRAALNE J�TKAJA |
B: te v�ite siit `ka nimodi minna m��da `trammiliini=m��da l�hete otse `edasi, (.) trammiliin p��rab `�ra aga teie l�hete kuni kaubamajani `v�lja sinna.   | KYJ: INFO ANDMINE |
 (.) te juba `n�ete kaubamaja `paistab [seal.]   | KYJ: INFO ANDMINE |
A: [jah]   | VR: NEUTRAALNE VASTUV�TUTEADE |
 ja mis `number trolliga nagu.   | KYE: AVATUD |  | KYE: T�PSUSTAV |
B: no sinna l�heb number `kolm l�heb, sis l�heb number `�ks ja number `kaks l�[hevad] ka oleneb kuhu te otseselt tahate `minna.  | KYJ: INFO ANDMINE |   | KYE: AVATUD |  | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
A: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
A: ma sooviksin `Tammsaare `teele minna.   | KYJ: INFO ANDMINE |    
B: * m `Tammsaare tee ma m�tle(h)n,*      | KYJ: EDASIL�KKAMINE |
(0.5) see kas seal on seal kus see `instituut oli=v�i.   | KYE: SULETUD KAS |    | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 (.) `tipp v�i={noh.}   | PA: ENESEPARANDUS |
A: seal on: maja- ja sis `majanduskool jah.   | KYJ: JAH |
B: no=vot=`ausalt=�eldes > ma enam ei `m�leta, ma olen ise seal `�ppind aga < (.) ee `Ehitajate teel,    | KYJ: INFO PUUDUMINE |
(.) ei tip asus `Ehitajate teel.    | PA: ENESEPARANDUS |
(0.5) `Tammsaare.   | KYJ: EDASIL�KKAMINE |
(.) `Tammsaare ahaa, (.) sinna on vist parem `kahega s�ita.    | KYJ: INFO ANDMINE |     | KYJ: KAHTLEV |
(0.5) �ldiselt `mina arvan `nimodi=et (0.5) a `kolmega saab `ka.       | KYJ: INFO ANDMINE |     | KYJ: KAHTLEV |
(0.5) `istuge sinna, t�hendab kui sinna `l�hete, `k�sige noh (.) kes seal `seisab.   | DIE: ETTEPANEK |
A: mhmh   | DIJ: PIIRATUD N�USTUMINE |
B: ta `�tleb teile, st kolm s�idab ka nagu `m��da oleneb kuskohal see `Tammsaares=`on.    | IL: P�HJENDAMINE |
(0.5)
A: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) {eks `h�sti siis.}   | RIE: L�PUSIGNAAL |
B: jah   | RIJ: L�PETAMISE VASTUV�TMINE |
A: ait�h=[teile.]   | RIE: T�NAN |
B: [palun.]   | RIJ: PALUN |

   

 

