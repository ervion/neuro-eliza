((231a7 kosmeetikapood))
((�htlustas Andriela R��bis 10.12.2003, kontrollitud 27.10.2006))

O: tere   | RIE: TERVITUS |
M: tere   | RIJ: VASTUTERVITUS |
O: ee ega teil ei=ole mingit `ee=vitamiiniga `kreemi.    | KYE: JUTUSTAV KAS |
(.) `n�okreemi {ma tahaks.}   | IL: T�PSUSTAMINE |
M: `dee v�i `ee.   | KYE: ALTERNATIIV |   | PPE: MITTEM�ISTMINE |
O: =ee.   | KYJ: ALTERNATIIV: �KS |  | PPJ: L�BIVIIMINE |
M: mh   | VR: NEUTRAALNE VASTUV�TUTEADE |  | VR: PARANDUSE HINDAMINE |
 (0.8) ((otsib)) mm | KYJ: EDASIL�KKAMINE |
(...) [{-}]   | YA: PRAAK |
O: [`sellel] firmal [on `ka]   | KYE: JUTUSTAV KAS |
M: [`aa] (.) ja `ee. | KYJ: EDASIL�KKAMINE |
(.) on sellel?   | KYJ: INFO ANDMINE |
O: mhmh   | VR: NEUTRAALNE J�TKAJA |
M: sis siin on (.) provitamiin `aa.   | KYJ: INFO ANDMINE |
(1.2)
O: t�hendab sellel `Spliss Formula `firmal on `ka `ee=vitamiinid nihukesed asjad.   | SEE: V�IDE |
M: {---}   | YA: PRAAK |
 (...) sellel {-} Splissil oli see `kehakreem oli ee=vitamiiniga.   | SEJ: PIIRATUD N�USTUMINE |
 (1.0) sest `n�okreemil {-}   | SEJ: MUU |
(.)
O: selline `v�ike purgike nagu oli   | SEE: V�IDE |
M: aa, | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
`seda meil [on isegi `olnud jah.]   | SEJ: N�USTUMINE |
O: [kuskil ma `ajakirjas n�gin.]   | SEE: V�IDE |
M: v�ibolla `k�ll jah   | SEJ: PIIRATUD N�USTUMINE |
O: mhmh   | VR: NEUTRAALNE J�TKAJA |
(0.8)
M: siin n�okreemid on {-} (1.0) {meil seda} ei=`ole.   | DIE: PAKKUMINE |
(1.5)
O: ei seda ma (.) ei=`taha   | DIJ: MITTEN�USTUMINE |
 aga rohkem nagu ei=ole [{-} jah]   | KYE: VASTUST PAKKUV |
M: [see on vist `v�ga] looduslik [{-}]   | SEE: ARVAMUS |
O: [mhmh]   | SEJ: PIIRATUD N�USTUMINE |
(1.5)
M: {---} (...) [ > siis ei=ole. < ]   | KYJ: N�USTUV EI |
O: [mhmh]   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) .jah   | VR: NEUTRAALNE PIIRITLEJA |
 (.) ait�h=teile.   | RIE: T�NAN |
M: palun.   | RIJ: PALUN |
O: head aega.   | RIE: H�VASTIJ�TT |


      
