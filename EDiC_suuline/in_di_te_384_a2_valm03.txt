((384a2 telefonik�ne reisib�roosse))
((�htlustas Andriela R��bis 3.12.2003))

  ((kutsung)) | RIE: KUTSUNG |
V: Gloobus reisid   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
 tere   | RIE: TERVITUS |
H: tere?   | RIJ: VASTUTERVITUS |
 .hh ma sooviksin: (.) l�hemat informatsiooni v�ibola=selle (.) .hh teie `reisi kohta mida te `pakute `Gr�n Ka`naariale.   | DIE: SOOV |
V: jaa?   | VR: NEUTRAALNE J�TKAJA |
H: mt et=�� k�igepealt (.) {et} kus=se �ltse $ `asub. $  | KYE: AVATUD |
  heh   | YA: MUU |
(.)
V: Gr�n Ka`naaria kus `asub.   | PPE: �MBERS�NASTAMINE |     | KYE: VASTUST PAKKUV |
 ((imestunud teise teadmatusest))
H: jaa?   | PPJ: L�BIVIIMINE |     | KYJ: JAH |
V: `asub seal kus on k�ik Kanaari `saared Aafrika (.) ranniku `��res.   | KYJ: INFO ANDMINE |
 (1.0) ja �sna soe `koht on, (1.2) on=hh   | KYJ: INFO ANDMINE |
(0.8)
H: jaa?   | VR: NEUTRAALNE J�TKAJA |
V: =et=et=et meie hotellid on seal Gran Kanaaria `l�una`osas k�ik.   | KYJ: INFO ANDMINE |
 (0.5) {-} ja Maspalomas ja San Augustinis.   | KYJ: INFO ANDMINE |
H: =mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 .hh ja siin on `m�rgitud=et=ee (.) kahe kahest kuni `viie t�rni hotellini=   | YA: REFERAAT |
et kas see on (.) n�t (.) iga (.) reisija=ise (.) enda `valida=et millist ta n�d `valib v�i .hh   | KYE: SULETUD KAS |     | KYE: T�PSUSTAV |
V: jaa {ikka}     | KYJ: JAH |
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (0.5) ja umbes mis: `hinnaklassis need hotellid v�iks `olla sis [{-}]  | KYE: AVATUD |  
V: [`kahe]t�rnised akkavad n��d=hh (.) n�d=n�d=n��d    | KYJ: INFO ANDMINE |
(.) odot=a=mis `kuup�eval te=tahtsite    | KYE: AVATUD |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.5) {-}   | YA: PRAAK |
H: ee ma `kuup�eva veel   | KYJ: MUU |
 ma `vaatan siin et teil on alates �eksateistk�mnendast   | KYE: SULETUD KAS |
(.)
V: jah ok`toobrist on meil   | KYJ: JAH |
H: =kuni   | YA: MUU |
V: {-} (.) [aga]   | YA: MUU |
H: [et=noh] kuup�eva `veel ei oska `�elda p�ris t�pselt et ma   | KYJ: INFO PUUDUMINE |
V: jaanuari `algus v�i `nii jah   | KYE: VASTUST PAKKUV |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: no (.) arvatavasti jah.   | KYJ: JAH |
V: aa.   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 .hh siin=on=n�d nii=et=et `aastavahetuse hinnad ei=`olnud `esime=n�dal on natukene `kallimad=et=et `�heteistk�mnendast jaanuarist (.) l�hvad innad `odavamaks, ma=m�tlen sis `neid hindasid sin pr�egu.   | KYJ: INFO ANDMINE |
(0.5)
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: on n��d n�iteks `bungalod on siin `�heksa=tuhat seitsesada �heksak�mend=`viis akkab, (0.5) kui=n�d `kahekesi `minna. | KYJ: INFO ANDMINE |
(.) ja: v�rdluseks `kolmet�rnihotell `hommikus��giga �ksteist=tuhat �eksada=�eksak�mend=`viis.   | KYJ: INFO ANDMINE |
 see on n�d koos `lennu `majutuse (.) ja (.) `esindajateenustega=et `kindlustus on peale `selle vel.   | KYJ: INFO ANDMINE |
(1.0)
H: jaa?   | VR: NEUTRAALNE J�TKAJA |
V: ja siis on n�d `neljat�rnihotellid on sin `neliteist tuhat.   | KYJ: INFO ANDMINE |
(0.5)
H: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 .hh (.) ja: (.) nagu ma=`aru=saan=et `aeg sis v�ib ise: `valida=et   | KYE: VASTUST PAKKUV |   
V: jah.   | KYJ: JAH |
H: ahah?   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (1.0) aga: (0.8) mt mingisuguseid (.) `soodustusi on `ka v�i ei=`ole.   | KYE: JUTUSTAV KAS |    
 heh [{et kuidas}]   | KYE: MUU |
V: [kui n��d] t�pset `kuup�eva ei=oska `�elda sis=me=i=`oska nagu `pakkuda ka [{-}]  | KYJ: KEELDUMINE |
H: [{ahah}]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) et �hes�naga ma=san=aru=et=see aast- kuni �he`teistk�mnendast �heteistk�mnenda `jaanuarini=et=siss �he`teistk�mnendast `jaanuarist peaks sis olema `odavam.   | PPE: �MBERS�NASTAMINE |   | KYE: VASTUST PAKKUV |
V: jaa, (.) jaa.   | PPJ: L�BIVIIMINE |   | KYJ: JAH |
H: * ahah? *   | VR: PARANDUSE HINDAMINE |   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: {-} see aasta`vahetus ja `puhkuse ajad et need on nagu tavaliselt ostetakse k�ik `�ra ja seal nagu `soodustusi ka palju ei=`tule.   | KYJ: INFO ANDMINE |
(.)
H: ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
.hh aga kas see `aasta`vahetus n�iteks aastavahetus kas=seal=on aastavahetuse prog`ramm seal kohe v�i mis=sis on iga inimese enda `teha=et mida ta `teeb seal.   | KYE: ALTERNATIIV |    
 .hh v�i: on teil kohe selline   | KYE: ALTERNATIIV |   
(0.5)
V: ei sellist nagu: (.) konkreetset:=�� `kava n�d ei=`ole seal,   | KYJ: ALTERNATIIV: EITAV |
 (.) et=kui=te=olete nelja `viiet�rnihotellis seal on jah need `�htus��gid ja (.) `gaalad on seal hinna `sees, (.) mt a:ga muidu eraldi `k�ll ei=ole.   | KYJ: INFO ANDMINE |
(1.0)
H: ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
.hh aga kas see `aasta`vahetus n�iteks aastavahetus kas=seal=on aastavahetuse prog`ramm seal kohe v�i mis=sis on iga inimese enda `teha=et mida ta `teeb seal.   | KYE: ALTERNATIIV |    
 .hh v�i: on teil kohe selline   | KYE: ALTERNATIIV |   
(0.5)
V: ei sellist nagu: (.) konkreetset:=�� `kava n�d ei=`ole seal,   | KYJ: ALTERNATIIV: EITAV |
 (.) et=kui=te=olete nelja `viiet�rnihotellis seal on jah need `�htus��gid ja (.) `gaalad on seal hinna `sees, (.) mt a:ga muidu eraldi `k�ll ei=ole.   | KYJ: INFO ANDMINE |
(1.0)
H: ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.8) noo? | VR: NEUTRAALNE PIIRITLEJA |
pr�egu sis vist ei=`olegi rohkem midagi `k�sida.   | RIE: L�PUSIGNAAL |
 �kki teil on: (.) `interneti`aadress.   | KYE: JUTUSTAV KAS |
V: jaa,   | KYJ: JAH |
 veve`vee punkt gloobus`reisid punkt e-ee.   | KYJ: INFO ANDMINE |
H: mt ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(.) ait�h?   | RIE: T�NAN |
 suur `t�nu teile.   | RIE: T�NAN |
V: palun   | RIJ: PALUN |


