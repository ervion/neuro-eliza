((733_8a reisib�roo))
((�hestas Andriela R��bis 4.03.2010))

((kutsung))   | RIE: KUTSUNG |
V: Nordiktuurs=   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
tere=   | RIE: TERVITUS |
Aive=Aus=kuuleb   | RY: TUTVUSTUS |
(0.4)
H: tere.    | RIJ: VASTUTERVITUS |
(0.9) eeee `oleks `soov s�ita: `Helsinkisse `NELJANDAL `m�rtsil.   | DIE: SOOV | 
V: * mhmh? *    | VR: NEUTRAALNE J�TKAJA |
(0.5)
H: `hommik:use laevaga?    | IL: T�PSUSTAMINE |
(0.4)
V: * mhmh? *   | VR: NEUTRAALNE J�TKAJA | 
H: eeee ka:s=n�d eeeee > `internetist=`leitud=`info: vastab=`t�ele=et=ta=l�heb seitse=`kolmk�mend. <   | KYE: SULETUD KAS |
(0.3) mingi `Staar=   | IL: T�PSUSTAMINE |
V: = `Staar=jaa?   | KYJ: JAH |
 se=on `�ige.   | KYJ: JAH |
(.)
H: mhmh?=   | VR: NEUTRAALNE J�TKAJA |
V: = * seitse kolmk�mend.   | KYJ: JAH |
 (0.3) mhmh? *   | VR: MUU |
 .hhh ks=`�ks suund.    | KYE: VASTUST PAKKUV | | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4)
H: .hhhhhh  (0.6) > `tagasi `ka samal=p�eval=   | KYJ: MUU |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
aga=ma=i: `vaadanud mis=`kell ta=`tuleb. <   | KYE: AVATUD | 
V: * {olgu,} *   | VR: MUU |
 kas=ka `Staar.   | KYE: SULETUD KAS |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 seitse`teist null=`null: `Galaksi on:: .hhhh e=kaheksateist=`kolm`k�mend,  .hh sis=e  tuleb (0.5) < kak:sk�mend `kol:mk�mend >    | KYJ: INFO ANDMINE |
 > * oi=ma `vaatan kiiresti=et * <   | KYJ: EDASIL�KKAMINE |
(0.3)
H: ei=se `seitseteist::=h   | KYJ: INFO ANDMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: null [null]   | IL: T�PSUSTAMINE | 
H:  [on] (.) on=hea=jah    | KYJ: INFO ANDMINE | | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: * mhmh? *   | VR: NEUTRAALNE VASTUV�TUTEADE |
 .hhhhh ja: kelle=`nimele, (0.3) bro`neerida.   | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: �ks=`hetk.    | KYJ: EDASIL�KKAMINE |
((H r��gib kellegi teisega vaikselt))
H: .hhhhh eeee t�hendab `�ks `broneering l�heb `kindlasti=aga teine l�heb `ka=   | KYJ: INFO ANDMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 aga=kas::=eee=`saab=sis `�ra �elda selle.      | KYE: JUTUSTAV KAS |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
V: mmmmmmm (0.3) kuna=piletid=peab  {---} (0.7) siis se=on `nelja .hhh * `�ks `kaks `kolm *    | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
((loeb sosinal omaette)) (2.5) .h=ei.   | KYJ: EI |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 (0.5) > sis=peab trahvid.     | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 (.) nad saate �ra `�elda aga=te=peate=`trahvi maksma. <     | KYJ: INFO ANDMINE |      | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) kui `t�na broneerida   | KYE: AVATUD |      | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: > `isegi=ku=`t�na `broneerida < sis=se=pilet=mis ma n�d `teen, se=peab=ka t�na  kohe v�lja osta.   | KYJ: INFO ANDMINE |      | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: aga=se=tulep: ��� `�likooli `arvega selles m�ttes:::=ee=     | IL: T�PSUSTAMINE |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: =sis=ei=ole `midagi.   | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 (0.7) sis `siis=on noh `vastavalt `lepingule.    | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.2) et: `vastavalt n:ii=kuidas=`on see: `makse `t�htaeg.   | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 (0.9) aga=kui=se=`teine inimene ikka se- (0.1) `kindel ei=`ole,   | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: mhmh,    | VR: NEUTRAALNE VASTUV�TUTEADE |
V: [ja: a:ga]   | YA: MUU |
H: [sis=pra]egu ei broneeri.    | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |     | DIE: MUU |
V:  kui=ta: `trahvi ei=taha `maksta sis �rme bro`neerime.    | DIJ: N�USTUMINE |
(1.4)
H: s:elge,   | VR: NEUTRAALNE PIIRITLEJA |
 (0.5) eee �ks=`hetk?    | KYJ: EDASIL�KKAMINE |
(...) ((H r��gib k�rvalseisjatega))
H: .hh `teeme=nii=et `v�tan `�he.    | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |   | DIE: SOOV |
(0.3)
V: `v�tate �he [sis]   | KYE: VASTUST PAKKUV |  | PPE: �LEK�SIMINE |
 kelle `nimele=   | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: [.jah]   | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
H: =`Jaana Kasesalu    | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V: Jaa:na:   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
H: `Jaana=nagu:: j�nes.   | KYJ: MUU |     | PPJ: L�BIVIIMINE | 
V: mhmh?   | VR: NEUTRAALNE VASTUV�TUTEADE | | VR: PARANDUSE HINDAMINE |
(0.4) Kasesalu.   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
H: just.   | KYJ: JAH | | PPJ: L�BIVIIMINE |
(1.0)
V: s�nni`aeg    | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8)
H: kuuskend=`neli.    | KYJ: INFO ANDMINE | | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4)
V: e=`p�ev=ja=`kuu?   | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: viis`teist=null=`kolm.   | KYJ: INFO ANDMINE | | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: viisteist=null=`kolm kuuskend=`neli.    | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.7) `nii=se=on=siis neljas `m�rts, ja=`tagasi ka sama=�htul=   | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE | 
H: =jah    | KYJ: JAH | | PPJ: L�BIVIIMINE |
(0.2)
V: * neljas m�rts *   | VR: NEUTRAALNE VASTUV�TUTEADE |  | VR: PARANDUSE HINDAMINE |
 .hhhh `kontakt `number palu:n::    | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.9)
H: viis=`kaks,   | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: viis=`kaks,   | VR: NEUTRAALNE J�TKAJA | 
H: `kaks=kuus=`neli   | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: `kaks=kuus=neli   | VR: NEUTRAALNE J�TKAJA | 
H: `kuus `kolm.    | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.7)
V:  aa `kes=on=sis=`maksja.   | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: Tartu `�likool.     | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V:  `mis osakond.   | KYE: AVATUD |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: ee ja > `ei=`olegi osakonda `vaja `Suurkooli=`�heksateist=v�b=panna. <       | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
[v�i=`on v�i=on osa-]   | YA: MUU |
V: [{---} `ainult sis] `Tartu `�likool v�.    | KYE: VASTUST PAKKUV |    | PPE: �MBERS�NASTAMINE |
(0.2)
H: no > `v�ib=`osakonna=ka=`panna < `liiva: ja `mustmullateaduse (0.2) `osa[kond]   | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE | 
V: [ < * liiva] ja: (0.4) mu::st- * >   | VR: NEUTRAALNE VASTUV�TUTEADE | 
H: `ei mitte `osakond=vaid `instituut.    | KYJ: INFO ANDMINE |   | PA: ENESEPARANDUS |    
V: < * mull:atea:duse instituut. * >    | KYE: VASTUST PAKKUV |   | PPE: �MBERS�NASTAMINE |
(0.3)
H: mhmh?   | KYJ: JAH | | PPJ: L�BIVIIMINE | 
V: kas=`teie: `oletegi: `Jaana.    | KYE: SULETUD KAS |
(0.4)
H: > mina=`olengi `Jaana `jah   | KYJ: JAH | 
mhe[mhemhe]   | YA: MUU |
V: [hehe] siis: ������ (.) `meili `aadress > ma=v�in=`sinna `kinnituse `saata teile. <    | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.2)
H: > e=`Jaana punkt `Kasesalu. <    | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.2) ja: `siss `�likooli (.) uut=ee=ee   | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8) .hhhhh ja siss=e= `kindlustust ka `reisikindlustust `ka sis `samale arvele.    | DIE: SOOV |
(.) `pa[lun.]   | IL: MUU |
V: [mhmh] �ks=`p�ev=sis.    | KYE: VASTUST PAKKUV |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.5)
H: eeeeee `EI:::    | KYJ: EI |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
siis oleks `vaja: ka: `esmasp��v sisse.    | IL: T�PSUSTAMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.2)
V: e:t `neljas m�rts [{-}]   | KYE: MUU |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: [`kol]mas m�rts.   | KYJ: MUU | | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 > `neljas=m�rts=on `teisip��v. <      | IL: T�PSUSTAMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
et=siis `kolmandast:   | IL: �LER�HUTAMINE | | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: `kolmandast tahate   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
H: mhmh?   | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
V: `kolmandast=kuni=`neljandani.   | KYE: VASTUST PAKKUV | | PPE: �MBERS�NASTAMINE |
(.)
H: .h `kuni `viiendani igaks juhuks=h?     | KYJ: MUU | | PPJ: L�BIVIIMINE | 
V: * kuni `viiendani [{nimo]di} `tervisekindlustust *   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
H: [mhmh]    | KYJ: JAH |  | PPJ: L�BIVIIMINE |
(0.4)
V: aga `laevpilet on ikka `neljas.    | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE |
(0.3) `hommik `minek �htu `tulek.   | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE | 
H: v�i=`tegelikult `vist ei ole `vaja=sis seda: (0.3) > viiendat no=`t�hendab se=on=`selles =m�ttes `reisi `t�rke kins(h)u- `kindlustust (1.0) oleks `vaja `igaks `juhuks.     | KYJ: MUU |       | PPJ: L�BIVIIMINE |
(0.8)
V: sii:s: peaks `ikkagi panema ikkagi `ikka `�ks `kuup�ev `neljas.   | SEE: ARVAMUS |
(1.1)
H: mt=aga=ma=`akkan Tartust `s�itma Tallinnasse `kolmandal.     | SEJ: MITTEN�USTUMINE |
V: aa   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
`kolmandal=v�   | VR: NEUTRAALNE VASTUV�TUTEADE |
  (0.3) {ses} jah panen=sis `kolmas.=      | SEE: MUU |
H: =mhmh,   | SEJ: N�USTUMINE |
(0.3)
V: kolmas=kuni=`neli m:�tleks=`nimodi [ikkagi.]   | IL: �LER�HUTAMINE |
H: [mhmh]   | VR: NEUTRAALNE VASTUV�TUTEADE |
 mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.4)
H: et=ku=`neljandal midagi juhtub et=sis: .hhhh      | SEE: MUU |
V: jah [{sis=se `reisit�rge on ka}]      | SEJ: N�USTUMINE |
H: [sis=ta=on=niikuinii mhmh]      | SEE: MUU |
 jaa=jaa   | VR: MUU |
V: * medi`tsiin `pluss reisi`t�rge *   | IL: KOKKUV�TMINE |
(2.3)
V: eeeee `selge   | VR: NEUTRAALNE PIIRITLEJA |
 ma=teen=`�ra ja panen::::: `kindlustuse `ka teile meili=peale kui `soovite ja `arve=ka=sis=k�ik `meili peale.   | DIJ: MUU |   | DIE: PAKKUMINE |
(0.9)
H: ee=      | YA: MUU |
V: =v�i `prindin `v�lja=ja tulete siit `l�bi.   | DIE: PAKKUMINE |
H: `ei,   | DIJ: MITTEN�USTUMINE |
`meili=peale v�ib saata.      | DIJ: N�USTUMINE |
 `see=ju `toimib [�] kui `mina `v�lja prindin=juba `p�ris `arvena eksole   | KYE: VASTUST PAKKUV |
V: [jah]   | KYJ: JAH |
 jaa   | KYJ: JAH |
 .jah   | KYJ: JAH |
 `t�pselt=nii   | KYJ: JAH |
H: ja: ma=olen=`doktorant nii=et �=`vist on nagu mingi `soodustus `ka sellele: reisikindlustusele=v�   | KYE: JUTUSTAV KAS |
(0.3)
V: mmmm=`kas=se=t�hendab=et `�li�pilane.   | KYE: VASTUST PAKKUV |   | PPE: �MBERS�NASTAMINE | 
H: jah   | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
V: isik`kaart=on.    | KYE: SULETUD KAS |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.3)
H: eeee se=n�d p�ris ei=[`ke]hti=aga dokto`rant olen=ma=`k�ll.     | KYJ: MUU |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: [jah]   | VR: NEUTRAALNE J�TKAJA |
 sest=et=ta=b nagu `ette ka n�idata nagu `olema mingi `kaart * noh et:::::=h * (2.3) t:ellin [{---}]   | IL: P�HJENDAMINE |
H: [a ma=`saan selle]    | SEE: V�IDE |
V: {-}   | YA: PRAAK |
(.)
H: jaa, ma=`saan=selle=ju `teha.   | IL: �LER�HUTAMINE |
(1.0)
V: ma=panen=selle `�li�pilase `soodustuse `peale [sis]   | KYE: VASTUST PAKKUV |      | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: [jah]   | KYJ: JAH |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 jah   | KYJ: JAH |      | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: lissalt `teineko- lissalt tuleb {selne kah- `k�sklus=}et e .hhhhhh `m�ni {v�tab} `teie=k�est seda:  `�li�pilase `t�endit k�sida.       | IL: P�HJENDAMINE |
(.)
H: ee `kes=v�ib `k�sida.    | KYE: AVATUD |
(0.4)
V: noo mq `Salva.    | KYJ: INFO ANDMINE |
 (0.5) ku=n�iteks e `l�hebki: noh `juhtubki [selle=reisiga]=teil=`midagi {a: aka=te=}siss (.) tuleb `kahju `k�sitlus.       | KYJ: INFO ANDMINE |
H: [mhmh mhmh]   | VR: NEUTRAALNE J�TKAJA |
V: et=[��] (.) `noh (.) nagu `taotlete mingid summasid `tagasi.      | KYJ: INFO ANDMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
  mhmh   | VR: NEUTRAALNE J�TKAJA |
(0.4)
V: siis=nad v�ivad `teie=k�est=seda �li�pilas`t�endit `k�sida.=      | KYJ: INFO ANDMINE |
H: =jaa=jaa   | VR: NEUTRAALNE VASTUV�TUTEADE |
 ei=no `se=on: (.) `nii=v�i=teisiti on ka �l- `�li�pilas`pilet `olemas=et      | YA: INFO ANDMINE |
V: * noh *   | VR: MUU |
 (0.5) okei=   | VR: NEUTRAALNE PIIRITLEJA |
[sis {�li�pilaste} see (.) `soodustus]=sis.   | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE | 
H: [jah   | KYJ: JAH |    | PPJ: L�BIVIIMINE |
(.) et see`staatus mul on `kindel.]    | IL: �LER�HUTAMINE |    | PPJ: L�BIVIIMINE |
jah.    | KYJ: JAH |    | PPJ: L�BIVIIMINE |
(.)
V: .hhhhhhhh aga::=ja `ainult `p�hjamaades olete=sis=`Soomes ainult jah=   | KYE: VASTUST PAKKUV |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: =jah   | KYJ: JAH |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 jah   | KYJ: JAH |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 [jah.]   | KYJ: JAH |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: [ma]=panin (.) `piirkonnaks `p�[hjamaad {---}]   | IL: T�PSUSTAMINE |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: [p�hjamaad jaa jaa]   | KYJ: JAH |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
(0.3)
H: .hh aga=kas `pileteid muidu=`on veel et et et sis `teine=oleks `sama`suguse    | KYE: SULETUD KAS |
(.)
V:  `ma=arvan=`k�ll=et=on   | KYJ: JAH |
lissalt `reisija=`pilet ega=`autot ei=ole teil.   | KYE: SULETUD KAS |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: mqm   | KYJ: N�USTUV EI |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4) ei=`ole.    | KYJ: N�USTUV EI |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.7)
V: mina=arvan=`k�ll=et=on `kohti.      | KYJ: JAH |
 t= kui=`midagi on ma=`elistan teile v�i:   | YA: LUBADUS |
 ja: .hhhhhh  `hetkel=sis=ma=teen aind `�he `pileti.   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: `igaks `juhuks `jah.   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
V: * mhmh? *   | VR: NEUTRAALNE VASTUV�TUTEADE |
 .hhhhh [okei]      | VR: NEUTRAALNE PIIRITLEJA |
H: [ja=`mis]=selle `hind on.   | KYE: AVATUD |
(.)
V: mmmmmmmmmm (0.5) ta=`tule::b=seal `kuskil:: (0.5) > ma=�ten=`umbes < [`kahe]ksa`sajaga=v�i `�heksa`sajaga.    | KYJ: INFO ANDMINE |
H: [mhmh]    | VR: NEUTRAALNE J�TKAJA |
(0.9)
H: `pluss kindlustus.   | KYE: VASTUST PAKKUV | 
V: jah    | KYJ: JAH | 
H: mhmh?   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) et=no=mingi=`tuhande `piires.   | KYE: VASTUST PAKKUV |   | PPE: �MBERS�NASTAMINE | 
V: jah    | KYJ: JAH |   | PPJ: L�BIVIIMINE | 
H: jah    | VR: NEUTRAALNE VASTUV�TUTEADE | | VR: PARANDUSE HINDAMINE |
(0.4)
V: okei:=   | VR: NEUTRAALNE PIIRITLEJA | 
H: =jah=   | VR: MUU | 
V: =`saadan=sis `sinna teile k�ik    | DIE: PAKKUMINE | 
H: jah   | DIJ: N�USTUMINE | 
V: `meili=peale.   | IL: T�PSUSTAMINE | 
H: ait�h.   | RIE: T�NAN | 
V: * .jah *   | VR: MUU |
      
      

