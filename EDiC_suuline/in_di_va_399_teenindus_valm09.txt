((399 reisib�roo))
((�hestas Andriela R��bis 1.09.2009))

M: tere?   | RIE: TERVITUS |
 (1.0) sooviksin natuke (.) `n�u k�si[da teie k�est.]   | YA: EELTEADE |
T: [jaa?    | VR: NEUTRAALNE J�TKAJA |
palun?]   | VR: NEUTRAALNE J�TKAJA |
M: {mhmh}   | VR: MUU |
N: ota (0.8) ma=v�tan `tooli.   | YA: MUU |
(...)
M: sihuke `mure on=et (0.8) et tahaks `Soome minna `jalg`ratastega.   | DIE: SOOV |
T: ahah?    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(1.0) mmh?   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) no sis v�ib jalgrattad `kaasa v�tta ja minna `laeva p(h)eale.   | DIJ: INFO ANDMINE |
N: aga (...) on konk`reetsem soov Ahvena- `Ahvenamaale.=   | DIE: SOOV |
T: =Ahvenamaale.   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
N: jah.=   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
T: =mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |     | VR: PARANDUSE HINDAMINE |
(.) et=ee `Ahvenamaale on v�imalik siis �le `Helsingi s�ites saada, ee on (0.5) ee `Helsingisse `Stokkolmi k�ivad `laevad (.) teevad Ahve- Ahvenamaal `peatuse.   | DIJ: INFO ANDMINE |
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: ja ja sis on ka `Turust (.) `Stokkolmi k�ivad laevad, teevad ka `Ahvenamaal `peatuse.   | DIJ: INFO ANDMINE |
M: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: nii et=ee et=ee (0.5) noh et see et seda (.) `Siljalaini laevad sis ee �tleme siit `Tallinast Helsingisse `Siljalaini: Super`siik�tiga saate poole hinnaga `pileti, (.) ja Helsingist ee sis � Mariaan`haaminasse (.) `ka Siljalaini suure laevaga `v�i siis `Turust Marian(h)haminasse $ suure laeva p��lt, $   | DIJ: INFO ANDMINE |
M: mhmh?    | VR: NEUTRAALNE J�TKAJA |
(0.8)
T: et kuidas kuidas (1.0) aga: on teil mingi `aeg paika pandud v�i    | KYE: JUTUSTAV KAS |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |   
N: noh m�tsim=et `augustis j�rsku.=   | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
T: =mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(...) ((vaatab arvutist j�rele)) laevade `s�idugraafik on siis `selline=et (0.5) et=ee (...) ((vaatab arvutist)) ee `kui te n�d=ee (0.8) s�idate Talinast `Helsingisse?   | DIJ: INFO ANDMINE |
 (0.5) ee siss see `ilmselt peaks olema kuskil `p�rastl�unasel `ajal?    | DIJ: INFO ANDMINE |
kuskil kella ka- `kahe paiku, (.) `neliteist kakskend=viis (.) l�heb `Siljalaini Super`siik�t.    | DIJ: INFO ANDMINE |
siis olete te (.) kell `kuusteist kaksk�mend olete `Helsingis.   | DIJ: INFO ANDMINE |
 Helsingist l�heb edasi samast terminalist (.) `laev (.) `Stokkolmi poole, (.) kell seitseteist null=null `v�lja, (.) ja te olete kuskil `s�da��l ee `Ahvenamaal.    | DIJ: INFO ANDMINE |
(0.5) `tagasis�itu puhul on `nii et ka tagasis�it ee (.) s�da`��sel (.) tulete te (.) ee: Ahvenamaalt `laeva peale, (.) {-} �ksk�ik mitu `p�eva sis sinna vahele `j��b, eks te ise [siis] `vaatate, (.) ee: ja `hommikuks on laev `Helsingis, (.) ja `Helsingist siis saate kohe ka Tallinnasse tulla kuskil peale kella kahe`teistk�mne paiku    | DIJ: INFO ANDMINE |
aga te `ilmselt uvitab palju see asi sis `maksab.   | KYE: VASTUST PAKKUV |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
M: [nojah]      | VR: NEUTRAALNE J�TKAJA |
N: jah.    | KYJ: JAH |       | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(...) ((T vaatab arvutist j�rele))
N: need `laevad k�ivad `iga p�ev nagu=jah   | KYE: VASTUST PAKKUV |
T: jaa, abso[luutselt.]   | KYJ: JAH |
N: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: iga `p�ev on v�imalik `k�ia et seal nagu vahet ei=`ole.      | IL: �LER�HUTAMINE |
 (2.0) `k�ll aga:=ee `hinnast l�htu- `innad on erinevad=e n�dala`l�puti on `kallimad ja n�dala `sees on odavamad.    | DIJ: INFO ANDMINE |
ma korra    | DIJ: EDASIL�KKAMINE |
(...) ((vaatab arvutist j�rele)) mm `hinnad natukene langevad alates kahe`teistk�mnendast augustist.   | DIJ: INFO ANDMINE |
 (0.5) vot et kuni selle selli `selle ajani on (.) on nad �pris `k�rgemad=ee (.) kaheteistk�mnendast augustist `langevad    | DIJ: INFO ANDMINE |
ma=ei=tea mis `hinda ma teile n�d kalku`leerin.   | KYE: AVATUD |        | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
N: noh   | YA: MUU |
T: sellep�rast=et `hinnad on nii `erinevad, ma=ei=saa    | IL: P�HJENDAMINE |
(.) noh (.) l�htuvalt sellest et millal see s�idu`aeg siis on.   | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
N: mhmh,    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) v�ib `p�rast kaheteistk�mnendat [`augusti.]   | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
T: [mhmh]    | VR: NEUTRAALNE VASTUV�TUTEADE |
(...) siis on `odavam on sellised v�ljumised kus te: valite s�iduajaks `p�hap�evast kuni `kolmap�evani (.) reisi.=   | DIJ: INFO ANDMINE |
N: =mhmh   | VR: NEUTRAALNE J�TKAJA |
(.)
T: et ilmselt sis v�iks `seda vaadata jah?   | KYE: VASTUST PAKKUV |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
N: mhmh   | KYJ: JAH |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
M: [mhmh]   | KYJ: JAH |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
T: [mhmh?]      | VR: MUU |
 (...) ((vaatab arvutist j�rele)) 
N: kas `Ahvenamaal on p�him�tselt on ned on saared k�ik omavahel (.) `�hendatud teedega et saab `jalgrattaga s�ita igalt poolt v�i on   | KYE: JUTUSTAV KAS |
T: no �ldiselt ta `ongi selline koht kus `jalgratastega k�iakse,   | KYJ: INFO ANDMINE |
N: mhmh=   | VR: NEUTRAALNE J�TKAJA |
T: =noh nii=et `jalgra[tas on] seal `hea liiklusvahend k�ll, (0.5) et et et ajab asja nigu `�ra.     | KYJ: INFO ANDMINE |
M: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(.) 
M: mhmh?   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (1.0) see laevaga �levedu jalgratastele tuleb mingisugune `eraldi mingi `maks ka v�i midagi.   | KYE: JUTUSTAV KAS |
T: ee `ei tohiks `olla.   | KYJ: EI |
 (.) * ma kohe vaatan *    | KYJ: EDASIL�KKAMINE |
(...) ((vaatab arvutist j�rele)) ee v�ike maks `on.     | KYJ: INFO ANDMINE |   | PA: ENESEPARANDUS |
 (...) sada kolgend `krooni (1.0) on=ee nagu �ks `jalgratas on (0.8) on sis ee (1.0) sellel (1.0) Siljalaini `laeval,    | KYJ: INFO ANDMINE |
(.) 
N: mhmh    | VR: NEUTRAALNE J�TKAJA |
(...)
T: {---} selles m�ttes seep�rast et j�udmine Marian`haamenisse on siiski selline `�ine, neli o- o- (.) `ommikul, (0.5) nii=et tegelikult see p� se=`s�it on nagu (.) `�ine.   | YA: INFO ANDMINE |
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: {-}   | YA: PRAAK |
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
(1.0)
M: < selge? >       | VR: NEUTRAALNE PIIRITLEJA |
T: (...) ((vaatab pikalt katalooge ja arvutit, tr�kib arvutisse otsingut)) `nii.   | YA: JUTU PIIRIDE OSUTAMINE |
 (...) {---}    | YA: PRAAK |
(...) see tuleks `kahe peale kuskil tsirka kolm=tuhat viissada (.) `krooni.   | DIJ: INFO ANDMINE |
N: mhmh    | VR: NEUTRAALNE J�TKAJA |
M: edasi=tagasi   | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
T: edasi=tagasi jah,    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
M: ahah?   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |     | VR: PARANDUSE HINDAMINE |
N: kuidas seal `kohapeal on kas saab nagu telki[misplatsi]   | KYE: JUTUSTAV KAS |
T: [vot `ei,] vabandust vabandust, (.) ei, (.) vabandust, (0.5) ee (2.2) viis=tuhat `kuussada.    | DIJ: INFO ANDMINE |  | PA: ENESEPARANDUS | 
N: viistuhat [`kuussada l�heb]   | VR: NEUTRAALNE VASTUV�TUTEADE |
M: [kahe=peale.]   | KYE: VASTUST PAKKUV |        | PPE: �LEK�SIMINE |
T: kahe=peale.    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(1.0) 
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: see sisaldab ee `kajutit `ka.      | DIJ: INFO ANDMINE |
 kajuti `maksumus siis ee �tleme sellel `laevas�idul      | DIJ: INFO ANDMINE |
M: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: {---}   | YA: PRAAK |
N: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
M: [mhmh] mhmh   | VR: NEUTRAALNE J�TKAJA |
T: et=ee `v�imalik et `Vikinglaini laevadel on `odavamad need hinnad,    | DIJ: INFO ANDMINE |
(.) ja tegelikult on `hinnad {-} k�ik `internetis    | YA: INFO ANDMINE |
saab nimodi rahulikult kalku`leerida ja `vaadata [{-}] {---}   | DIE: ETTEPANEK |
N: [aa]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: neid on k�iki v�imalik `vaadata.   | IL: �LER�HUTAMINE |
(.)
M: mhmh=   | DIJ: PIIRATUD N�USTUMINE |
T: =et=ee noh ee kodulehtedelt nende: laevade kodulehed on: v�ga `lihtsad.    | YA: INFO ANDMINE |
Siljalain on vevevee silja: line punkt e-ee, (.) sis on (.) ee `Vikinglain on vevevee vikingline   | YA: INFO ANDMINE |
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: punkt ef=`ii se- seal=on soomekeelne {-}   | YA: INFO ANDMINE |
 et sealt saab k�ike `vaadata.   | IL: �LER�HUTAMINE |
M: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: [aga] nende `k�mpingute kohta jah, seal on sellised v�iksed `majakesed {---} ja neid saab sealt `kohapealt (.) nagu vaadata.   | KYJ: INFO ANDMINE |
 ma vaatan     | KYJ: EDASIL�KKAMINE |
[kas] n�d `selles ei=`olnud midagi=v�i.      | KYE: JUTUSTAV KAS |
 ma [arvan et `on.]   | SEE: ARVAMUS |
M: [mhmh]     | VR: NEUTRAALNE J�TKAJA |
 [ma=i::] `ole j�udnud teda vel [ni=`v:�ga (.)] v�ga=vel stu`deerida.   | KYJ: INFO PUUDUMINE |           | SEJ: KEELDUMINE |
T: [jah (.) jah]      | VR: MUU |
T: jah    | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.5) a:ga sellist=ee `Ahvenamaa `majutust meie nagu `kunagi ei=ole `ette broneerind mitte kellelegi.      | KYJ: INFO ANDMINE |
N: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: selles m�ttes=et=ee (.) inimesed on v�tnud i- ise sealt kohapealt v�i broneerinud l�bi `interneti v�i midagi sellist eksole      | KYJ: INFO ANDMINE |
 `reisib�roodel (.) ei (.) ee (.) puudub `v�imalus neid odavaid asju k�ike `ette broneerida.   | IL: P�HJENDAMINE |
(0.5)
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
(0.8)
T: {---}   | YA: PRAAK |
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
(2.2)
T: ausalt �eldes see `n�uanne oli v�ga `napp aga aga (.) aga see `suurus`j�rk on kuskil ikkagi edasi=tagasi s�idu eest viis=tuhat `krooni.   | IL: KOKKUV�TMINE |
 et selle=   | YA: MUU |
N: =mhmh   | VR: NEUTRAALNE J�TKAJA |
(1.5)
T: aga:=   | YA: MUU |
N: =a kas te n�iteks neid `Serena veeparki neid {-}   | KYE: JUTUSTAV KAS |     | TVE: PAKKUMINE |
T: ee pakettreis jah   | KYE: VASTUST PAKKUV |      | PPE: �MBERS�NASTAMINE |     | TVJ: VASTUV�TMINE |
N: jah=   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
T: =ee (0.5) `Tallinki omi jah, (0.5) ((telefon hakkab helisema)) Tallinki: pakettreisid on {-}.    | KYJ: INFO ANDMINE |
(1.0) aga n��d on hoopis `paremad veepargid on (1.5) ((keegi vastab telefonile)) on v�i hoopis `parem veepark on {-} `Tamperes, `Tamperes on (.) p�ris n�d `uus veepark,   | YA: INFO ANDMINE |
N: ahah?   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: mhmh?     | VR: MUU |
(.) a=ma=`arvan et seal `ees selle kohta on {kogunisti} `olemas, (.) ma=l�n `vaatan,   | DIE: PALVE OODATA |
N: mhmh   | DIJ: N�USTUMINE |     
M: jaa?   | DIJ: N�USTUMINE |     
(...) ((T l�heb brosh��ri tooma, M ja N vaatavad niikaua �hte brosh��ri))
M: siin ei=ole selle {-} (.) kohta `midagi.   | SEE: V�IDE |
 (1.2) siin on aint `selle kohta et Turu `saarestiku kohta.      | IL: T�PSUSTAMINE |
N: mmh?   | SEJ: MUU |
M: rohkem pole mitte=`midagi kirjas.   | IL: �LER�HUTAMINE |
(1.2)
N: sinna `veeparki v�iks `minna, onju.   | DIE: ETTEPANEK |
M: mhmh?      | DIJ: N�USTUMINE |
(0.5)
N: et=sis `Tampere.      | IL: KOKKUV�TMINE |
 (.) kui seda `on olemas.        | IL: T�PSUSTAMINE |
(0.5) kuskil.      | IL: MUU |
 (.) {---}   | YA: PRAAK |
M: {---}   | YA: PRAAK |
N: {-}   | YA: PRAAK |
(1.0) ((T tuleb brosh��ridega tagasi))
T: siin on �ks (.) Tallinki paketid.        | KYJ: INFO ANDMINE |
 siis on `Serena, `Serena veepark, {---}         | KYJ: INFO ANDMINE |
M: mhmh?   | VR: NEUTRAALNE J�TKAJA |
T: {---} siin on k�ik nende (0.5) Hel[singi:] `linnaekskursi`oonide kohta ja k�ik paketid on siin `olemas.        | KYJ: INFO ANDMINE |
M: [mhmh]      | VR: NEUTRAALNE J�TKAJA |
N: mhmh=   | VR: NEUTRAALNE J�TKAJA |
M: ((n�itab brosh��ri, mida ta enne vaatas)) =siin on ainult Turu `saarestiku kohta.   | SEE: V�IDE |
 (.) rohkem pole `midagi.   | SEE: V�IDE |
 [`Ahvenamaa] Ahvenamaa kohta ma=i:   | SEE: V�IDE |
T: [ei=ole=v�i.]   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(0.5)
T: ei=ole `midagi {-} =   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
M: =ei ma=ei:=    | KYJ: N�USTUV EI |    | PPJ: L�BIVIIMINE |
T: =mhmh   | VR: NEUTRAALNE J�TKAJA |     
M: v�ibola ma=i=oska siin `vaadata=aga (0.5) ei=`old nigu.   | IL: PEHMENDAMINE |     | PPJ: L�BIVIIMINE |
T: mhmh   | VR: NEUTRAALNE J�TKAJA |     | VR: PARANDUSE HINDAMINE |
(...) ((T vaatab brosh��ri))
M: {-}   | YA: PRAAK |
(...) ((T vaatab brosh��ri))
T: tundub et ei=ole {-}   | SEJ: N�USTUMINE |
(...) ((T vaatab brosh��ri))
T: Ahvenamaa kohta siin `t�esti ei=ole=   | SEJ: N�USTUMINE |
aga (.) `siin on �ks `k�mpingutega ((telefon hakkab helisema)) seotud, Soome `k�mpingute kohta siin on ka `Ahvenamaa kohta {k�ik `sees.}     | YA: INFO ANDMINE |
N: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: on: eraldi `Serena veepargi kohta, (.) ja: �ks `v�ga=v�ga ea `aadress kus `ilmselt te saate `k�ikide k�simustele vastuse teada, (.) ((telefon on kogu aeg helisenud, j��b n��d vait, teine telefon hakkab helisema)) veve`vee punkt, {-} (.) n��d `punkt, (.) ef=ii.    | YA: INFO ANDMINE |
(1.0) see=on Soome turismiarenduskeskuse kodu`lehek�lg.      | IL: T�PSUSTAMINE |
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: seal: on v�imalik `eestikeelseks minna `inglisekeelseks `ka.       | IL: T�PSUSTAMINE |
(.) 
M: [mhmh?]   | VR: NEUTRAALNE J�TKAJA |
T: [et=ee] et=ee e (.) see=on: (.) organisatsi`oon, mis on t��tab nagu (0.5) mille esindus on ka `Eestis ja nende `�lesanne (.) on (.) tu`ristide `n�ustamine.    | IL: SELETAMINE |
(.)
M: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: [nii=et] nemad=e on:: teevad seda t�iesti `tasuta ja (.) ja see `ongi nende `t��.   | IL: SELETAMINE |
 Soome [mine]vate tu`ristide `n�ustamine et seal on nende kon`takttelefoninumber ka `olemas.   | IL: SELETAMINE |
M: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: et nemad on (.) `v�ga suured spetsialistid Soome alal=et   | IL: KOKKUV�TMINE |
(.)
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
T: `k�ik asjad saate sealt (.) sealt ilmselt `k�tte.   | IL: KOKKUV�TMINE |
 `sellelt lehelt.     | IL: T�PSUSTAMINE |
N: .jah=   | VR: NEUTRAALNE J�TKAJA |
M: =mhmh   | VR: NEUTRAALNE J�TKAJA |
T: ja `laevapileteid saab sis siit meie juurest {-}      | IL: �LER�HUTAMINE |
N: {-}   | YA: PRAAK |
T: .jah   | VR: MUU |
 (0.5) ma kirjutan teile ned=ee `kodulehtede `aadressid.   | DIE: PAKKUMINE |
(...)
M: mhmm (0.8) hmm   | DIJ: PIIRATUD N�USTUMINE |
T: jah   | VR: MUU |
(.)
M: aga n�iteks (.) mida mida veel Soomes uvitavat `teha saab.    | KYE: AVATUD |      
mis seal `p�hja pool veel uvitavat on.   | KYE: AVATUD |
(1.0)
T: heh $ no: ol-oleneb mida keegi n�ha `tahab. $   | KYJ: MUU |
M: a mida teil `pakkuda [on.]   | KYE: AVATUD |
T: [no:] (.) meie ei paku me (.) `p�him�tselt meil on aint `piletim��gib�`roo.   | KYJ: INFO PUUDUMINE |
M: [aa:]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
N: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
T: [ega me ei]=korralda selliseid me ei=`pane kellegile mar`shuute maha ega [midagi.]   | KYJ: INFO PUUDUMINE |
M: [aa.]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: ja `p�hitegevusvaldkond on `lennukipiletite `m��k [{-}]   | IL: T�PSUSTAMINE |
M: [mhmh]   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) ahhaa.=   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: =mhmh     | VR: MUU |
(2.8) et ma `arvan=et `vaadake `seda lehte ja te: seal on {---} samas=on: (0.5) selles   | DIE: ETTEPANEK |
M: mhmh=   | DIJ: PIIRATUD N�USTUMINE |
T: =on `ka kui te selle `l�bi loete siss (.) `ilmselt on seal `kirjas mis on uvitavat P�hja=Soomes.   | DIE: ETTEPANEK |
M: a:hah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
T: mhmh   | VR: MUU |
(.)
M: `selge?   | VR: NEUTRAALNE PIIRITLEJA |
(.)
T: `loodus on huvitav.   | KYJ: INFO ANDMINE |
N: mhmh   | VR: NEUTRAALNE J�TKAJA |
(0.8)
M: [{-}]   | YA: PRAAK |
N: [{sis=on}] `k�ik jah?   | RIE: L�PUSIGNAAL |
M: ait�h?   | RIE: T�NAN |
 [n�gemist?]   | RIE: H�VASTIJ�TT |
T: [{palun?}]   | RIJ: PALUN |
N: ait�h?   | RIE: T�NAN |
 n�gemist   | RIE: H�VASTIJ�TT |
   
 
   
 

