((200a1 teek�simine))
((�htlustas Andriela R��bis 28.12.2003))

A: vabandage palun   | RY: KONTAKTEERUMINE |
B: palun?   | VR: NEUTRAALNE J�TKAJA |
A: ee kas te oskate meile `�elda, kuidas me saaksime `Lasnam�ele, (.) kui me soovime `minna sinna.   | KYE: AVATUD |
B: e `Lasnam�ele: `buss l�heb siit`samast, ((osutab k�ega bussipeatuse poole)) kus siit `paistab se `bussipeatus.   | KYJ: INFO ANDMINE |
A: aga mis [num]ber `bussiga me nagu `minema peameh.   | KYE: AVATUD |     | KYE: T�PSUSTAV |
B: [ai]   | YA: MUU |
B: aga aga=ei `Lasnam�ele l�heb `niimitu `bussi,   | KYJ: INFO PUUDUMINE |
A: aa.   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) a=[SEAL]    | YA: MUU |
(.)
B: [jah]   | VR: NEUTRAALNE J�TKAJA |
A: t�hendab bussi`tulpade peal peaks nagu `kirjas olema=et [mis number buss `l�heb.]   | KYE: VASTUST PAKKUV |  | KYE: T�PSUSTAV |
B: [teate (.) seal juba,] (.) k�sige inimeste `k�est.=   | KYJ: INFO PUUDUMINE | | DIJ: ETTEPANEK |
C: =jah [et] kus`kandi      | IL: T�PSUSTAMINE |
A: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
C: ja kui teil on `t�nav teada sis (.) ma usun et te saate `teada  | IL: T�PSUSTAMINE |
.nhh ja siss (.) e kui `siit ei saa, siis on (.) `teenindusmaja eest.  | KYJ: INFO ANDMINE |  
A: ahah.   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) a `kuhu kanti see `teenindusmaja j��b.   | KYE: AVATUD |
C: `teenindusmaja e n�t ee e `siit e=   | KYJ: MUU |
B: =natuke maad siia `paremale,=   | KYJ: INFO ANDMINE |
C: =paremale jah.   | KYJ: INFO ANDMINE |
A: ahah.   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
C: ee ja �mber `nurga, j�rgmisel (.) ristmikul on `teenindusmaja, ja sealt=`l�heb .hh p�ris palju `busse.   | KYJ: INFO ANDMINE |
 (.)
A: mhmh.   | VR: NEUTRAALNE J�TKAJA |
C: ja samuti l�heb ka `sii:t otsast,  no `vot n��d oleneb kuhu te (.) eh, (.) >mis [`t�nav see `on.]<   | KYJ: INFO ANDMINE |
B: >[a te `t�navat] `teate.<   | KYE: JUTUSTAV KAS |
A: �� (0.5) meil, (.) me isegi ei tea `t�navat, | KYJ: INFO PUUDUMINE | 
meil on lihtsalt vaja `saada sinna `Lasnam�ele. | DIE: SOOV |
C: no siis no Las[nam�ele saate]   | DIJ: INFO ANDMINE |
B: [sest et Lasnam�e]on nii <`�udselt> suur, nii et [jah.]   | DIJ: INFO ANDMINE |
C: [Lasnam�ele] te `igal juhul `saate, kui te [`siit] n��d selle `bussiga l�hete. | DIJ: INFO ANDMINE |
A: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
A: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) ait�h?   | RIE: T�NAN |
C: palun   | RIJ: PALUN |



   

 
   

 

