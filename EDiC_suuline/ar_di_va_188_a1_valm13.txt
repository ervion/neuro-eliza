((188a))
((�hestas Andriela R��bis 16.10.2013))

((litereeritud 36.14 - 42.55, uuesti litereeritud m�rts 2013))
((U, M - pererahvas, A, J, T (laps) - k�lalised))

J: muidu me m�ttesime `siukest asja=et=et ajaks ennast selle:: �hese `praami=p�l.    | SEE: V�IDE |
(0.3) 
U: mhmh?   | VR: NEUTRAALNE J�TKAJA |
(2.9)
J: �ks l�b `siit=eks.    | KYE: VASTUST PAKKUV |
(2.5) 
J: `Kuivastust.   | IL: T�PSUSTAMINE |
 nojah kui sa kella `kolmeks tuled=sis    | DIE: ETTEPANEK |
(3.4)
M: {---}    | YA: PRAAK |
(1.7)
J: s=tuled `j�r[gi.]   | DIE: ETTEPANEK | 
M: $ [`lau]pp�val ka `menna `kus sa ikka `kipud `nii `ruttu {-} $   | SEJ: MITTEN�USTUMINE |
 | DIE: ETTEPANEK |
(0.4)
U: hehe    | VR: MUU |
(2.1) 
U: ja `reedese `p��val=ju `s�nni `minna `ka.   | SEJ: MITTEN�USTUMINE |
 | SEE: V�IDE |
(2.3) 
U: `usk ei `luba `�ltse: n�naviisi.    | SEE: V�IDE |
(1.0) 
U: ikka (.) `teisib� `neljaba=`lauba [vot siis `k�ivad.]   | SEE: V�IDE | 
M: [hehe]   | VR: MUU |
(0.9)
U: [{-}]    | YA: PRAAK | 
A: [mis=u- mis]=`usk se=`on.    | SEJ: MUU |
 | KYE: AVATUD |
 | YA: RETOORILINE K�SIMUS |
(0.5) 
A: ma vist ei `ole $ seda `us[ku.] $   | SEJ: MITTEN�USTUMINE |  | SEE: ARVAMUS | 
U: [see]   | KYJ: MUU | 
M: hehe   | VR: MUU | 
U: [see=on] siuke `t�eks`pidamine.   | KYJ: INFO ANDMINE |
M: [noo?]   | VR: MUU |
(.) 
J: oda `me tulime `kolmap�ev=noh,   | YA: INFO ANDMINE |
(.)
U: $ noo? $   | VR: NEUTRAALNE J�TKAJA |
(5.3)
J: `neljab�.   | YA: INFO ANDMINE |
 (.) nelja- (1.2) > palju ma `t�na=maha < (0.7) `p�rutasin.    | KYE: AVATUD |
(2.6)
J: {-}   | YA: PRAAK | 
M: {--}    | YA: PRAAK |
(1.0)
J: mt=.hh mhh    | YA: MUU |
(0.4)
{-}: .h��   | YA: MUU |
(1.3) 
J: mis=me=`teeme.    | KYE: AVATUD |
(4.9)
U: `lauba omiku l�te esimese `praamiga `�le.   | KYJ: INFO ANDMINE |   | DIE: ETTEPANEK | 
A: �hhhh saa=nii `vara `�lesse.     | DIJ: MITTEN�USTUMINE |  | SEE: ARVAMUS |
U: hehe ((pikk l�bus parastav naer))   | VR: MUU | 
J: $ < `ole `m�istlik. > $    | DIJ: MITTEN�USTUMINE |
(0.4)
M: $ `ei ot `laupp�eval ei l�he see `esimene nii `v�ga vara `midagi. $   | SEJ: MITTEN�USTUMINE | | SEE: V�IDE |
(0.5)
J: ainult kella `kuue ajal=jah.   | SEJ: MITTEN�USTUMINE |
    | KYE: VASTUST PAKKUV |
(.) `sel[ge.]    | VR: MUU | 
M: [ei] | KYJ: EI |
ot ma=i=[`tea kas ikka] v�ibola isegi `veel iljem.  | KYJ: INFO ANDMINE | 
U: [ei=`ei.]    | KYJ: EI |
(0.6)
U: kui se pole ika `kaheksast esimene: lau`p��val [{-}].   | KYJ: INFO ANDMINE | 
J: [mt] < `kaheksa. >      | VR: NEUTRAALNE VASTUV�TUTEADE |
`kaheksa ajal `mina keeran heal juhul teist `k�lge kui `m�takaid s�l `juures ei oleks.    | DIJ: MITTEN�USTUMINE |
(4.9) 
J: < mt kui `kuked ei `kireks, (0.4) `m�takad ei `n�giks, >   | IL: T�PSUSTAMINE | 
U: hehe ((l�hike naer))    | VR: MUU |
(2.1) 
((kass n�ub))   
(1.1)
M: anna=tale=`s�ija.    | DIE: ETTEPANEK |
(2.5)
J: `linnas on `ea.   | YA: INFO ANDMINE |
 (.) siis on `niimodi=et ku `m�takad tulevad `�lesse?    | YA: INFO ANDMINE |
 (1.2) ((vahepeal kass n�ub)) nemad `teavad=et `ommiku peavad `vakka olema, issi `magab.     | YA: INFO ANDMINE |
(2.0)
U: �=see    | YA: MUU |
(1.5)
M: {---} ((t�is suuga)) (0.7) `linnas ((3. v�lde)) ka=ju `kaua ((3. v�lde)) `magada.     | SEE: V�IDE |
(0.7)
J: ei=no=ku siis=ku `kool on sis `muidugi ei saa [sis ]   | SEJ: N�USTUMINE | 
M: [noh?]   | VR: NEUTRAALNE J�TKAJA |
(0.3)
J: sis=ku `kool on sis olen `mina niimodi: `l�ind, `nemad keeravad=vel `teist k�lge=noh.     | SEJ: N�USTUMINE |
(.) 
M: mm?   | VR: MUU | 
J: ok[ei.   | VR: MUU |
 aga] * se=on *  | YA: MUU |
U: [see]   | YA: MUU |
(0.5) 
J: `n�dalavahetusel on nimodi nemad on (0.5) `n�ksti teises `toas=s�l (0.9) panevad `teleka m�ngima vaatavad oma `multikaid [mingisugused,]   | YA: INFO ANDMINE | 
U: [`kui `Tall]inas elas see `vanaisa alles.    | YA: JUTUSTAMINE |
 (1.8) ja::: ja=`siiss=��� kui me sj�nna `l�ksime,    | YA: JUTUSTAMINE |
 (0.7) vat `seal=eee mis pagana    | KYE: AVATUD |
(0.4) 
M: {--} ((�tleb midagi kassile))   | YA: PRAAK | 
U: jah `jaam `on seal=��=mh nende    | KYE: AVATUD |
(0.5)
M: `Balti ((palataliseerimata)) `jaam.   | KYJ: INFO ANDMINE | 
U: `Balti ((palataliseerimata)) `jaam   | VR: NEUTRAALNE VASTUV�TUTEADE |
 seal nende (0.5) ee `maja `juures.   | YA: JUTUSTAMINE |
 (0.8) ja=[vata=sis]   | YA: JUTUSTAMINE | 
M: [{--}] {-}    | YA: PRAAK |
(0.3) 
U: $ `ommiku $ (0.7) `ommiku (1.0) nn `kella `neljast kui akatakse (0.6) ee seda:::: `vagunid kokku panema.    | YA: JUTUSTAMINE |
 (0.8) vat [`siis=�]    | YA: JUTUSTAMINE | 
M: [{mis `vagun}]    | KYE: AVATUD |
     | PPE: MITTEM�ISTMINE |
(0.4) 
U: `neh v seda `ro[ngi.]   | KYJ: INFO ANDMINE |    | PPJ: L�BIVIIMINE | 
J: [ron]gi. | VR: PARANDUSE HINDAMINE |
M: `rongi.   | VR: PARANDUSE HINDAMINE |
 ennem=sid [`vag]unid [{-}]   | VR: PARANDUSE HINDAMINE |   | PPE: MUU | 
U: [siis] [seda `ron]gi,    | PPJ: MUU |
 (0.5) siis see `vagun akab seal (.) @ hashsha hashsha hashsha. | YA: JUTUSTAMINE | 
(.) ������������� ((imiteerib vedurivilet)) uuuuuuuuu hashsha hashsha @ (0.6) `sedavisi=[ta=`v�tab, ]    | YA: JUTUSTAMINE | 
A: [hehe]    | VR: MUU |
(1.0) 
U: ja ja < `neljast > `saadik. | YA: JUTUSTAMINE | 
(.) vahest isegi `varem akas.   | YA: JUTUSTAMINE |
J: no: aga `siis ku=nad nimodi `kokku l�hvad sis=on `�ks ((l��b k�si kokku)) kolks ja=sis=on=see (.) > kolladi=kolladi=koll=kolladi=koll[adi=koll.] <   | SEE: ARVAMUS | 
U: [aga] `ongi, | SEJ: N�USTUMINE |
aga siis=ta=akab `esiteks=ee akab sedasi @ .hh h�shsh h�shsh h�shsh uuuuuuuuu pop pop=pop=pop uuuuuuuuu @   | YA: JUTUSTAMINE |
 (.) ja=vot `sedasi ta `v�tab.    | IL: KOKKUV�TMINE |
(0.6) `issand jeesus.    | IL: HINNANG |
(1.8)
J: $ ja=seal nad `elavad. $   | SEE: V�IDE | 
U: jah, ja nad `elavad `seal.     | SEJ: N�USTUMINE |
no=s mis`moodi te `elate: sii=et (1.0) `magavad `sedavisi=et=ei `kuule ega `n��.    | KYE: AVATUD |
     | YA: RETOORILINE K�SIMUS |
 (0.3) ma=n `neljast `saadik `�leval, silm j�me `peas vahid (2.5) ja: ja=sis=ku kui sa akad `ommikul seal `tulema akkama, (0.6) sis sa j��d * `magama. *     | YA: JUTUSTAMINE |
 (2.9) `irmus koht.     | SEE: ARVAMUS |
 (0.5) {-} (1.7) @ uuuuuuuuu �shsha �shsha �shsha �shsha �sh (.) pop (0.5) pop=tuuuuuuu t�t=t�t=t�t=t�t=t�t @    | IL: �LER�HUTAMINE |
(1.0)
M: $ `arjud `�ra, sis p�le �da `midagi. $=   | SEJ: PIIRATUD N�USTUMINE |    | SEE: ARVAMUS | 
U: =$ jah $    | VR: MUU |
(1.2) sa=i=sa ju `arjud.     | SEJ: MITTEN�USTUMINE |
(3.3) `ma �tli `siip�rast `Vellole akkame .hhh `�hta=ajal ommiku `l�ksime, tuleme �hta `tagasi.    | YA: JUTUSTAMINE |
(.) nagu`nii=�p=saa `magada:=�� kella (.) `kolmest=neljast `�leval kui seda: akatakse `kokku=duu `klopsima, (0.4) sis ta (.) trummeldap:= seal {--}     | YA: JUTUSTAMINE |
(1.2)
M: vot nii.    | VR: NEUTRAALNE PIIRITLEJA |
k�rvaklapid $ `p�he ��seks [sis] $   | DIE: ETTEPANEK | 
U: $ [ja]ah?    | DIJ: N�USTUMINE |
 k�r[vaklapid] `p�:he. $    | DIJ: N�USTUMINE | 
M: $ [kuule midagi.] $    | IL: P�HJENDAMINE |
(2.2)
J: $ seal `sinu k�rval on=to=�ks $ mingisugune (.) `superpood [kus on (.) `t��riideid k�rva`klappe=ja]   | YA: INFO ANDMINE | 
U: [ah `ma pean `nendele=ju `otsima=ju] (1.3) `kuivatusasjad > sis kohe `sauna. {-} <     | YA: INFO ANDMINE |
(2.4) ((U l�heb k��gist �ra))
J: ms=me=`teeme.    | KYE: AVATUD |
 (.) kuule.    | KKE: ALGATUS |
(1.0)
M: $ l�hete `sauna. $    | KYJ: INFO ANDMINE |
     | DIE: ETTEPANEK |
(0.7)
J: ei=no `sauna l�hme=s `nigunii.     | DIJ: N�USTUMINE |
(0.3) 
M: $ no v�rk [`siukke.] $   | VR: NEUTRAALNE PIIRITLEJA | 
J: [ms=me] `omsega [teeme.]   | KYE: AVATUD | 
M: $ [`sau]nas ((3. v�lde)) `m�tlete. | DIE: ETTEPANEK |
seal=on:::=eee `k�rgem temperatuur, $ | IL: P�HJENDAMINE |
A: hehe $ tuleb [p(h)aremad `m�tted,] $ | DIJ: N�USTUMINE | 
M: $ [`soodustab `m�]tlemist. $ | IL: P�HJENDAMINE |
(1.6)
J: l�hme `marjule.    | DIE: ETTEPANEK |
 (1.8) mt `p�ld`marju saad.=    | IL: T�PSUSTAMINE | 
A: =ku=sa `j�uad nende `p�ld`marjadega `Tartusse, mis on sul nendest $ `j�rele `j��nud.     | DIJ: MUU |
 | KYE: AVATUD |
     | YA: RETOORILINE K�SIMUS |
 kis`selli sul pole vaja `keeta, [kis`sell on `v(h)almis.] $    | SEE: V�IDE | 
M: [hehe] ((pikk l�bus naer))    | VR: MUU |
(1.6)
J: noo=aga (1.2) kas=sa `Tartus `saad kuskil > p�ldmarju    | SEJ: MUU |
 | KYE: SULETUD KAS |
     | YA: RETOORILINE K�SIMUS |
 sa=i=sa=isegi < `t:uru=b�lt p�ldmar[ju.]   | SEE: V�IDE | 
A: [no] igal=`juul me `korjame ja v�tame kaasa.    | DIJ: N�USTUMINE |
 paneme `pudeli[sse=ja]   | DIE: ETTEPANEK | 
M: [l�hed `sau]na.    | KYE: SULETUD KAS |
(0.4)
T: l�hen?    | KYJ: JAH |
(.)
M: noh tule `vaata {-} `on,    | DIE: ETTEPANEK |
(3.4)
A: no `ommikul me `saame=ju korjata me=i=`pea=ju terve `p�ev otsa n�d `sinna {-}.    | DIJ: PIIRATUD N�USTUMINE |
(1.6) * {---} *    | YA: PRAAK |
(1.1)
J: * `viis, (0.7) `kuus, `seitse, `kaheksa, �eksa=`k�mme. *    | YA: MUU |
(1.6)
A: * l�hme mingi `viiese `praamiga. *    | DIE: ETTEPANEK |
(0.9)
J: {-} juba `kolmese praami `peale.    | DIJ: MUU |
     | DIE: ETTEPANEK |
(4.4)
J: `kolmese `praamiga `�le,    | DIE: ETTEPANEK |
(2.4) ((pausi ajal mingi m�rts))
U: kus [`l�ks=n��d,]   | KYE: AVATUD | 
A: * [{--}] {---} *    | YA: PRAAK |
(5.0) ((keegi toksib))
U: $ `all, $=   | YA: MUU | 
J: =v�i l�hme `�leomme,    | DIE: ETTEPANEK |
(1.0)
A: [{-}]   | YA: PRAAK | 
J: [ja siis] `vaatame=s��l.    | DIE: ETTEPANEK |
(0.9)
U: `�leomme te `l�hetegi.    | DIJ: N�USTUMINE |
(6.2)
U: mis=te t�-=`l�hete omme.    | KYE: AVATUD |
     | YA: RETOORILINE K�SIMUS |
(2.5)
U: meitel=on=koe (.) {-} kurat `muud `teha=ei=saa   | YA: MUU | 
J: mheH   | VR: MUU |
U: `pange {-} `Koguva=metsa `seenele niikauaks [{-}] �� (.) [leiate `kuke]`seeni.   | DIE: ETTEPANEK | 
J: [mheh]   | VR: MUU |
 < [seeni] >   | VR: NEUTRAALNE VASTUV�TUTEADE | 
A: `on `ka m�ni=v�i.    | KYE: SULETUD KAS |
(0.5)
A: me `k�isime �kskord kukeseeni `otsimas me ei leidnud `midagi.    | IL: P�HJENDAMINE |
(0.6) ((laps naerab taustal))
U: [{-}]   | YA: PRAAK | 
J: [et meile] (.) eee `meil on �ks siuke: `maru hea (0.9) `vaarika`mets    | YA: INFO ANDMINE |
 ((lapsed hullavad ja naeravad taustal)) 

      

      
