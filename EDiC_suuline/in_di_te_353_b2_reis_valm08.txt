((353_b2_tel_in))
((�hestas Andriela R��bis 14.09.2008))

((kutsung))   | RIE: KUTSUNG | 
V: {`Vip} reisid   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
=tere?   | RIE: TERVITUS |
 (.) `Terje=kuuleb,    | RY: TUTVUSTUS |
(.)
H: e tere:?    | RIJ: VASTUTERVITUS |
`mind=uvitavad `suusareisid.   | DIE: SOOV | 
V: jaa?   | VR: NEUTRAALNE J�TKAJA |
 (.) `v�ga `tore?      | VR: HINNANGULINE J�TKAJA |
(.)
H: kas ainult `Austriasse pakute.    | KYE: JUTUSTAV KAS |
(0.6)
V: �� `t�hendab `no: siin=on mingeid=e `teisi variante `ka ikka n�iteks Itaaliat=ja=h?   | KYJ: INFO ANDMINE |
  (1.0) ja `Prantsusmaad=ja?   | KYJ: INFO ANDMINE |
(0.6)
H: aa [vee-]   | YA: MUU |
V: [et=mis]=`koht teid nagu `t�psemalt `uvitaks.   | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: a Skandi`naaviasse ei=`ole n�iteks.   | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |    | KYE: JUTUSTAV KAS | 
V: on `ikka,      | KYJ: MITTEN�USTUV JAH |
 (.) sin=on ��: (.) > `Norras=on n�iteks `Tresine=suusakeskus=ja `Rootsis=on se={`Pikmedure} `n�iteks=ja, <   | KYJ: INFO ANDMINE |
(0.8)
H: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: [Idre]=ja=ja=siis `Soomes on k�ik=ned `Tahko=`Himos (.) {`Vuokatti}=`k�ik `v�imalikud.   | KYJ: INFO ANDMINE |
(0.4)
H: ahah.   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) ja `pakute `mis=ajast `alates sis=h (.) neid=`rei[se.]   | KYE: AVATUD |
V: [��] `t�pselt `niimoodi > nagu `ise `soovite=et ned `pannakse `k�ik individu`aalselt `kokku=teile. <   | KYJ: INFO ANDMINE |
(. )
H: aha   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
=`mingeid `grupivariante=ei=`ole jah?     | KYE: SULETUD KAS | 
V: � `grupireise=n�d Skandi`naaviasse `k�l=ei=ole.   | KYJ: N�USTUV EI |
(.)
H: aga=`kuskile `mujale?   | KYE: AVATUD |
(.)
V: �� `Austriasse on.    | KYJ: INFO ANDMINE |
(.) > ja=siis `Itaaliasse=`Prantsusmaale. <   | KYJ: INFO ANDMINE |
(0.4)
H: * ahah. *   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 .h aga n�iteks=kui=seda `Austria kohta n�d `k�sida=et    | YA: EELTEADE |
(.)
V: jaa?   | VR: NEUTRAALNE J�TKAJA |
H: .h et `hind on pandud teil=seal alates mingist `neljateist=tuhandest jah.   | KYE: VASTUST PAKKUV |
(0.4)
V: ��: jah,   | KYJ: JAH |
 (.)  > `p�him�tselt=`k�ll=ned `veebruarikuu=`reisid. <    | IL: T�PSUSTAMINE |
(.)
H: a=`millest=se=`ind nagu `olenep=sis,   | KYE: AVATUD |
 (.)  > `veel=et kas ta v�ib `t�usta v�i: <  | KYE: SULETUD KAS |
(.) `v�hemaks=ta `kindlasti vist ei [l�he jah.]   | KYE: VASTUST PAKKUV |
V: [� `v�he]maks=ei=`l�he   | KYJ: N�USTUV EI |
=et n�d=`ongi selline: `mure meil= et=e seoses k-=nende `terrori `aktidega, (.) on=k�ik `lennu `firmad oma: hindasid `t�us- `t�stnud,   | KYJ: INFO ANDMINE |
(.)
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
V: et =me `v�ga n�d `loodame=et=ned `tsharterinnad `oluliselt `k�rgemaks ei `l�h�,   | KYJ: INFO ANDMINE |
(1.2)
H: aga `sellne=k�simus mis=se $ `tsharter `lend �ldse t(h)�hendab. $   | KYE: AVATUD |   | PPE: MITTEM�ISTMINE | 
V: �� `tsharterlend t�hendab `seda=et see: (.) l-`lennuk=l�heb otse: (.) `Tallinnast `siht `kohta=et `te istute `siit (.) `lennuki=peale, (.) ja=`lennuk `maandub n�iteks `M�nhenis.   | KYJ: INFO ANDMINE |    | PPJ: L�BIVIIMINE |
(.)
H: ahah,   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS | | VR: PARANDUSE HINDAMINE |
[ selles=et]      | YA: MUU |
V: [ja=`M�nhenis] on=teil:: (.) > noh `otselend=et mingeid `�mberistumisi ei=`ole. <      | KYJ: INFO ANDMINE |    | PPJ: L�BIVIIMINE |
(.)
H: * ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |     | VR: PARANDUSE HINDAMINE |
 `v�ga=hea,       | VR: HINNANGULINE INFO OSUTAMINE UUEKS |  | VR: PARANDUSE HINDAMINE |
 `v�ga=hea. *     | VR: HINNANGULINE INFO OSUTAMINE UUEKS |    | VR: PARANDUSE HINDAMINE |
(.)
V: mhmh?   | VR: MUU |
(.)
H: ja: `soodustusi: mingitele `gruppidele=n�iteks ei=`ole.   | KYE: JUTUSTAV KAS |
(.)
V: ee no=`neid saab nagu alati `kokku`leppeliselt (.) �� siin: `l�bi=r��kida.    | KYJ: INFO ANDMINE |
(.)
H: et=`n�ideks kui=ma mingi `k�mme inimest `kokku ajan=et(h) (.) [et]      | KYE: MUU |
V: [`siis] `kindlasti annab=seda `r��kida.    | KYJ: INFO ANDMINE |
(.)
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) .h ja: `mida=te `majutuseks=nagu `pakute=sis=selle=`hinna sees.   | KYE: AVATUD |
(.)
V: �� `majutuseks on meil: � > se=`odavam variant on=n�d `kolmet�rniho`tell koos `ommiku `s��giga.   | KYJ: INFO ANDMINE |
 < (.) ja=otellis on olemas ka=`saunad=h.   | KYJ: INFO ANDMINE |
(0.4)
H: * mhmh *=   | VR: NEUTRAALNE J�TKAJA |
V: =et `k�ik see on nagu `tasuta.   | KYJ: INFO ANDMINE |
 (.) ja sis on ��=noh (.) > `lennupilet edasi=tagasi=ja < bussi`transfer=sis.   | KYJ: INFO ANDMINE |
(0.4)
H: [m]   | YA: MUU |
V: [��] `lennujaamast �� ho`telli=on=see * ja=sis `tagasis�idul ka `sama=moodi. *   | KYJ: INFO ANDMINE |
(.)
H: aga `s��giga=n�d (.) on `kuidas,   | KYE: AVATUD |
 v=`aind `hommikus��k.    | KYE: VASTUST PAKKUV |
(.)
V: `ainult `hommikus��k,    | KYJ: JAH |
>  `nelja=t�rni: hotellil on=ka `hommiku=`�htus��k `m�lemad sees. <    | IL: T�PSUSTAMINE |
(.)
H: ahah.    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.4) ja m- > `m�eldud `kauaks teil=`on=ned=`reisid,   | KYE: AVATUD |
 <  `n�dal=v�i noh [tavaliselt]   | KYE: VASTUST PAKKUV |
V: [`n�dalased,]   | KYJ: JAH |
 `laup�evast `laup�evast m��b=`alati `Austria=ennast.   | IL: T�PSUSTAMINE |
(.)
H: ahah,   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 `n�dalaajased [jah?]   | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE | 
V: [mhmh]    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(1.0)
H: .hh aga `mis=sis `veel `k�sida.    | YA: RETOORILINE K�SIMUS |
(1.6) 
H: > `lennuk l�heb `Tallinnast `seda=ma=juba sit=`lugesin, <   | YA: INFO ANDMINE |
  (.) .h aga m- `mingid `lisa `tasud vel=`kuskilt (.) v�iksid ka `juurde tulla.   | KYE: JUTUSTAV KAS |
 (.) `kind[lustus v�ib]      | YA: MUU |
V: [�� no `kindlustus] `kindlasti nagu=soovitame `vormistada.       | KYJ: INFO ANDMINE |
(.)
H: aga `palju `see maksab.   | KYE: AVATUD |
(.)
V: �� `kindlustus=sin `kaheksa p�eva: (.) * riski=�raj��mise `katkemisega maksab=`kohe=�tlen * `teile=h (1.6) `kakssada=`viisteist `krooni.   | KYJ: INFO ANDMINE |
(.)
H: `kaksada=viisteist `krooni [jah?]   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
V: [jah]   | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
H: selge?      | VR: NEUTRAALNE PIIRITLEJA |     | VR: PARANDUSE HINDAMINE |
(1.0) 
H: [a]   | YA: MUU |
V: [ja] `m�e `pilet muidugi=tuleb `kohapeal=siis=`osta.    | KYJ: INFO ANDMINE |
(.)
H: aha   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 `m�epilet `tuleb=ka `juurde osta [jah.]   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
V: [jaa]    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(.)
H: ja mis `see: orien`teeruvalt v�iks `meie=rahas `maksta.   | KYE: AVATUD |
(0.8)
V: � `meie `rahas ta=on `mingi `kaks=pool=`tuhat on `umbes `kuue=p�eva `m�e `pilet=h.   | KYJ: INFO ANDMINE |
(.)
H: * mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) `kuus `p�eva siis. *       | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE |
(.)
V: mh?      | KYJ: JAH |    | PPJ: L�BIVIIMINE |
(.)
H: ai`t�h `teile=sis.   | RIE: T�NAN |
(.)
V: jah palun?   | RIJ: PALUN |
(.)
H: n�ge[mist?]   | RIE: H�VASTIJ�TT |
V: [n�ge]mist   | RIJ: VASTUH�VASTIJ�TT |
      

