((100a17 raamatute k�itmine))
((�htlustas Andriela R��bis 16.12.2003))

P.T: kas te `k�idate `ka.   | KYE: JUTUSTAV KAS |
 (.) n�iteks kui ma toon �he `kapsaksloetud `raamatu kas te selliseid k�itet�id [ka teete.]   | IL: T�PSUSTAMINE |  | KYE: JUTUSTAV KAS |
T: [ei::]   | KYJ: EI |
 meil on rohkem nagu `kontori=ja `diplomit��d ja `need [asjad.]    | KYJ: INFO ANDMINE |
spi`raali paneme k�ll=ja    | KYJ: INFO ANDMINE |
((n�itab �hte spiraaliga k�idetud t��d))
P.T: [ahah ahah]    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
P.T: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) aga niimodi    | KYE: AVATUD | 
(1.5) 
T: seda saa[te] T�he viis`teist, `seal nad restaureerivad [raamatuid.]   | YA: INFO ANDMINE |
P.T: [ta-]   | YA: MUU |
 [aa, `T�he] viis`teist,   | VR: NEUTRAALNE VASTUV�TUTEADE |
 ma `m�letan et `kunagi oli `siin `ka.   | SEE: V�IDE |
(1.2)
T: n��d ei=ole   [muud kui] `T�he viisteist   | SEJ: MITTEN�USTUMINE |
 ja=sis=on: (.) `Vaba t�navas peaks `ka olema �ldiselt.   | YA: INFO ANDMINE |
P.T: [n��d ei ole jah]    | YA: MUU |
P.T: ahah?   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (0.8) `T�he viisteist mis `maja see `on.   | KYE: T�PSUSTAV |     | KYE: AVATUD |
(.)
T: s:ee on (.) `viiekordne elu`maja, seal on rek`laamid `v�ljas kuskilt `tagant`poolt saab sis- sisse.   | KYJ: INFO ANDMINE |
P.T: ahah    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(.) selge.   | RIE: L�PUSIGNAAL |
 (0.5) ait�h?=   | RIE: T�NAN |
T: =seal on kohe=et restaureerivat `meil nigu restau`reerimist ei=ole.=   | RIJ: L�PETAMISE TAGASIL�KKAMINE |    | IL: �LER�HUTAMINE |
P.T: =mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 te lihtsalt kui ma toon paberilehed [sis te]   | PPE: �MBERS�NASTAMINE |    | KYE: VASTUST PAKKUV |
T: [jah, | PPJ: L�BIVIIMINE |   | KYJ: JAH |
aa `neli] ((joonistab k�ega �hus A4 formaadi)) siis v�ib=  | IL: T�PSUSTAMINE |
P.T: =k�idate `kokku [need.]   | PPE: �MBERS�NASTAMINE |    | KYE: VASTUST PAKKUV |
T: [jaa] (.) .jah   | KYJ: JAH |  | PPJ: L�BIVIIMINE |
(0.8)
P.T: jah,    | VR: NEUTRAALNE VASTUV�TUTEADE |
`selge.   | RIE: L�PUSIGNAAL |
T: mhmh   | RIJ: L�PETAMISE VASTUV�TMINE |
P.T: ait�h?   | RIE: T�NAN |
T: * palun *   | RIJ: PALUN |

   

 
