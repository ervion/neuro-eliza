((in_di_te_572_4))
((�hestas Andriela R��bis 28.11.2008))
((kutsung))   | RIE: KUTSUNG |
V: > `X amet=   | RIJ: KUTSUNGI VASTUV�TMINE | | RY: TUTVUSTUS |
tere=`p�evast <   | RIE: TERVITUS |
(.)
H: tere.   | RIJ: VASTUTERVITUS |
 ma=sooviks `k�sida `v�lismaalase: `t��le `tuleku=kohta.    | DIE: SOOV |
(0.4) eq=`Eestisse.    | IL: T�PSUSTAMINE |
(0.5) et `selleks peab tal=olema `t��`luba (.) [kui=ta]       | KYE: JUTUSTAV KAS |
V: [jaa]   | KYJ: JAH | 
H: tuleb (.) n�iteks `Iirimaalt.       | KYE: JUTUSTAV KAS |
(0.4)
V: eeeeeeeeeeeee=   | YA: MUU | 
H: =see `on: Eu`roopa Liidu `riik n��d.      | IL: T�PSUSTAMINE |
(.)
V: Eu`roopa Liidu `riikm:es`riikide puhul < `on: n��d `nii:?   | KYJ: INFO ANDMINE |
 .hhh �ks `hetk ma `v�tan meie `veebilehel `lahti selle `koha?   | KYJ: EDASIL�KKAMINE |
 (.) .hhhh v�lismaalase > `kui on:=e `mitte (.) E`uroopa Liidu `riige sii:s=e   | KYJ: INFO ANDMINE | 
H: eq `tema just `ongi.        | IL: T�PSUSTAMINE |
(0.3)
V: mt=.h ee (0.3) `tema `on.   | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE | 
H: jah.   | KYJ: JAH |   | PPJ: L�BIVIIMINE | 
V: .hh (0.6) sii:s (0.5) siis `temal=on < `natu`kene @ `kergem. @ >    | KYJ: INFO ANDMINE |
(2.7)
H: et    | YA: MUU |
(1.0)
V: .hh [nii:,   | YA: JUTU PIIRIDE OSUTAMINE |
Eu`r]oopa   | KYJ: INFO ANDMINE | 
H: [`mis=ta peab `t(h)egema.]   | KYE: AVATUD | 
V: > Liidu ja=Euroopa vahenduspiirkonna=`riikide=ja=`Shveitsi=kodanikud `ei `vaja 
`esimese `kolme=kuu=jooksul `Eestis t��tamiseks `luba. <     | KYJ: INFO ANDMINE |
((loeb))
(.)
H: jaa,   | VR: NEUTRAALNE VASTUV�TUTEADE |
 [a]=kui=on `pikem=aeg?   | KYE: AVATUD | 
V: [.h]   | YA: MUU |
(.)
V: `tuleb {-} `elamisluba `tao- (0.3) `t��tle::- `t��tamiseks `Kodakondsus=ja=Migratsi`ooniametist .hhh ee: et `otse `sinna.   | KYJ: INFO ANDMINE |
 (.) < et `siin ei `ole >  (0.5) ee > `T��turuameti n�usolekut `vaja. <    | KYJ: INFO ANDMINE |
(0.3)
H: aa,   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 nii=et `otse `sinna sis.   | KYE: VASTUST PAKKUV |    | PPE: �MBERS�NASTAMINE | 
V: jah.    | KYJ: JAH |  | PPJ: L�BIVIIMINE |
(.) et `kui on=ee `Venemaa Uk`raina `siis on=ee l�bi `meie.    | IL: T�PSUSTAMINE | | PPJ: L�BIVIIMINE |
(.)
H: aa,    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS | | VR: PARANDUSE HINDAMINE |
(.) siis=ma `v�tan `nendega otse `�he[ndust.]     | KYE: VASTUST PAKKUV |
V: [jah.]     | KYJ: JAH |
(.)
H: ai`t�h teile.   | RIE: T�NAN | 
V: pa:lun   | RIJ: PALUN |
 [{n�gemist}]   | RIE: H�VASTIJ�TT |
H: [head]=aega   | RIJ: VASTUH�VASTIJ�TT |
      
