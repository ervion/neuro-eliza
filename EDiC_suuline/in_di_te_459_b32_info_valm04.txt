((459b32 infotelefon))
((�htlustas Andriela R��bis 28.10.2004, parandatud 11.06.2012))

((kutsung))   | RIE: KUTSUNG |
V: info`telefon=   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
Kersti=   | RY: TUTVUSTUS |
tere   | RIE: TERVITUS |
H: te:re    | RIJ: VASTUTERVITUS |
sooviks teada (.) `P�hja politseiprefek`tuuri `Ida politsei`osakonna `faksinumbrit.   | DIE: SOOV |

(...)
V: .hh (.) ja `kuidas=se=oli `P�hja,    | KYE: AVATUD |        | PPE: MITTEM�ISTMINE |
meil on `Ida politseiprefek`tuur `seda m�tlete v�i, J�hvis=   | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
H: =`P�hja politseiprefek`tuuri Ida politsei`osakond see on `Tallinas tegelikult.    | KYJ: INFO ANDMINE |      | PPJ: L�BIVIIMINE |
(0.5) seal on mingid `osakonnad `eraldi `Ida=ja   | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |
V: j:ah     | VR: MUU |     | VR: PARANDUSE HINDAMINE |
H: see on Ida politsei`osakond.    | IL: �LER�HUTAMINE |
aga=ta=on `P�hja politseiprefek`tuuri (.) �ks=`osakond lihtsalt see=Ida: Ida Ida `osakond.    | IL: �LER�HUTAMINE |
(2.0) Ida `jah on vel=   | IL: �LER�HUTAMINE |
V: =jah=      | VR: MUU |
H: =`eraldi.   | IL: �LER�HUTAMINE |
V: .hh e `Lastekodu kolgend=`�ks on Ida politsei`osakond.      | DIE: PAKKUMINE |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(2.5)
H: * ma=i=tea mingi Vikerlase=neli`teist. *        | DIJ: MITTEN�USTUMINE |
(1.0) aga: [{-}]   | YA: MUU |
V: [m:a vaatan] siin on veel erinevaid aadresse �ks=hetk,    | DIJ: EDASIL�KKAMINE |
(0.5) .hh (2.0) > teine oli Vikerkaare <   | KYE: AVATUD |     | PPE: MITTEM�ISTMINE |
H: Vikerlase neli`teist on siin ma=i=tea [{---}]   | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |
V: [`Vikerlase neli]`teist,   | DIJ: EDASIL�KKAMINE |
(1.0)
H: {-} seal v�i nad on `�ra kolind sealt.         | IL: T�PSUSTAMINE |
V: jaa ei siit k�ll=ei   | DIJ: INFO PUUDUMINE |
(.)
H: ei `n�e jah   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
V: ei < `n�ita > jah    | KYJ: N�USTUV EI |     | PPJ: L�BIVIIMINE |
(.) * ma vaatan uuesti. *    | DIJ: EDASIL�KKAMINE |
(1.2) ks=ma `pakun sis=se `Lastekodu `numbri.   | KYE: SULETUD KAS |     | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: jah.   | KYJ: JAH |
V: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
(2.0)
H: faksi `number {-}=   | DIE: SOOV |
V: =�ks=`hetk,    | DIJ: EDASIL�KKAMINE |
(...) h e neil on m�rgi < tud > > korrapidamistalituse < `lauanumber,      | DIJ: INFO PUUDUMINE |     | DIE: PAKKUMINE |
(1.5)
H: `faksi ei=`ole=jah   | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
V: jah,    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
`kuus �ks `kaks, (1.0) neli `kaheksa �ks `null.   | DIJ: INFO ANDMINE |
(1.2)
H: < selge. >    | VR: NEUTRAALNE PIIRITLEJA |
ait�h?   | RIE: T�NAN |
V: palun   | RIJ: PALUN |

   

 
   

 
