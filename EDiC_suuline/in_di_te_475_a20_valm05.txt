((475a20 telefonik�ne sotsiaalabisse))
((�htlustas Andriela R��bis 5.08.2005))

((kutsung))   | RIE: KUTSUNG |
V: sotsiaalabiosakond=   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
`tere   | RIE: TERVITUS |
H: � .hh tere p�evast.   | RIJ: VASTUTERVITUS |
 .hh vabandage, �kki te oskate `�elda kas:=e sellise murega saab midagi ette `v�tta.       | KYE: JUTUSTAV KAS |
(.) .hh ee on �ks `vanur, ta on `sisse kirjutatut=ee `Tartusse elab `L��nistes.    | YA: INFO ANDMINE |
(.) .hh e=tal on Tartus `l�hedaset aga nad vist ei `abista teda eriti `palju.   | YA: INFO ANDMINE |
 .hh igatahes tal on n��d gang`reen ja ta `k�ib ennast Tartus `ravimas.    | YA: INFO ANDMINE |
noh `sissekirjutuse `j�rgi.    | IL: T�PSUSTAMINE |
.hh ee ta peab siss noh �le `p�eva �le `kahe p�eva Tartus `k�ima?    | YA: INFO ANDMINE |
(.) ja siis ta noh n�gin t�na `almust `palumas et=e:t bussi`raha (.) saada.    | YA: INFO ANDMINE |
(.) ja=noh, (.) arvestades seda=et kaksnd=`kaheksa ja kaksnd=`kaheksa (.) .hh noh ta `t�epoolest ei=`tule=nagu sellest `pensionist `v�lja?    | YA: INFO ANDMINE |
aga ta ei `l�he oma `poja `juurde, .hh ee noh=ee `selgitab siis `sellega=et=et see poja`naine on `vastu.    | YA: INFO ANDMINE |
(0.5) .hh �   | YA: MUU |
V: ee ta `ela:- ee `sissekirjutus on `Tartus ja `kuskohas ta t�pselt [elab]   | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: � [ee] `L��nistes, se=on V�ru=`taga. �   | KYJ: INFO ANDMINE |      | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8)
V: V�ru `taga.   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: � m:hmh �   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
V: a mis `p�hjusel ta `seal `elab.   | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: � .hh v�i v�i noh ei=ole, t�ndab see on ikka `Tartu: Tartu `maakond.    | PA: ENESEPARANDUS |
(.) .hh ee t�hendab tal=on mingi `maa`maja, (1.0) `seal.    | KYJ: INFO ANDMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.0) kus=ta kus=ta=i kus=ta=i=saa `hakkama aga noh see `poeg=ja: ja ja `naine naine on `vastu et ta siia noh `elama `tuleks. �      | IL: �LER�HUTAMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8)
V: * mhmh *   | VR: NEUTRAALNE J�TKAJA |
H: � ja ta noh `eelistap=�� noh seda almust `paluda noh selle `asemel et sinna `��bima minna.   | IL: �LER�HUTAMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 ta `liigub �sna `vaevaliselt.       | IL: T�PSUSTAMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8) mt=.hh et et kas noh �   | KYE: MUU |
V: aga mis `p�hjusel tal see `sissekirjutus siin Tartu `linnas siis [on]   | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: � [.hh] vat `seda ma ei [`tea.] �   | KYJ: INFO PUUDUMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: [{-}] ta on `valla inimene st=et me ei `saa talle mingit=ee .hh e < `teenust `pakkuda    | KYJ: INFO PUUDUMINE |
kuna: > `v�ljaspool Tartu `linna meie `t��tajad ei `l�he.   | IL: P�HJENDAMINE |
H: � m:hmh=mhmh=mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.5) jah. �   | VR: MUU |
V: et v�ipola ta=ei=ole lihtsalt `kursis sellega sest ma saan aru k�ll jah et `vallas seda medit`siinilist poolt ei `ole,   | SEE: ARVAMUS |
(.)
H: � m:hmh �   | SEJ: PIIRATUD N�USTUMINE |
V: aga aga mis `p�hjusel ta siis nagu: ei ole ennast `valla inimeseks nagu registreerind=   | KYE: AVATUD |      | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
et siis otse `valla poole p��rduda kus ta `pidevalt `viibib.      | DIE: ETTEPANEK |
H: � .hh ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 .hh ee noh, ma=ei=`tea mis mis mis ned asjad `on    | KYJ: INFO PUUDUMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
aga aga `paistab et ned suhted `l�hedastega: noh on on `sassis=ja ja siis=ta=n v�ga `t�rges igasuguse abi suhtes.    | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.0) .hh aga noh, (0.5) p�him�tteliselt kas kas nagu ei `saagi mitte `midagi teha.   | KYE: JUTUSTAV KAS |
 v�i v�i v�i noh `on [.hh] �   | KYE: MUU |
V: [no] ma ei kujuta `ette mismoodi me temaga `tegeleme    | KYJ: INFO PUUDUMINE |
sellep�rast et et < `siia ma arvan ta ei `tule ja `meilt ei akka ju `keegi ka `valda s�itma. >   | IL: P�HJENDAMINE |
(.)
H: � [.hh ahah] �   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: [et kui]das me temaga nagu `tegeleme et v�ibolla .hh ee see `valla: sots   | KYJ: INFO ANDMINE |
 `teie olete n��d (.) m�ni `tuttav tal.   | KYE: VASTUST PAKKUV |      | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: � jah, mina olen `lihtsalt tuttav. �   | KYJ: JAH |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: et siis nagu see `valla sotsiaalt��taja koos siis `meie sotsiaalt��tajaga v�ibola saaksid mingile `kokkuleppele ja p�iavad `koos seda asja `lahendada.   | KYJ: INFO ANDMINE |
H: � mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
et sis `vallast proovida sotsiaal[t��tajat k�tte saada.] �   | KYE: VASTUST PAKKUV |      
V: [jah,    | KYJ: JAH |     
`tema saab ikkagi] `kohapeale minna ja selle inimesega `vestelda ja `veenduda kui: kui noh see=     | IL: P�HJENDAMINE |   
H: =� mhmh �   | VR: NEUTRAALNE J�TKAJA |
V: `masendav see `olukord seal `on=et   | IL: P�HJENDAMINE |
H: � mhmh �   | VR: NEUTRAALNE J�TKAJA |
V: et `millist abi talle pakkuda mida ta `ise asjast arvab ja ja ni=`edasi.   | IL: P�HJENDAMINE |
H: � mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) [`selge.] �   | VR: NEUTRAALNE PIIRITLEJA |
V: [et v�ibla] `niipidi ja sis kuna se `sissekirjutus on siin siis v�ib nagu `meie teenistusega ka `�hendust v�tta ja ja `uurida.   | IL: KOKKUV�TMINE |
H: � mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) `selge. �   | RIE: L�PUSIGNAAL |
V: jah   | RIJ: L�PETAMISE VASTUV�TMINE |
H: � .hh tohutu suur ait�h teile. �   | RIE: T�NAN |
V: head [aega.]   | RIE: H�VASTIJ�TT |
H: � [n�ge`mist] �   | RIJ: VASTUH�VASTIJ�TT |

   

 
   

 
   

 
