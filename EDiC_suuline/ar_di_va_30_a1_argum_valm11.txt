((30_a1 argivestlus))
((�hestas Andriela R��bis 4.11.2011))

K: ((l�heb kapi juurde ja v�tab leiva)) ei ma ikka ei `saa oma `rosina`leiba j�tta.    | SEE: V�IDE |
(1.6)
L: hmmh       | VR: MUU |
(1.5)
K: a=`sulle see ei `meeldind=v�.     | KYE: SULETUD KAS |
(0.7)
L: < ei, >   | KYJ: N�USTUV EI |
 `mitte `midagi ise`�ralikku ma ei `tunne tem�st.     | SEE: ARVAMUS |
(1.3)
K: mis `ise�ralikku sa=sin=`tundma pead,      | KYE: AVATUD | | YA: RETOORILINE K�SIMUS || SEE: V�IDE |
(* leib on leib. *)   | SEE: V�IDE | 
L: (et=ta et=ta] `parem peaks olema.   | KYJ: INFO ANDMINE | | SEJ: MUU |    | SEE: ARVAMUS |
(2.0) ku `tavaline leib, no sii m�ni `rosin s��l `sihen om   | KYJ: INFO ANDMINE |    | SEJ: MUU | | SEE: ARVAMUS |
(1.0)
K: no `kihvt=ju, m�nus `omap�rane `maitse.     | SEJ: MITTEN�USTUMINE |   | SEE: ARVAMUS |
(0.8)
L: mhmh?    | SEJ: PIIRATUD N�USTUMINE |
(5.2)
K: `tervislik.   | SEE: V�IDE |
 (5.6) palju `tervislikum ku rosina`sai.    | SEE: V�IDE |
(3.0)
L: `mina=i=`tia, nojah le- ta=n `leib.    | SEJ: PIIRATUD N�USTUMINE |     
(3.0)
K: `rosinad on tervislikud.    | SEE: V�IDE |
 ja `leib on tervislik.    | SEE: V�IDE |
L: mhmh?    | SEJ: PIIRATUD N�USTUMINE |
(0.8)
K: kusjuures see on veel `sellest (1.4) mmmmmm `rukki `liht`jahust.    | SEE: V�IDE |
(7.1) ma=i=tea.    | IL: MUU |
(3.1)
L: ei=tia igal=jul ta k- `k(h)�sis `t�na=et kas teil seda `laste shokulaadi seda `Shnikersit ei `ole ka.    | YA: INFO ANDMINE |     | TVE: PAKKUMINE |
(0.7) @ `ei: ole. @   | YA: INFO ANDMINE |     | TVE: PAKKUMINE |
 (1.9) no t(h)ema (0.2) $ r(h)aadio=televiisorist `n�ge. $    | YA: INFO ANDMINE |     | TVE: PAKKUMINE |
(0.6) es taht muidugi lastele `osta.   | YA: INFO ANDMINE |     | TVE: PAKKUMINE |
(0.4)
K: mis `lapsi- `lastele.    | TVJ: VASTUV�TMINE |  | KYE: AVATUD | | PPE: MITTEM�ISTMINE |
(.) mis=`kus tal ned `lapsed on.     | TVJ: VASTUV�TMINE | | KYE: AVATUD || PPE: MITTEM�ISTMINE |
(0.3)
L: no: aga (.) ma `arvatsi: (0.4) ����=mm tal=om=jo `kaks last `s��l.=    | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE | 
K: =`temal.    | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE | 
L: [no]    | KYJ: MUU |     | PPJ: MUU | 
K: [tal ei] `olegi ju [`lap]si.    | SEE: V�IDE | | PA: MUU |
L: [no=ta]      | KYJ: MUU |     | PPJ: MUU |
L: no=aga no `lapse s��l `majan > no ta `neile tahtigi `osta <   | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |   | SEJ: MUU |
�tel kas `j�ulud {`saadagi kusk-.}       | YA: INFO ANDMINE |
(0.6) �tel=no ei `tea.       | YA: INFO ANDMINE |
(1.3)
K: a=mingit `muud shokolaadi ei v�i sis osta.    | KYE: SULETUD KAS | | YA: RETOORILINE K�SIMUS | | SEE: V�IDE |
`Snikers on �ltse `vastik mingi `venib nigu (.) `tatt.       | SEE: V�IDE |
(0.9) p�h.      | SEE: V�IDE |
(0.8)
L: no=aga ta (0.7) ta: rek`laamib teda `irmsald.     | SEJ: MUU | | SEE: V�IDE |
(1.3)
K: `venib nigu `tatt nigu (.) mingi `tatt (0.5) p�h- (0.4) mm (0.2) t�hendab `p�hklid `tatis ma �tleks.    | SEJ: MUU |  | SEE: V�IDE |
L: `Siivi t�i `ka `�kskord `siia teda.   | YA: INFO ANDMINE |
K: `Snikersit=v�.   | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE |
(0.4)
L: `kas=tas `too siss.   | KYE: SULETUD KAS |   | PPE: MUU | 
K: �q��.    | KYJ: EI |    | PPJ: L�BIVIIMINE |
(2.3) `Snikershit ta `k�ll ei=ole=`toond.      | IL: T�PSUSTAMINE | | PPJ: L�BIVIIMINE | 
L: mq (1.3) mis {`n�ll} se=se `ribade `viisi=ja   | KYE: AVATUD | 
K: a=se=on=ju `Kinder, (.) `Kinder shokolad.   | KYJ: INFO ANDMINE |   | SEE: V�IDE | 
L: ah=`jaa.       | VR: MUU |
(0.3) `jah se=oli `Kinder `jah.    | SEJ: N�USTUMINE |
(1.1) mh    | VR: MUU |
(4.5) mh   | VR: MUU |
 (2.1) ((s��giriistade kolin)) `Snikersil pidi olema `piima palju `sihan.    | SEE: V�IDE |
(1.1)
K: `Snikersil ei=`ole `piima,   | SEJ: MITTEN�USTUMINE |
 `Snikersil on `p�hklid.   | SEE: V�IDE |
 `Kinder=shokuladil on `piima.    | SEE: V�IDE |
(0.5)
L: ahah.    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
      

