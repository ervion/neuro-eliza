((357a4 telefonik�ne reisib�roosse))
((�htlustas Andriela R��bis 18.11.2003))

  ((kutsung)) | RIE: KUTSUNG |
V: {--}   | YA: PRAAK |    | RIJ: KUTSUNGI VASTUV�TMINE |
H: tere,   | RIE: TERVITUS |
ma sooviksin `Pariisi `reiside kohta `k�sida.   | DIE: SOOV |
V: jaa?   | VR: NEUTRAALNE J�TKAJA |
(.)
H: et `mis perioodil oleks v�imalik (.) `s�ita.   | KYE: AVATUD |
(0.6)
V: kahek�mmne `kuuendal.   | KYJ: INFO ANDMINE |
(.) [ok]toobril.   | IL: T�PSUSTAMINE |
H: [* ahah *] | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.4) ja: kui `pikk on see `reis.    | KYE: AVATUD |
kas see on: (.) `grupireis.   | KYE: SULETUD KAS |
(.)
V: ee see on {n�d}`jah `grupina.   | KYJ: JAH |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
V: a:ga kui te tahate `lennata siis on (.) kuusteist `november,   | KYJ: INFO ANDMINE |
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.4) et: kui ma individu`aalselt `l�heksin (.) `on teiega v�imalik `minna.   | KYE: SULETUD KAS |
(0.8)
V: e:i   | KYJ: EI |
H: ei `ole, (.) `ainult grupi`reisid.   | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.6) a:ga `k�siks siis (.) `grupireiside kohta=et ee (.) `m:ida: see pakett `sisaldab.   | KYE: AVATUD |
(1.0)
V: ee see on n�d bussi {--}   | KYJ: INFO ANDMINE |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
(0.6)
V: {kus on `sees on siis e} `bussi `s�it?   | KYJ: INFO ANDMINE |
`mugavustega buss, eestikeelne `kiid (.) ekskurs`joonil, (0.4) ja: (0.4) `��majad koos siis `hommikus��kidega.   | KYJ: INFO ANDMINE | 
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.2) aa `kui: suur on (.) `kui: palju see `maksaks.   | KYE: AVATUD |
(1.0)
V: {---}   | YA: PRAAK |
 (9.0) ((paberite kr�bin)) see on n��d `t�iskasvanu hinnaga kolm=tuhat `kuussada �heksakend {`kaheksa}   | KYJ: INFO ANDMINE |
H: aga kas `�li?pilasel on v�imalik `soodustust saada.   | KYE: JUTUSTAV KAS |
V: `Leen oled `jah.   | KYE: VASTUST PAKKUV |
(0.4)
H: $ ei $   | KYJ: EI |
V: mhemhe @ `t�itsa totu. @ | VR: HINNANGULINE VASTUV�TUTEADE |
((l�busa h��lega))
(2.0)
H: halloo?   | KKE: ALGATUS |
V: jaa?   | KKJ: KINNITAMINE |
(.)
H: ee (0.4) ma=i saanud n��d `aru, kas on `v�imalik �li�pilastele `soodustust.   | KYE: JUTUSTAV KAS |
V: ei �li�pilastele ei `ole kahjuks.   | KYJ: EI |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
(0.4)
V: et ee jah see `t�iskasvanu hind kehtib | IL: �LER�HUTAMINE |
(.)
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
V: =kehtib `k�igile,   | IL: �LER�HUTAMINE |
V: kui on n��d tegemist `alla (.) kaheksateist {aastasega}   | KYJ: INFO ANDMINE |
H: mhmh   | VR: NEUTRAALNE J�TKAJA |
V: {* siis on soodustus. *}   | KYJ: INFO ANDMINE |
(0.8)
H: ee `kas on vajalik `reisi `kindlustus.   | KYE: JUTUSTAV KAS |
(0.4)
V: {< reisi kindlustus > n�d} ei `ole vajalik | KYJ: EI |
 aga `tervisekindlustuse osa `k�ll, {see=on `sada `krooni osadel.}   | KYJ: INFO ANDMINE |
(.)
H: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.6) selge?   | VR: NEUTRAALNE PIIRITLEJA |
(0.4) a:ga `kui palju `varem peaks (.) `proneerima pileti.   | KYE: AVATUD |
(1.0)
V: senikaua kuni `kohti `on `senikaua me {neid} `m��me.   | KYJ: INFO ANDMINE |
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.0) selge    | VR: NEUTRAALNE PIIRITLEJA |
(0.4) > ait�h siis [teile.] <   | RIE: T�NAN |
V: [jah=]`palun?   | RIJ: PALUN |
(.) head=aega?    | RIE: H�VASTIJ�TT |

   

   

 
   

 
