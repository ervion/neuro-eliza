((97b1 telefonik�ne ortopeediafirmasse))
((�htlustas Andriela R��bis 20.11.2003))

((kutsung))   | RIE: KUTSUNG |
V: hallo?   | RIJ: KUTSUNGI VASTUV�TMINE |
H: mt tere   | RIE: TERVITUS |
V: tere?   | RIJ: VASTUTERVITUS |
H: ee kas te `oskate �elda, millal `jalan�usid saab `tellida.    | KYE: AVATUD |

(0.8)
V: ee t�hendab ee kas `lapsele jah?   | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |     | KYE: VASTUST PAKKUV |
H: jah    | KYJ: JAH |

(0.8)
V: ee tulete `siia=ja (0.5) koos `lapsega=ja (.) m��dame �ra=ja    | KYJ: INFO ANDMINE |

(0.5)
H: mm aga: mingid kindlad `p�evad pidid olema v�i enam ei `ole.   | KYE: AVATUD |
V: ee t�hendab=ee kella `�heksast kella `neljani.    | KYJ: INFO ANDMINE |
(.) ja me paneme p�eva `kirja.    | DIE: ETTEPANEK |
`oodake korraks.    | DIE: PALVE OODATA |

(.)
H: jah    | DIJ: N�USTUMINE |

(55.0)
V: halloo?   | KKE: ALGATUS |
H: jaa?   | KKJ: KINNITAMINE |
V: kas teil `omme `sobib.   | KYE: SULETUD KAS |
H: ee `sobib `k�ll jah.   | KYJ: JAH |
V: ee:=�tleme et=e kella `k�mnest=ee=h (.) `neljani on `vaba.          | YA: INFO ANDMINE |
(1.0)
H: �� siis paneks (2.0) kas kell `kaks=v�i v�i pool `kolm=v�i.      | DIJ: N�USTUMINE |
V: ��=hh (.) pool=`kolm jah?   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: jah.    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(.)
V: ee ja kuidas `nimi on.   | KYE: AVATUD |
H: Saabas.    | KYJ: INFO ANDMINE |

(.)
V: Saabas jah.   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: jah    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(3.2)
V: nii,    | YA: JUTU PIIRIDE OSUTAMINE |
`jalatsid jah?=   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: =jah.   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
V: a `v�tke laps kindlasti `kaasa.=   | DIE: ETTEPANEK |
H: =jaa.    | DIJ: N�USTUMINE |

(0.5)
V: ja mis `jalatsit te tahate.   | KYE: AVATUD |
H: ee `suveks mingeid san`daale ja: ja `saapaid.   | KYJ: INFO ANDMINE |
V: ahah.    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
no `selge,   | VR: NEUTRAALNE PIIRITLEJA |
 v�tke laps `kaasa jah,   | DIE: ETTEPANEK |
H: jah   | DIJ: N�USTUMINE |
V: tulge `siia,    | DIE: ETTEPANEK |
me m��dame `�ra=ja (.) saame (.) `tellimuse anda.    | IL: KOKKUV�TMINE |
no meil on `midagi `kohapeal ka sin nagu.   | IL: T�PSUSTAMINE |
H: jah   | DIJ: N�USTUMINE |
V: selge.    | VR: NEUTRAALNE PIIRITLEJA |
n�gemist?   | RIE: H�VASTIJ�TT |
H: ait�h?   | RIE: T�NAN |


   

 
