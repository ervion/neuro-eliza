((423a11 telefonipood))
((�htlustas Andriela R��bis 11.12.2003, kontrollitud 27.10.2006))

O: tere Vilja.   | RIE: TERVITUS |
M1: tere.   | RIJ: VASTUTERVITUS |

O: hehe kas sa nende mobiilidega ka ikka `tegeled veel.   | KYE: SULETUD KAS |
M1: jaa aga millega siis `veel.   | KYJ: JAH |
 (.) noh | VR: NEUTRAALNE J�TKAJA |
O: kuule tead mul on `selline asi=et   | YA: EELTEADE |
M1: =millega sa `hakkama said.   | KYE: AVATUD |
O: `mina ei saand `millegagi hakkama.   | KYJ: INFO ANDMINE |
M1: mhmh   | VR: NEUTRAALNE J�TKAJA |
O: mul on kodus `kassipoeg, (.) ja vaata mis ta `tegi.   | YA: INFO ANDMINE |
M1: n�ris.   | SEE: ARVAMUS |
(0.8)
O: kas siia uut `saab=v�.   | KYE: JUTUSTAV KAS |
(0.5)
M1: mm (.) aga ei=`sega sind=ju.   | KYE: VASTUST PAKKUV |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
O: ei   | KYJ: EI |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 aga kas ta noh n�iteks=ee   | KYE: JUTUSTAV KAS |
M1: =�heksakend krooni on uus.     | KYJ: INFO ANDMINE | 
O: aa.   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) a kas ta nagu noh   | KYE: JUTUSTAV KAS |

(0.5)
M1: ku=ta levi alla ei `t�mba, sis ei=ole h�da `midagi.   | KYJ: INFO ANDMINE |

(0.5)
O: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M1: {-} ee (.) [`m�nel] on palju `hullem.   | KYJ: INFO ANDMINE |

O: [kas {-}]   | YA: PRAAK | 
O: ma m�tlen=et kas see on $ `suur vigastus=v�i=v�i $   | KYE: JUTUSTAV KAS |
M1: `ei=ole.   | KYJ: EI |
 {-} siin on `sees nagu sellised metallist spir- spiraalid ja `need (.) kui `need �ra `murduvad v�i midagi, noh see on lihtsalt ka- kaitsega see.   | KYJ: INFO ANDMINE |
O: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.5)
M1: mis sul on uus `kassipoeg siis=v�i.   | KYE: SULETUD KAS |
O: jah   | KYJ: JAH |
M1: `kui (.) kui n��d `levi hakkab k�vasti `k�ikuma,      | KYJ: INFO ANDMINE |
(.) 
O: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M1: siis v�iks vaadata.      | KYJ: INFO ANDMINE |
 (.) aga uus antenn on `�heksakend krooni.     | KYJ: INFO ANDMINE | 
O: a mis need `Eeriksonide `akud maksavad.   | KYE: AVATUD |
M1: mm (.) `sellele on kahesaja �heksa`k�mnega n��d see (.) `�hemad, (1.5) jah, sul on see {-} aku.   | KYJ: INFO ANDMINE |
 (...) {---}   | YA: PRAAK |
O: ei ma lihtsalt `huvitun.      | YA: MUU |
(...) ((poem�ra))
M2: {---}   | YA: PRAAK |
O: `kass n�ris `�(h)ra.     | YA: MUU | 
M1: n�ed, `sellised on uued.     | KYJ: INFO ANDMINE | 
O: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(1.0)
M1: kakssada �heksakend `krooni.   | KYJ: INFO ANDMINE |
 (.) `�hemad tunduvalt kui `sinu oma.     | IL: T�PSUSTAMINE | 
M2: ja samas (.) `mahtu rohkem.     | IL: T�PSUSTAMINE | 
M1: `mahtu on `rohkem jah.   | IL: �LER�HUTAMINE |
M2: [et peab `kauem vastu.]   | IL: J�RELDAMINE |
M1: [{---}] {-}   | YA: PRAAK |
O: mhmh   | VR: NEUTRAALNE J�TKAJA |
M1: {---}   | YA: PRAAK |
M2: `kergem ta ei=`ole.      | IL: T�PSUSTAMINE |
 (.) `kaalu poolest `sama.     | IL: T�PSUSTAMINE | 
O: mhmh   | VR: NEUTRAALNE J�TKAJA |
M1: sul on ikka praegu siin igavene `suur.   | IL: HINNANG |
M2: seal on `�hku k�vasti sees {akul.}     | IL: T�PSUSTAMINE | 
M1: hehe   | YA: MUU |
O: a=kas sellele `Eeriksonile neid teisi: (.) noh `katteid on neid, (.) esipa`neele teist `v�rvi midagi.   | KYE: JUTUSTAV KAS |
(...)
M2: {---}   | YA: PRAAK |
 beezh.   | KYJ: INFO ANDMINE |
(0.5)
O: mhmh   | VR: NEUTRAALNE J�TKAJA |
(...)
M1: see palju ei=`erine sinu omast praegu.   | SEE: ARVAMUS |
O: ei ma vaatan [jah,] see on samasugune `sinine=et lihtsalt=et   | SEJ: N�USTUMINE |
 (0.5) 
M1: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
O: mida on mida `on.   | YA: MUU |
 (0.5) a=ma just `m�tsin=et �kki ma pean uue antenni ostma et=ee   | KYE: JUTUSTAV KAS |
M1: ei, | KYJ: EI |
`see [ei tohiks] `see ei tohiks `midagi teha kui see praegu {-} ei h�iri sis   | KYJ: INFO ANDMINE |
O: [et see]   | YA: MUU |
(0.5)
O: ahah   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.5) aga antennid on muidu ka t�itsa noh `samasugused v�i on [midagi teist.]   | KYE: JUTUSTAV KAS |   
M1: [jah,] on pike- on `l�hemaid `ka.   | KYJ: INFO ANDMINE |
 (.) ja on (0.5) ma=i=tea kas sellele `vilkuvaid `on v�i {-} l�bipaistvaid.   | KYJ: INFO ANDMINE |     
O: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M1: aga on `l�hemaid `ka sellele.   | KYJ: INFO ANDMINE |
(0.5)
O: mhmh   | VR: NEUTRAALNE J�TKAJA |
M1: a=mitte `nii pikad.   | KYJ: INFO ANDMINE |
 no see on �ks pikemaid.   | IL: T�PSUSTAMINE |
(0.5)
O: ja mis see maksis `�heksa[kend krooni.]     | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
M1: [�heksakend] `krooni on jah.       | KYJ: JAH |     | PPJ: L�BIVIIMINE |
 (.) ja see on `kakssada �heksak�mend, aga see on n��d jah      | IL: T�PSUSTAMINE |
 {---}   | YA: PRAAK |
O: `kaua need `Eeriksonide akud muidu tavaliselt `peavad.   | KYE: AVATUD |
M1: =aasta.   | KYJ: INFO ANDMINE |
O: aasta.      | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
M1: {---}   | YA: PRAAK |
 hakkab niikaugele juba minema `ka.    | KYE: JUTUSTAV KAS |
O: sep`tembris `ostsin.   | KYJ: INFO ANDMINE |
M1: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) a palju (.) `mitu p�eva ta `peab.   | KYE: AVATUD |
O: kuskil kolm neli vastavalt `helista[misele.]   | KYJ: INFO ANDMINE |
M1: [siss=ei]=ole: h�da `midagi.   | VR: HINNANGULINE VASTUV�TUTEADE |
 (0.8) kruvisid `lahti ei=`aja=v�.   | KYE: JUTUSTAV KAS |
 (0.8) sul ei=`ole seda viga jah.   | KYE: VASTUST PAKKUV |
O: ei=`ole.   | KYJ: N�USTUV EI |
M1: nendel mudelitel on see=et need `kruvid kipuvad `lahti tulema noh mis tal `sees on.   | YA: INFO ANDMINE |
 (1.0) noh siin `Nogia (.) siin on n��d niuksed `kruvid, (.) et kui `pilti hakkab `eest niiviisi `�ra pilduma kui n�iteks natuke `v��nad=et=sis (.) `tavaliselt on kruvide `viga, et kruvid [tulevad] lahti jah,   | YA: INFO ANDMINE | 
O: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
O: mhmh   | VR: NEUTRAALNE J�TKAJA |
M1: aga need on muidugi {-} [aga sul on] `kott `�mber.   | YA: INFO ANDMINE |
O: [{-}]   | YA: PRAAK |
O: mul oli algul `olid=e (.) noh lihtsalt {nagu} `kiilus `kinni kuidagi mingi `kiri on ees ja mitte mingid `klahvid ei `aita ja siis l�litas ennast `v�lja, (.) siis ma k�isin mobiilipoes siis ta `puhastas ja=s=ma ostsin [`koti `�mber igaks juhuks   | YA: JUTUSTAMINE |
 {---}]   | YA: PRAAK |
M1: [{---}]   | YA: PRAAK |
 need `klemmid kipuvad `�ra minema siin.   | YA: INFO ANDMINE |
 (.) {-} natuke `�le k�ia.   | DIE: PAKKUMINE |
(0.5)
O: aa siis `k�ll jah.   | DIJ: N�USTUMINE |
(0.5)
M1: `saad `veel natukene: hoida raha alles.   | IL: �LER�HUTAMINE |
(.)
O: jah, (.) siis on `hea.   | VR: HINNANGULINE VASTUV�TUTEADE |
 [(.) ma `m�tsin]=et `�kki (.) [�kki: `vigastas `hullusti.]   | SEE: ARVAMUS |
M1: [{---}]   | YA: PRAAK |
 [ei, see ei `ole.]   | SEJ: MITTEN�USTUMINE |
 (.) ei ole kui ta p�ris niivisi `seest ikka (.) noh `murdub v�i niiviisi [siis] on juba vaja, kui tal hakkab see levi kohe `k�ikuma.   | IL: T�PSUSTAMINE |
O: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
O: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M1: �tleme=et kui ta (.) `ikka niivisi=et `�ks pulk on ja=sis viskab j�lle `t�is, vot `siss on midagi `viga.   | IL: T�PSUSTAMINE |
O: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M1: a=kui=ta sul `praegu ainult �he ja kahe vahel [`k�igub] niivisi=et l�heb `�ra  | IL: T�PSUSTAMINE |
O: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M1: [seal] ei=ole   | IL: T�PSUSTAMINE |
O: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS | 
O: siis `k�ll.   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) siis on `hea.   | VR: HINNANGULINE PIIRITLEJA |
M1: siis peab ta sul `vastu.   | IL: KOKKUV�TMINE |
 noh, (.) l�hed `kooli poole=v�.   | KYE: VASTUST PAKKUV |
O: ei veel ei=`l�he.   | KYJ: EI |
M1: veel ei=`l�he.      | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
O: ostan kooli`asju.      | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |
 hehe   | YA: MUU |
M1: $ vaeseke $   | SEE: ARVAMUS |
O: jah   | SEJ: N�USTUMINE |
M1: $ oh sa `pisikene $ hehe   | SEE: ARVAMUS |
O: $ ait�h.   | RIE: T�NAN |
 [head =aega] $   | RIE: H�VASTIJ�TT |
M1: [n�gemist]   | RIJ: VASTUH�VASTIJ�TT |


   

 

      
