((531_a28 infotelefon))
((�hestas Andriela R��bis 14.12.2008))
((kutsung))   | RIE: KUTSUNG |
V: `infotelefon=   | RIJ: KUTSUNGI VASTUV�TMINE |   | RY: TUTVUSTUS |
Kersti    | RY: TUTVUSTUS |
`tere.   | RIE: TERVITUS |
(0.2)
H: .hhh (.) h (.) `te:re `p�evast=�.    | RIJ: VASTUTERVITUS |
(.) .hh � `kas te `�tleks mulle `Rakvere <*`s*pordi>`hoone.   | KYE: JUTUSTAV KAS |
 ((2.v�lde)) (0.5) ee `t�pse `aadressi ja `posti`indeksi.      | IL: T�PSUSTAMINE |
(0.2)
V: ja:   | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.2) �ks `hetk.   | KYJ: EDASIL�KKAMINE |
(12.5) ((telefonihelin, arvutiklaviatuuri kl�bin))
V: .hh (0.6) on=��=`Rakvere `spordi`keskus.     | KYJ: INFO ANDMINE |   | DIE: PAKKUMINE |     
 `Kastani `puiestee `kaks`teist `Rakvere.       | KYJ: INFO ANDMINE |    | DIE: PAKKUMINE |
(1.2) ((mingi h��l))
H: ei:   | DIJ: MITTEN�USTUMINE |     
`see peab `olema `spordi`oone   | DIE: SOOV |
aga `ma (0.2) ma=i=`tea kas ta `n��d `aktsia`selts > v�i=v�i=v�i < `mis ta `on.       | IL: T�PSUSTAMINE |       
.hh ee (0.2) ma `tean et `ta=on `V�imla (0.4) `V�imla `�ks.      | IL: T�PSUSTAMINE |    
(0.2) et tal on see `ainuke `�ige `aadress.      | IL: T�PSUSTAMINE |      
(0.6)
V: ee=`V�imla `�ks on `spordiklubi `Rakvere.         | DIJ: INFO ANDMINE |    
(1.0)
H: ee `spordik- `klubi `Rakvere:.   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(.)
V: jah   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(.)
H: a (0.4) `midagi mis {-} `infot v�i `midagi=i=`ole.    | KYE: MUU |     
(0.5)
V: `ettev�tlus`vormi ei `ole siin `m�rgitud.    | KYJ: INFO PUUDUMINE |     
(0.4)
V: et   | YA: MUU |
H: ei:   | YA: MUU |
 (0.3) p=�=mm (0.6) {ei}   | YA: MUU |
 (0.4) `Rakvere jah.   | KYE: VASTUST PAKKUV |        | PPE: �LEK�SIMINE |
(0.5)
V: j:ah    | KYJ: JAH |    | PPJ: L�BIVIIMINE |
(0.2) 
V: kas:=ee (.) `telefoni ka `soovite.    | KYE: SULETUD KAS |     
(0.7)
H: jah    | KYJ: JAH |     | DIE: SOOV |
(0.2)
V: .h (.) ee=`number > `kinnitamata andmetel < `kolm `kaks `viis    | DIJ: INFO ANDMINE |
(1.9)
H: [{kolm}]   | YA: MUU | 
V: [{-}]   | YA: PRAAK |
(0.7)
H: `kolm `kaks `viis=   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
V: =jah    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.2) `null `viis    | DIJ: INFO ANDMINE |
(0.3)
H: *mhmh*   | VR: NEUTRAALNE J�TKAJA |
(0.3)
V: `�heksa `kaks.   | DIJ: INFO ANDMINE |
(.)
H: .hh (.) `ei `l�he `ikka `kokku=hh.    | YA: INFO ANDMINE |
(.) `minul on `siuke `number .hhh ee < `kolmsada `kakskend `kaks > (.) < `kolgend `kaks > (0.2) < `�heksa`tei:st. >    | YA: INFO ANDMINE |
(0.4) ja `peaks nagu `olema `Rakvere `spordi`oone.      | DIE: SOOV |
(4.5)
H: et et e[t]   | YA: MUU | 
V: * [a]ga: (.) `aadress on k�ll `sama. *       | SEE: V�IDE |
(1.2)
H: {t�esti}      | SEJ: MUU |
(.)
V: v�i on `Rakvere=<spordi`keskus> � `Kastani puiestee kaks`teist.       | DIJ: INFO ANDMINE |  | DIE: PAKKUMINE |   
(0.6)
H: ei no (0.2) `see peaks `olema `jah `V�imla t�nav `�ks.      | DIJ: MITTEN�USTUMINE |    
 aga `ei tea `kuidas selle `t�pne `nimetus on *`kes*=seda `�tleb.     | IL: MUU |  
(1.2)
V: .hh (0.2) ee `siis on `n�iteks sellel `aadressil `veel=ee `Rakvere `spordi`selts `Kalev.    | DIJ: INFO ANDMINE |   | DIE: PAKKUMINE |
(1.2)
V: v�ib=olla `see siis.     | SEE: ARVAMUS |
(0.3)
H: `V�imla �ks.     | KYE: VASTUST PAKKUV |        | PPE: �LEK�SIMINE |
(0.2)
V: jah    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.4)
H: ee `Rakvere:=   | KYE: AVATUD |    | PPE: MITTEM�ISTMINE | 
V: =`spordi`selts (0.2) `Kal[ev.]   | KYJ: INFO ANDMINE |    | PPJ: L�BIVIIMINE | 
H: [`Kale]*v*    | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(0.4)
V: j:ah   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.2) .hh (0.2) ja: `siin on:= `number `kolm kaks `kaks `kolm kaks �ks `�heksa.   | DIJ: INFO ANDMINE |
(0.2)
H: j:ah    | DIJ: N�USTUMINE | | SEJ: N�USTUMINE |
 (.) .hhh < `Rakvere `spordi`selts `Kalev. >     | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.3) mis=`se=on=   | KYE: SULETUD KAS |
V: =jah=   | VR: NEUTRAALNE J�TKAJA |
H: =see `t�pne `nimetus.=   | KYE: SULETUD KAS |
V: =jah   | KYJ: JAH |
(0.3)
H: ai`t�h `teile.   | RIE: T�NAN |
 head `p�[eva.]   | RIE: SOOVIMINE |
V: [jah]   | RIJ: MUU |
 (.) ead `p�eva.   | RIJ: VASTUSOOVIMINE |
      
