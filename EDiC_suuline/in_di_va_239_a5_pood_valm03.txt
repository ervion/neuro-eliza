((239a5 lillepood))
((�htlustas Andriela R��bis 11.01.2004, kontrollitud ja VTE, VTJ lisatud 22.10.2006)) 

O1: meil on �ks `k�simus nende toa`rooside kohta=et (.) [kuidas]   | KYE: MUU |
M: [need on] ee tegelikult `�ue`roosid.     | PA: PARTNERI PARANDUS |
O1: tegelt `�ue.       | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.5) aga=   | YA: MUU |
M: =talvel lihtsalt `kasvatatakse neid toas.   | IL: T�PSUSTAMINE |
O1: jaa    | VR: NEUTRAALNE VASTUV�TUTEADE |
aga nad (0.5) `suureks nad `kasvavad sis.   | KYE: AVATUD |
M: no `laiemaks.   | KYJ: INFO ANDMINE |
O1: `laiemaks lihtsalt, `k�rgemaks nad enam [ei l�he.]   | KYE: JUTUSTAV KAS |    
M: [seal on] potis kuskil kolm neli `taime nii et    | KYJ: INFO ANDMINE |
O1: aa   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M: k�ik `eraldi istutada {---}    | KYJ: INFO ANDMINE |
O1: ja=sis=tuleb (.) kas neil (.) palju neid `�isi tuleb.   | KYE: AVATUD | 
(0.8)
M: `k�mme kuni kakskend=`viis.   | KYJ: INFO ANDMINE |
 (2.0) see on k�ll [t�itsa] `suvaline kuidas te viitsite neid=ee (.) `v�etada ja `kasta.   | IL: T�PSUSTAMINE |
O1: [nojah]   | VR: NEUTRAALNE J�TKAJA |
O1: mhmh      | VR: NEUTRAALNE J�TKAJA |
(0.5)
M: viib inimene siit `koju ja v�ibla kolme p�eva p�rast ta=on on `surnud.   | YA: INFO ANDMINE |
 (.) nad tahvad `iga `p�ev `vett.      | YA: INFO ANDMINE |
(.)
O1: iga p�ev `vett.       | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) a selles m�ttes=et `kuhu teda `istutada ka- kas [ku-]   | KYE: AVATUD |
M: [no] `muru sisse {-} tehakse neid k�nnitee: `hekke,   | KYJ: INFO ANDMINE |
O1: aa=et=ee   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M: muster`peenardesse pannakse kuhu iganes.   | KYJ: INFO ANDMINE |
O1: `valguse suhtes ja `soojuse [suhtes]   | KYE: AVATUD |
M: [no] p�ikest `p�ikest ikka tahavad `rohkem saada jah.   | KYJ: INFO ANDMINE |
(.)
O2: `talveks tuleb uuesti `sisse tuua siis=v�.    | KYE: JUTUSTAV KAS |
M: kat- katate `kinni nagu roosid.   | KYJ: INFO ANDMINE |
O2: mm   | VR: NEUTRAALNE J�TKAJA |
(1.0)
M: no see on ikka linna- `linnainimese mingi: `silmailu lihtsalt [kui] ta `potti pistetud on.    | YA: INFO ANDMINE |
O1: [aa]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(1.0) 
M: tegelikult ta on ikka `muru`roos.   | YA: INFO ANDMINE |
O1: mm    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.8) no aga sest=et `osad on (.) mis ei=ole `�ltse sellised mis mis nagu toas poti sees need `v�iksed `roosid on need on tegelikult k�ik `�ueroosid=ve.    | KYE: JUTUSTAV KAS |   
{-}: {---}   | YA: PRAAK |
O2: `need on siis sama`moodi (.) `�ue omad=v�.   | KYE: JUTUSTAV KAS |     
(1.8)
M: lihtsalt=ee ma=�tlen=et=ned=on `linnainimese: `silmailuks.     | KYJ: INFO ANDMINE |   
(0.5) `roos on lihtsalt (.) pistetud `potti=ja=ja (0.8) arvatakse=et ta on `igavene kui ta (.) `toas `kasvab.   | KYJ: INFO ANDMINE |
 (4.5) kevadel v�lja: saab istutada kui maa juba `suland on, (.) [�itseb] suvi `l�bi.   | KYJ: INFO ANDMINE |
O1: [mm]   | VR: NEUTRAALNE J�TKAJA |
(0.5)
O1: a=no mis tempera- (.) nojah et kui kui soe `v�ljas peab olema et ta noh ikka `kas[vama l�heks.]   | KYE: AVATUD |   
M: [nii kuidas te] `istutada saate juba ikka.    | KYJ: INFO ANDMINE |
O1: aa   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
M: aprill, mai      | KYJ: INFO ANDMINE |
O1: mhmh   | VR: NEUTRAALNE J�TKAJA |
M: oleneb `kevadest siin `p�hjamaal on vahest lumi `maikuus ka maas siis ei ole m�tet `istutada.   | KYJ: INFO ANDMINE |
 (2.2) `istutamise aeg on t�pselt siis kui=hh (.) kui maa on `sulanud.   | IL: KOKKUV�TMINE |
O1: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) a=mis huvitav lill `see on.   | KYE: AVATUD |   | TVE: PAKKUMINE |
(0.8)
M: h�pe`roomia.   | KYJ: INFO ANDMINE |   | TVJ: VASTUV�TMINE |
 (1.5) kasvab `pimedas.   | IL: T�PSUSTAMINE |
O1: `pimedas p�ris.      | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE |
O2: kas tal tulevad veel mingid (.) ekstra [`�ied]   | KYE: JUTUSTAV KAS |
M: [ei, ta] [`ongi �ietu.]   | KYJ: EI |
O1: [{ta `ongi t�pselt}]   | YA: MUU |
(0.5)
M: `pimedas kasvamise: kasvamise: `lill noh, | IL: �LER�HUTAMINE |
{---}   | YA: PRAAK |
O2: kuskil: kodus just h�marates nurkades=v�.    | KYE: JUTUSTAV KAS |
(1.8)
O1: a=`sooja poolest temal `ka on (.) `soe[mat vaja.]      | KYE: JUTUSTAV KAS |
M: [ei ole] vaja.    | KYJ: EI |
O1: ei ole vaja       | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE |
(1.0)
O2: aga (.) on `m�ni lill siuke ka mis on just nagu (0.5) tahab siukest `soojemat tuba,    | KYE: JUTUSTAV KAS |
(.) n�iteks=et meil [on]    | IL: T�PSUSTAMINE |
M: [no] `soe v�ib `olla aga mingi `kuiv=hh (.) kui `endal on juba keeruline `hingata seal ee Annelinna `korteris siis ega taimed ei `kasva seal.      | KYJ: INFO ANDMINE |
 (.) tuleb �hku `niisutada.   | KYJ: INFO ANDMINE |
O1: mhmh   | VR: NEUTRAALNE J�TKAJA |
M: kasv�i `pritsiga korra l�bi (.) k�ndida oma elamisest.   | KYJ: INFO ANDMINE |
 (.) {-} (1.0) `vanasti pandi siuksi (.) `n�usid veega kuskile radi`aatori k�lge `rippuma=et   | KYJ: INFO ANDMINE |
O1: mhmh   | VR: NEUTRAALNE J�TKAJA |
M: lihtsalt seda �hu`niiskust s�ilitada.   | KYJ: INFO ANDMINE |
 (0.5) k�ik `s�najalat��pi tahavad ikka lehed `m�rjaks vahepeal (.) n�dalas `kaks korda `k�ll saada.   | KYJ: INFO ANDMINE |
O1: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.8) * mhmh *   | VR: NEUTRAALNE VASTUV�TUTEADE |
selge?   | VR: NEUTRAALNE PIIRITLEJA |
 (0.5) aga=   | YA: MUU |
M: =`�hk peab olema `niiske nigu `kasvuhoones.   | KYJ: INFO ANDMINE |
 et seal v�ib ka nii `palav olla �le `kolmek�mne `kraadi aga=kui `niiskus on on `tasemel [siis] nad ei `sure �ra.   | KYJ: INFO ANDMINE |
O1: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
(1.5)
O1: a=kas=[ta]   | KYE: MUU |
M: [mis] `korter teil on `Annelinna korter=v�.   | KYE: JUTUSTAV KAS |    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
O1: {[me elame �his]`elamus ikka.}     | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
O2: [{-} �his`elamus.]   | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
M: nojah, seal on ka ikkagi: (.) kesk`k�te ju.   | KYE: VASTUST PAKKUV |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
O1: jah    | KYJ: JAH |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |

(.)
M: siis peate: peate lilli lihtsalt `m�rjaks kastma.      | KYJ: INFO ANDMINE |
 (0.5) `lehti `ka.    | IL: T�PSUSTAMINE |
O2: et meil just `oligi �ks prob`leem=et meile `kingiti see `toas pisikese `paprika v�i `pipar mis ta on [siuke.]     | YA: INFO ANDMINE |
M: [noh kuivas] �ra jah.   | KYE: VASTUST PAKKUV |
O2: [jah]   | KYJ: JAH |
O1: [jah]   | KYJ: JAH |
M: `lehed viskas `maha=ja   | KYE: VASTUST PAKKUV |
O1: jah   | KYJ: JAH |
{-}: {---}   | YA: PRAAK |
O1: ta ongi: ilmselt selle `kuivuse p�rast nagu.   | IL: P�HJENDAMINE |
(2.0) et me sis   | IL: MUU |
(0.5)
M: kasvuhoones meil kasvab nimodi=et on `�itseb ja siis tulevad j�lle `uued viljad ja teinekord {-} vanad `kuivand vil- viljad peal ja siis tulevad j�lle `uued viljad=ja (.) siin on ikka `niiskuse (.) `tasakaal on `v�rdne.   | YA: INFO ANDMINE |
O1: aga=et siis ta peaks noh toas `p�sima kui teda `niisutada siis ta     | KYE: SULETUD KAS |
M: nojah   | KYJ: JAH |
O2: ja siis ta on ikka nagu `p�sitaimena [{-}]   | KYE: SULETUD KAS |    
M: [p�si`taim,]   | KYJ: JAH |
 ta kasvab `suuremaks ja peab `k�rpima ja `vilju peab noppima vahepeal.     | IL: T�PSUSTAMINE | 
O1: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 a=siis kui nad `�ra nimodi juba n�rbuvad siis tuleb need viljad �ra l�igata=[v�.]      | KYE: SULETUD KAS |
M: [{jah}]   | KYJ: JAH |
O2: aga (.) aga kas ta tahab mingit noh selles=m�ttes=et=noh (.) kui `ostan poest noh siis istutan `�mber, (.) aga kui palju ta:   | KYE: AVATUD |
M: oleneb (.) oleneb `aastaajast j�lle.   | KYJ: INFO ANDMINE |
O1: mhmh   | VR: NEUTRAALNE J�TKAJA |
(1.0)
M: `talvel ostetakse siis ei ole `m�tet {---}   | KYJ: INFO ANDMINE |
O2: ahah      | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 (.) aga kas ta tahab nagu (.) umbes `sama suurt potti siis nagu ta (.) `ostes on v�i ta tahab [mingit]   | KYE: ALTERNATIIV |    
M: [ta=tahab] kasvu`mulda mitte seda seda `turvast kus ta `sees on.   | KYJ: MUU |
 (.) see kui ta `tuleb on mingis suvalises `turbas.   | IL: T�PSUSTAMINE |
O2: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) ma m�tlesin just seda `poti suurust, noh seda `kasvu[`ruumi.]   | KYE: AVATUD |
M: [pott v�ib] `sama olla.   | KYJ: INFO ANDMINE |
O2: jah, sama.   | VR: NEUTRAALNE VASTUV�TUTEADE |
no=selge   | VR: NEUTRAALNE PIIRITLEJA |
(0.5)
O1: a=kui=ta `suuremaks l�heb siis ta tuleb `aegajalt suuremasse potti siis `�mber istutada=ju.     | KYE: VASTUST PAKKUV |
M: aegajalt tuleb `�mber=istutada jah.   | KYJ: JAH |
(1.2) 
O1: < mhmh >   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (1.5) selge?   | VR: NEUTRAALNE PIIRITLEJA |
 (1.5) ait�h.   | RIE: T�NAN |


   

 
   

 

      
