((in_di_te_733_7 taastusravi osakonna registratuur))
((�hestas Andriela R��bis 2.03.2010))

((kutsung))
   | RIE: KUTSUNG |
(1.3) ((m�ra taustal))
V:  `taastusravi=   | RIJ: KUTSUNGI VASTUV�TMINE |
     | RY: TUTVUSTUS |
tere
   | RIE: TERVITUS |
(.)
H: tere,    | RIJ: VASTUTERVITUS |
.hhhhh mul=on `saatekiri `v�imlemisele.    | DIE: SOOV |
(1.2) kas:::=e=jah [{---}]   | YA: PRAAK |
V: [{---}] se=leht.    | YA: PRAAK | 
H: `doktor `Kutsar.
   | YA: MUU |
(1.7)
V: {`tema on tohter} v�.=   | KYE: SULETUD KAS |    | PPE: �MBERS�NASTAMINE | 
H: =jah jah    | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
V: .hh `ma: `panen=n�d=neid `taastusraviarsti `vastuv�ttule.   | DIJ: INFO ANDMINE |
 kas=te `varem `meie majas `olete k�ind vastu{-}   | KYE: SULETUD KAS |
    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: .hhhh `ei `ole
   | KYJ: EI |
     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.8) 
V: eeee `kolma`p�eval=h, kolme`teistkendal=hh,   | DIE: PAKKUMINE |
    | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: .hh se=on `see n�dal.   | KYE: VASTUST PAKKUV |
   | PPE: �MBERS�NASTAMINE | 
V: jah   | KYJ: JAH |   | PPJ: L�BIVIIMINE | 
H: .hh `see=n�dal `ei=`sobiks `jah    | DIJ: MITTEN�USTUMINE |
     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.) et=kui=saaks `j�rgmisesse panna?   | DIE: SOOV |
V: `siss on:=`j�rgmine kolmap�ev `kahek�mnes.
   | DIE: PAKKUMINE |
   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.5)
H: jaa?
    | DIJ: N�USTUMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V: kas kell=`�ksteist kell=`kaks. 
   | KYE: ALTERNATIIV |
   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.6) 
V: `pool `kolm
   | KYE: VASTUST PAKKUV |
   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(3.4)
H: < `poo:l `k:olm oleks p:arem:. >   | KYJ: JAH |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: `neliteist `kolgend.
    | KYE: VASTUST PAKKUV |
    | PPE: �MBERS�NASTAMINE |
(.)
H: jah    | KYJ: JAH |   | PPJ: L�BIVIIMINE | 
V: * mhm *    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.)  .hhhh e=`kuidas lapse nimi.   | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: .hhhh e n�d=on=mul `kaksikud.   | KYJ: MUU |
     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 ni=et tegelt [oleks vaja `kaks last]     | DIE: SOOV |
V: [aa   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 siis=ma=pean] kaks aega panema [k�rvuti]   | SEE: V�IDE |
H: [j�rjest] mhmh?   | SEJ: N�USTUMINE |
V: sest `poolene=on `aeg   | IL: P�HJENDAMINE |
H: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: [.hhh] {-} kas=sis=panen `kaks=ja `pool `kolm v�. 
   | KYE: SULETUD KAS |
     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8) 
V: mul `siin `niisukesed=kaks aega `on.    | IL: P�HJENDAMINE |
     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
.hhh
   | YA: MUU |
(0.7)
H: .hhhh aga kas `omikupoolikul > vat=se=`kolmap�eva `hommik ei=`sobi < kas `muu kolma- `muu hommikupoolik oleks=`ka:.   | KYJ: MUU |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |   | KYE: JUTUSTAV KAS | 
(0.5)
V: * `oodake=ma=`vaatan=sin=pr�egu? * .hhhhhh=   | KYJ: EDASIL�KKAMINE |
H: =se=on=neil=muidu [siuke (.) `magamise aeg.]   | IL: P�HJENDAMINE | 
V: [a=ma vaat- no `kolmap�ev=jah] just tuleks kell `�ksteist=ja �ksteist=`kolgend on nagu `vabad.   | KYJ: INFO ANDMINE |  
H: ja:h?   | VR: NEUTRAALNE VASTUV�TUTEADE |
  `kolma[p�ev=�sti ei] `saa?    | IL: �LER�HUTAMINE | 
V: [aga siis te=i saa?]    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.4) .hhhh * oota=vaatame `teist tohtrit ka=aga mis=`tema aegasid=sin=`on.    | KYJ: EDASIL�KKAMINE |
.hhhhhhhhhh mm kahe`teistken-=neljateistkendal=on=tal `kinn:i * (1.4) jah siss olek:s:=ot:=ot:=ee .hhhhhh (.) mmmmmmmmm �heksa`teist veebruar.
    | DIE: PAKKUMINE |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.9)
H: ja=[s:e=on]   | YA: MUU |  
V: [ja=kell]=�ks`teist=ja=�ksteist=`kolgend.=   | DIE: PAKKUMINE |  | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: =jah=se=oleks `parem.   | DIJ: N�USTUMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: `need `sobiksid [jah]   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
H: [jaa]
     | KYJ: JAH |
  | PPJ: L�BIVIIMINE |
(1.4)
V: .hhhh ja=`kudas lapse=`nimed on.
   | KYE: AVATUD |
     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
H: Kasesalu (0.3) .hh `Aldis=ja `Krister.
   | KYJ: INFO ANDMINE |
     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4)
V: e `Kasesalu.   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
H: jah
     | KYJ: JAH |  | PPJ: L�BIVIIMINE |
(0.9)
V: * mhmh (2.8) Kase (1.1) salu (3.3) Aldis   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (2.4) ja=kui `vanad nad=on, *   | KYE: AVATUD |      | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: .hh `viis=ja=`pool=h.
 | KYJ: INFO ANDMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
V: `kuud.  | KYE: VASTUST PAKKUV |      | PPE: �MBERS�NASTAMINE | 
(0.4) 
H: .hhh   | YA: MUU |
V: v�=`aastat.= | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE | 
H: =aastat=hehe[hehehe]   | KYJ: JAH |   | PPJ: L�BIVIIMINE | 
V: [hehehe] `viis=ja `pool `aastat jah?= | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
H: =jah | KYJ: JAH |   | PPJ: L�BIVIIMINE | 
V: .hhhh siss ku=nad=tulevad n- nii=`vanad on siss on=neil viisiiti `tasu kakskend=viis `krooni.   | YA: INFO ANDMINE |
H: jaa | VR: NEUTRAALNE VASTUV�TUTEADE |
selge?   | VR: NEUTRAALNE PIIRITLEJA |
V: jah=ah    | VR: MUU |
.hh siis v�tke `kaasa * sis seda *   | DIE: ETTEPANEK |
 .hhh e=ja: `esmaselt `kordselt tulevad sis jah?   | KYE: VASTUST PAKKUV |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: .hh `jah `sinna `k�ll [* mhmh *]   | KYJ: JAH |
    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 

V: [ja:=ja] `telefoninumber=oleks   | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
H: viis=`kaks,
   | KYJ: INFO ANDMINE |      | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.7)
V: jaa?   | VR: NEUTRAALNE J�TKAJA |
H: `kaks `kuus `neli  | KYJ: INFO ANDMINE |      | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: kaks kuus neli?    | VR: NEUTRAALNE J�TKAJA |
H: kuus=`kolm.  | KYJ: INFO ANDMINE |      | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V: kuus kolm.    | VR: NEUTRAALNE VASTUV�TUTEADE |
(.) .hhhh > viiskend=`kaks, kakskend=`kuus, < `neli `kuus `kolm.   | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
H: jah    | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
V: .hhhhh mhmh.   | VR: NEUTRAALNE VASTUV�TUTEADE |
     | VR: PARANDUSE HINDAMINE |
 .hh=siss on `nii.    | VR: NEUTRAALNE PIIRITLEJA |
(1.2) m mt �heksa`teist veebruar?   | KYJ: INFO ANDMINE | 
H: jaa?   | VR: NEUTRAALNE J�TKAJA |
V: .hhhhhh ja:::: `�ksteist kol- eeeee �ksteist=null=`null, ja �ksteist=`kolgend.
    | KYJ: INFO ANDMINE |
(.)
H: .hh ja=`kuhu=me tuleme?   | KYE: AVATUD | 
V: ja tulete sis Viirpalu nelikend=`kuus    | KYJ: INFO ANDMINE |
`teate,
    | KYE: SULETUD KAS |
     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
H: jah   | KYJ: JAH |
     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
 se [on:]      | IL: T�PSUSTAMINE |  | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE | 
V: [seal]=kus `elektriv�rgud se=`t�nav=on.    | KYJ: INFO ANDMINE | 
H: mhmh?   | VR: NEUTRAALNE J�TKAJA |
V: aga `me=j��me nagu sinna rohkem (0.3) `Pihlaka t�nava juurde=    | KYJ: INFO ANDMINE | 
H: =mh[mh?]    | VR: NEUTRAALNE J�TKAJA | 
V: [`�le] t�nava meil=on=n�d `Teksora, .hhhhhh ja siin=on` kolm ��heksakorruselist=> aga meie oleme nagu `Viirpalu t�nava j�rgi < meie `k�rval siin on veel see .hhhhhh noh `autopood=ja=sis=sin [`va]hepeal=vel `juuksur=ja     | KYJ: INFO ANDMINE |
H: [jah]
   | VR: NEUTRAALNE J�TKAJA |
(.)
H: e see=on `ikka `hoovi=poolt [`sisse nagu `vanasti oli]    | KYE: VASTUST PAKKUV | 
V: [ja=tagant (.) `ikka `jah]     | KYJ: JAH | 
H: mhmh=    | VR: NEUTRAALNE J�TKAJA | 
V: =`maja `tagant `sisse,   | KYJ: JAH |
 .h ja: `saatekirjad `kaasa=kui=on `lapsed=ja .hhh `kliendi `teenendaja juurde k�igepealt.   | YA: INFO ANDMINE | 
H: mhmh    | VR: NEUTRAALNE VASTUV�TUTEADE |
ja=`mis arsti juurde me (.) siis `ole[me.]    | KYE: AVATUD | 
V: [do]ktor Miina Velma.
     | KYJ: INFO ANDMINE |
(0.6)
H: Miina Velma.    | KYE: VASTUST PAKKUV |    | PPE: �LEK�SIMINE | 
V: jah    | KYJ: JAH |    | PPJ: L�BIVIIMINE | 
H: mhmh,    | VR: NEUTRAALNE J�TKAJA |     | VR: PARANDUSE HINDAMINE | 
V: `Velma.     | IL: �LER�HUTAMINE |
`kirjutage=�lesse=* sis=[se] *    | DIE: ETTEPANEK | 
H: [mh]mh    | DIJ: N�USTUMINE | 
V: * .jah. *    | VR: MUU |
 (.) kell=`�ks`teist=ja �ksteist=kolgend kella=�he`teistk�mneks siis jah,
    | IL: �LER�HUTAMINE | 

H: jah,    | VR: NEUTRAALNE VASTUV�TUTEADE |
ait�h=teile=    | RIE: T�NAN | 
V: =pa:lun=    | RIJ: PALUN | 
H: =jah    | RIJ: MUU |
 n�gemist=    | RIE: H�VASTIJ�TT | 
V: =jah    | RIJ: MUU |
