((475b12 telefonik�ne hambakliinikusse))
((�hestas Andriela R��bis 17.10.2005, kontrollitud 23.08.2006))

((kutsung))   | RIE: KUTSUNG |
V1: Kuku `kliinik   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
 tere   | RIE: TERVITUS |
H: .hh tere    | RIJ: VASTUTERVITUS |
ma `elistan n�d `tagasi Jaana Kasesalu.    | RY: TUTVUSTUS |
.hh `paneks �he (.) `raviaja ka `kinni=et (.) niigu`nii ma pean `tulema.   | DIE: SOOV |
(.)
V1: �ks=mo`ment?=palun       | DIJ: INFO PUUDUMINE |     | RY: �LEANDMINE |
(...) ((V1 r��gib midagi kolleegiga)) | RIE: KUTSUNG |  
V2: ee tere=`p�evast=��         | RIJ: KUTSUNGI VASTUV�TMINE |  | RIE: TERVITUS |
H: jah=      | RIJ: MUU |
V2: =muidu kelle juurde `aeg oli pandud=�=   | KYE: AVATUD |      | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: =.hh doktor `Moosisaar.   | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(2.5)
V2: aa,    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
see `kolma`p�eva me [panime jah?]   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: [just,] just.    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
aga paneme:: `edasi ka `ravi juba `kinni.   | DIE: SOOV |
(2.8)
V2: nii?         | VR: NEUTRAALNE VASTUV�TUTEADE |
et (.) et=sis=ma: `raviaega ma pean `j�rgmist aega vaa[tama.]   | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
H: [jaa,] just=just.    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(1.0) noh, `lihtsalt et ta oleks juba `olemas et nigunii ma pean `tulema.   | IL: P�HJENDAMINE |
(1.0)
V2: `tund aega raviaega sis [m�tlete]   | KYE: VASTUST PAKKUV |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: [`kindlasti] jah   | KYJ: JAH |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.8)
V2: kakskend=`seitse, oktoober.    | DIE: PAKKUMINE |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: mis `p�ev se `on   | KYE: AVATUD |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V2: kolma`p�ev.    | KYJ: INFO ANDMINE |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
kell=`kolm, kell=`neli on pakkuda.    | DIE: PAKKUMINE |     | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.2)
H: .hh ee=`n:eli oleks parem.    | DIJ: MUU |     | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.) mhmh?   | VR: MUU |
(0.5)
V2: mhmh?    | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.5) nii?    | VR: NEUTRAALNE VASTUV�TUTEADE |
(4.2) Ka-se- (.) `salu?   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
H: just   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
V2: ja `eesnimi=oli   | KYE: AVATUD |     | PPE: MITTEM�ISTMINE |
H: Jaana.   | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |
(.)
V2: Jaana.   | VR: NEUTRAALNE VASTUV�TUTEADE |     | VR: PARANDUSE HINDAMINE |
 (.) ja=mis=se `vana nimi oli   | KYE: AVATUD |     | PPE: MITTEM�ISTMINE |
H: Viljap�ld.   | KYJ: INFO ANDMINE |     | PPJ: L�BIVIIMINE |
(3.0)
V2: nii,    | VR: NEUTRAALNE PIIRITLEJA |     | VR: PARANDUSE HINDAMINE |
on `olemas n��d.       | DIJ: MUU |
kakskend=seitse ok`toober ja kuusteist nul=`null.   | DIJ: INFO ANDMINE |
H: ait�ma.   | RIE: T�NAN |
V2: palun   | RIJ: PALUN |
H: n�gemist   | RIE: H�VASTIJ�TT |
V2: n�gemist   | RIJ: VASTUH�VASTIJ�TT |


   

 
   

 
