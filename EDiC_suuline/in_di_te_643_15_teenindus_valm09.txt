((in_di_te_643_15_teenindus, koolitusfirma))
((�hestas Andriela R��bis 6.01.2010))

((numbri valimine, kutsung)) | RIE: KUTSUNG |
V: � {--}{Pol�log.} �  | RIJ: KUTSUNGI VASTUV�TMINE |    | RY: TUTVUSTUS |
(0.3)
H: .hhhh tere. | RIE: TERVITUS | 
(0.3) .hhhh `ega te `lastele `v�lismaal keele`laagreid ei `korralda.   | KYE: SULETUD KAS |
V: � korraldame `k�ll. �   | KYJ: MITTEN�USTUV JAH |
(0.5)
H: `nii?=h | VR: NEUTRAALNE PIIRITLEJA |
ja=`mis=teil, `on teil `j�rgmiseks aastaks juba midagi `olemas.=  | KYE: JUTUSTAV KAS |
V: � =ee `jaa, isegi `tellijad on.=h | KYJ: JAH |
hehe .hhhhhhh on on on `vanemaid kes on `k�sinud selle `kohta. | KYJ: INFO ANDMINE | 
(0.8) .hhh aga `millal teie `laps tahab `s�ita ja kui `vana ta `on. �  | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
H: ee `viis`teist.  | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.5)
V: � `viisteist aastat vana | VR: NEUTRAALNE VASTUV�TUTEADE |
ja `kuhu ta `soovib �  | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
H: `Inglismaa.=hh  | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: � `Inglismaale jah? | VR: NEUTRAALNE VASTUV�TUTEADE | 
(.) .hhh jaa `Inglis`maal on ka kohe `mitu `partnerit kellega meie `oleme `t��tanud juba `aastaid. | KYJ: INFO ANDMINE |
 (1.4) jah. | VR: MUU |
 (1.0) a mis `kuu teil oleks, �  | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4)
H: eee `juuli.=h   | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
V: � .hhh `juuli`kuu, | VR: NEUTRAALNE VASTUV�TUTEADE |
ja kui `kauaks? �  | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4)
H: no �tleme `kaks `n�dalat.   | KYJ: INFO ANDMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
V: � .hh `kaks n�dalat jah? | VR: NEUTRAALNE VASTUV�TUTEADE | 
(0.4) .hhhhhh et=eeee �� m=���� ja `kus ta soovib `olla, `Londonis v�i `rannikul. �  | KYE: ALTERNATIIV |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4)
H: eeee noh see on `p�him�tteliselt �ks`k�ik,   | KYJ: ALTERNATIIV: M�LEMAD |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(.)
V: * mhmh, mhmh *   | VR: NEUTRAALNE J�TKAJA |
H: et `oleneb mis teil seal `pakkuda $ `oleks. $   | IL: T�PSUSTAMINE |   | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |   | KYE: AVATUD |   | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.4)
V: � .hhhh ed=eeee meie `vaatame `kohe (0.5) need `asjat=ja: ja saame teile v�ibolla `meiliga saata v�i `elistada.   | KYJ: KEELDUMINE |    | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |   | KYE: ALTERNATIIV | 
(0.6) kuidas te `soovite. �   | KYE: AVATUD |
H: ����� (0.3) `meili peale, | KYJ: ALTERNATIIV: �KS |
a=ega teil `kodulehel ei `ole selle kohta mingit [`infot=v�i,]   | KYE: JUTUSTAV KAS |
V: � [.hhh e] no `hinnad me ei `pane, sest `januari`kuust nad eee `j�rgmineva `aasta=ee `suve `hinnad veel ei `ole. �  | KYJ: INFO ANDMINE |
(.)
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: � .hhhh et ned `tulevad `januari `kuus aga:::: `meil on v�ga `hea kui me `teame juba, .hhhh `kes on `kandidaadid, `kes sooviksid `minna=ja=et=e sis .hhh siis me v�ime juba `k�sida `umbes, et vat `meil on `nii palju `rahvast {et=nii} (0.5) .hhh ega `sellest muidugi `nende `hinnad `eriti ei `muutu=aga aga `ikkagi meie sellise prog`noosi `anname nendele. �   | KYJ: INFO ANDMINE |
H: .hhh a:ga `oskate `suurus`j�rku `�elda, `palju �ks `kahen�dalane `reis v�iks `maksta.  | KYE: AVATUD |
V: � .hhhhhhhhh no:::h (0.6) `pr�egu mul on `raske �elda, `hinnad on `erinevad, kui palju `tundi laps `v�tab, kas ta v�tab po-prog`ram kus on `kaks`k�mmend tundi v�i, .hhh v�i `v�tab `see kus on:=ee see `holidei ((holiday)) prog`ram kus oli `kuusteist tundi, .hhhh ja e mis prog`rammid `nemad pakuvad `j�rgmiseks `aastaks, sest `oli `�ks aasta kus oli `v�ga .hhhh `mitmekesised (0.3) prog`ramid aga `on ka: `on ka sellised kus on `viis`teist `tundi, .hhh `pljuss pljus (0.5) m�ned ekskursi`oonid. | KYJ: INFO ANDMINE |
 (1.3) et=eee selli- sellise, aga (0.3) `umbes `umb`kaudselt �tleme `kaksk�mend tuhat `krooni. | KYJ: INFO ANDMINE |
 (0.3) .hhh selle `peale tuleb orien`teeruda. �  | KYJ: INFO ANDMINE |
(0.6)
H: �� kas `selle sees on `s�it `ka [vel.]  | KYE: SULETUD KAS |
V: � [ei,] `s�it `tavaliselt `mitte. �  | KYJ: EI |
(0.3)
H: ahah. | VR: NEUTRAALNE INFO OSUTAMINE UUEKS | 
(0.5) `selge? | VR: NEUTRAALNE PIIRITLEJA |
.hhhh aga::=���=h ma `annaks siis oma [`meili`aadressi.]  | DIE: PAKKUMINE |
V: � [jaa, ma] `paluksin.=h �  | DIJ: N�USTUMINE |   | DIE: SOOV |
(.)
H: Kata`riina,=hh  | DIJ: INFO ANDMINE |
(0.3)
V: � .hhhhh Ka=h �  | KYE: AVATUD |    | PPE: MITTEM�ISTMINE |
(.)
H: `Ka- `ta-`rii-`na.  | KYJ: INFO ANDMINE |   | PPJ: L�BIVIIMINE |
(0.3)
V: � .hh `ta: `ri:, | VR: NEUTRAALNE VASTUV�TUTEADE |  | VR: PARANDUSE HINDAMINE |
(.) `kaks iid. �   | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
(.)
H: jah  | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.3)
V: � .hh Kata`riina? | VR: NEUTRAALNE J�TKAJA |   | VR: PARANDUSE HINDAMINE |
(.) [�d?] �  | VR: NEUTRAALNE J�TKAJA |
H: [punkt] | DIJ: INFO ANDMINE |
ee `punkt, | PA: MUU |
(0.3) `Saabas.  | DIJ: INFO ANDMINE |
(.)
V: � .hhh ����� `Saa:bas kahe `aaga. �  | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
H: jaa?  | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.4)
V: � .hh `Saabas. �  | VR: NEUTRAALNE J�TKAJA |     | VR: PARANDUSE HINDAMINE |
(.)
H: �t  | DIJ: INFO ANDMINE |
V: � .hhhh `�:d, �  | VR: NEUTRAALNE J�TKAJA |
H: `ut, (0.3) `punkt `e-`ee.  | DIJ: INFO ANDMINE |
V: � punkt ee, | VR: NEUTRAALNE VASTUV�TUTEADE |
te olete `�likooli [`inimene] jah? �  | KYE: VASTUST PAKKUV |
H: [jah.]  | KYJ: JAH |
(0.3)
V: � .hhhh eee `kena. | VR: NEUTRAALNE PIIRITLEJA |
.hh ja: `laps on eeee `viisteist `aastane? �=  | KYE: VASTUST PAKKUV |   | PPE: �LEK�SIMINE | 
H: =jah=  | KYJ: JAH |   | PPJ: L�BIVIIMINE |
V: =� `poiss v�i `t�druk. �  | KYE: ALTERNATIIV |    
H: t�druk.  | KYJ: ALTERNATIIV: �KS |    
(.)
V: � .hh `t�druk? | VR: NEUTRAALNE VASTUV�TUTEADE |
jaa? | VR: NEUTRAALNE VASTUV�TUTEADE |
mhmh mhmh | VR: NEUTRAALNE VASTUV�TUTEADE |
.hhh `mitu `aastat ta on `�ppinud `keel. �  | KYE: AVATUD |     
(0.8)
H: ee ta on `inglise keele eri`klassis=ja `selleks aas- `selleks ajaks ta on sis `seitse aastat `�ppinud.=h  | KYJ: INFO ANDMINE |   
V: � .hh * mhmh? * | VR: NEUTRAALNE VASTUV�TUTEADE | 
(0.4) mis `koolis ta `�pib. �=  | KYE: AVATUD |
H: =`M�nnilinnas.  | KYJ: INFO ANDMINE |
V: � .hhh mhmh? | VR: NEUTRAALNE VASTUV�TUTEADE |
jaa?   | VR: NEUTRAALNE VASTUV�TUTEADE | 
(0.4) .hhhhh < `M�nni `linna? `kool? >   | VR: NEUTRAALNE VASTUV�TUTEADE | 
(0.5) .hhh (.) `nii,   | VR: NEUTRAALNE PIIRITLEJA |
ja siis `soovib `kaks `n�dalat?    | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE | 
(1.0) .hhh ja `juulis. �   | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(0.6)
H: `jah.   | KYJ: JAH |    | PPJ: L�BIVIIMINE |
V: � .hhhhh < `juulis `kaks tuhat=h `seitse. > | VR: NEUTRAALNE VASTUV�TUTEADE |    | VR: PARANDUSE HINDAMINE | 
(0.3) .hhhhh eeee `nii, | VR: NEUTRAALNE PIIRITLEJA | 
(.) me `vaatame sis=eeeee `m�ned koolid on meile juba `saatnud omat .hhhh ee `plaanid ja `k�ik, me vaatame `�:le ja siis `v�tame teiega kon`takti, .hhh aga kuna see on nagu `juuli`kuus, ma `arvan et `k�ige `parem `oleks kui .hhhh no �tleme me `saadaks teile `t�pset=h � `t�pne `info `januari .hhhhhh kuskil `l�pu `poole, siis meil `k�ik `olemas juba. �  | DIE: PAKKUMINE |
H: jah.  | DIJ: N�USTUMINE |
V: � `t�psed `andmed jah. �   | IL: T�PSUSTAMINE |
(0.4)
H: .hhhhhh jah=  | DIJ: N�USTUMINE |
V: =� jah �   | VR: MUU |
(0.2) 
H: [ait�h]    | RIE: T�NAN |
V: � [palun] �   | RIJ: PALUN |
H: n�[gemist?]   | RIE: H�VASTIJ�TT |
V: � [mitu `ini]mest on `k�inud=ja: `�sna, $ v�ga=`v�ga `rahule j��nud `isegi. $ �=   | YA: INFO ANDMINE |
H: =jah, | VR: NEUTRAALNE VASTUV�TUTEADE |
ai`t�h?  | RIE: T�NAN |
V: � k�ike `head,   | RIE: SOOVIMINE |
n�ge`mist. �  | RIJ: VASTUH�VASTIJ�TT |
