((785 s�prade argivestlus))
((k�ik litereeritud))
((�hestas Andriela R��bis 16.10.2013))

K: tule `r��gime n�d=se s�nnip�evajuttu=s. | DIE: ETTEPANEK |
(0.7)
S: noo::h? | DIJ: N�USTUMINE |
(2.0) ota=mul {---} `midagi. | YA: INFO ANDMINE |
(3.6)
K: aa | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
se=on nagu `nimekiri mis vaja �� (.) `tuua on=v�, | KYE: VASTUST PAKKUV |
S: jah. | KYJ: JAH |
(1.5)
S: vaatasin=et=���� (0.3) [et=se] | YA: INFO ANDMINE |
K: @ [aga] `plastik`topse=ju `on=ju. @ | SEE: V�IDE |
(0.8)
S: & �ks kaks kolm neli: & kuus `t�kki. | SEJ: PIIRATUD N�USTUMINE | | SEE: V�IDE |
(.)
K: @ seal on `veel=ju. | SEJ: MITTEN�USTUMINE | | SEE: V�IDE |
nad ei=ole `plastikust aga nad on to- `topsid. @ | IL: T�PSUSTAMINE |
(0.7) 
S: aa, | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
okei? | SEJ: N�USTUMINE |
sellest piisab, (.) vast. | SEJ: PIIRATUD N�USTUMINE |
[{-}] | YA: PRAAK |
K: [t�mban] `maha, | KYE: VASTUST PAKKUV |
(0.6)
S: < t�mba=`maha > {-} | KYJ: JAH |
(0.8) {-} | YA: PRAAK |
K: {-} | YA: PRAAK |
(1.5) 
S: ((ohkab)) * {---} * | YA: PRAAK |
< et >=���������� (0.7) aa hapu`koort ei=ole. | SEE: V�IDE |
(1.4) ((S valab kohvi tassi)) 
S: noo `m�tsin | SEJ: MUU |
vaata=see:: `hapukurk=on `sinu=toodud=v�. | KYE: VASTUST PAKKUV |
K: jaa, | KYJ: JAH |
(0.8)
S: kas �hest `purgist `piisab=v�, | KYE: SULETUD KAS |
(0.8)
K: `salati sisse ma arvan `k�ll. | KYJ: JAH |
(1.0)
S: kas=sa=tahad eraldi `lauale panna? | KYE: SULETUD KAS |
(1.6)
K: eeeeeee `mida eraldi.  | KYE: AVATUD | | PPE: MITTEM�ISTMINE |
(.) 
K: `laua peale [panna]=v�. | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE |
S: [jah] | KYJ: JAH | | PPJ: L�BIVIIMINE |
(1.9)
S: kui �tled=et `salati sisse k�ll. | IL: P�HJENDAMINE |
(1.2)
S: {---} | YA: PRAAK |
(1.9)
K: {-} | YA: PRAAK |
(0.8)
S: ah | KYE: AVATUD | | PPE: MITTEM�ISTMINE |
(0.3)
K: `ei=noh jah `ei. | KYJ: EI | | PPJ: L�BIVIIMINE |
 (0.9) sobib, | KYE: VASTUST PAKKUV |
(1.3)
S: kurki `v�rsket kurki akka `ostma. | SEE: ARVAMUS | 
(1.2)
S: < et siis >=����� `see, | YA: MUU |
(2.3)
S: h�����-����� `palju sinna: {`viina} peab ostma. | KYE: AVATUD |
(1.2)
S: mt=.hhhh {---} `liitrist peaks (.) `piisama. | SEE: ARVAMUS |
(1.0)
S: {-} | YA: PRAAK |
(1.7)
S: aga mis me `ise joome. | KYE: AVATUD |
K: aga `k�ik toovad ju `iseenda `alkoholi. | KYJ: MUU | | SEE: V�IDE |
(.)
S: jah | SEJ: N�USTUMINE |
(1.9)
S: a nojah mingi `tervitusnaps vaja vaadata. | SEE: ARVAMUS |
(1.3)
K: aa millist, | KYE: AVATUD |
(1.0)
S: {---} vanasti `peale. | KYJ: MUU |
(0.7)
K: njoh | VR: NEUTRAALNE J�TKAJA |
(0.5)
S: mis sa `arvad=et (0.7) `k�ima t�mmata juba. | KYE: MUU |
(2.3)
K: hehe .hhh sul on viin `nimekirjas `ka. | KYE: VASTUST PAKKUV |
(0.5)
S: [jah] | KYJ: JAH |
K: [ma] arvan `liiter on p�ris `hea. | KYJ: INFO ANDMINE | | SEJ: N�USTUMINE |
(0.5)
S: aga mis me `ise joome, | KYE: AVATUD |
(1.0)
S: {-} | YA: PRAAK |
(0.3)
K: `viina ma enam ei `taha. | KYJ: INFO ANDMINE |
S: mhmm (0.3) `mis {--} | YA: PRAAK |
K: mingi `veini v�i nimodi. | KYJ: INFO ANDMINE |
(0.7)
S: tekiila? | KYE: VASTUST PAKKUV |
(1.1)
K: eeeeeeeee | KYJ: EDASIL�KKAMINE |
(0.7)
S: miks `mitte,=hhhh | KYE: AVATUD | | YA: RETOORILINE K�SIMUS |
nii `�ge=ju. | SEE: ARVAMUS |
(2.2)
S: siuke {-}, | YA: PRAAK |
(0.3)
K: sis=on paha `olla p�rast.  | KYJ: INFO ANDMINE | | SEJ: MITTEN�USTUMINE | | SEE: ARVAMUS |
(.)
S: nagunii on. | SEJ: N�USTUMINE |
(1.7)
S: {--} | YA: PRAAK | 
(0.3) ku=ma olen `�ksinda=sis `poolest peaks `piisama `k�ll=ju. | SEE: ARVAMUS |
(2.8)
K: hehe $ `eelmist pidu `m�letad=v�. $ | KYE: SULETUD KAS |
(0.3)
S: jaa | KYJ: JAH |
aga sis=me=olime `neljakesi. | IL: T�PSUSTAMINE |
(1.4)
K: ei mitte `eelmist, (0.4) seda `pidu=pidu kus n terve kursus `siin oli, | PA: ENESEPARANDUS |
S: `nii? | VR: NEUTRAALNE J�TKAJA |
mis=sis `oli, | KYE: AVATUD |
(0.5)
K: siis meil ei=`olnd sul sul oli ainud `�ks. | KYJ: INFO ANDMINE |
(0.6) 
K: ja `�ks oli vist `s�gavk�lmas=v�. | KYE: VASTUST PAKKUV |
(0.4) 
K: v�i sa t�id `juurde [poole=selle.] | KYE: VASTUST PAKKUV |
S: [{t�i}]={poo-} t�in `juurde. | KYJ: JAH |
poest. | IL: T�PSUSTAMINE |
(0.7)
K: no=`vot, sa t�id `juurde. | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.3) n��d sa pead kohe `varuga arvestama. | DIE: ETTEPANEK |
S: `kaks liitrit viina {-} | DIJ: N�USTUMINE | | SEE: ARVAMUS |
(0.9)
K: @ `kaks `liitrit. @ ((imestades, sosinal)) | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE |
S: noja | KYJ: JAH | | PPJ: L�BIVIIMINE |
`ses suhts=et ku midagi `�le j��b sis onju:::: j��b lihtsalt `�le noh. | IL: P�HJENDAMINE | | PPJ: L�BIVIIMINE | 
(0.7) `�le v�ib `alati j��da. | SEE: ARVAMUS |
(0.4)
K: noo | SEJ: MUU |
S: {-} | YA: PRAAK |
(0.8)
K: tegelt se=on �ige `jutt. | SEJ: N�USTUMINE |
.hhh a `tavaliselt on `nii=et kui sul on `rohkem=sis (1.1) `k�ik juuakse `�ra, hehe | SEE: V�IDE | 
S: no eks `n�eb. | SEJ: MUU |
K: $ `�le j��b v�ga `arva. $ | SEE: V�IDE |
(0.4)
S: jaa | | SEJ: N�USTUMINE |
(0.4) .hhhh ja=s kokakoolat peaks ostma n�iteks::: (3.0) `kaks pudelid `v�hemalt. | SEE: ARVAMUS |
(1.6)
K: jah | SEJ: N�USTUMINE |
(.)
S: mt=.hhhhhhh < siis >=����=mhh | YA: MUU |
K: kas sa oled nende t��bikestele `�elnud=et nad peavad `ise enda alkoholi tooma=v�. | KYE: SULETUD KAS |
S: jaa, | KYJ: JAH |
see minu=arust peaks olema nagu `iseenesestm�istetav. | SEE: ARVAMUS |
(0.5)
K: nojah | SEJ: N�USTUMINE |
(0.5)
S: aa `Margus ka `k�sis t�na ja=s=ma �tsin=tale=et jaa=et ika `ise peab tooma. | KYJ: INFO ANDMINE |
(1.5) 
S: hmmmmmmmm ((m�riseb)) (0.4) st `ma k�ll ei=j�ua seda k�ike `kinni maksta {-}. | IL: P�HJENDAMINE |
(0.4) 
K: [{mm}] | VR: MUU |
S: [{-}]| YA: PRAAK | 
(0.4) `salati sisse l�heb siis `vorsti, (0.6) `hapukurk on `olemas, (.) `muna peab ostma �he karbi igaks juhuks, | SEE: ARVAMUS |
(0.3)
K: jah | SEJ: N�USTUMINE |
(0.4)
S: aa siis=��� see | YA: MUU |
(2.2)
S: [{-}]| YA: PRAAK |
K: [`majoneesi] meil `on | SEE: V�IDE |
apukoor (.) apukoort, .hhh | KYE: AVATUD |
S: peame `ostma, `hapukoort peaks mingi `kaks t�kki * vist ostma. * | KYJ: INFO ANDMINE | | SEE: ARVAMUS |
K: jah, v�hemalt, | SEJ: N�USTUMINE |
S: kaks. | IL: �LER�HUTAMINE |
(1.5) 
S: mt=.hhhhh siis < `saia > `piima on niigunii vaja, | SEE: ARVAMUS |
(0.8)
K: jah | SEJ: N�USTUMINE |
(0.6)
S: `hommikul tahab ka midagi `s�ia, | IL: T�PSUSTAMINE |
(0.3)
K: kule `kr�binaid, | SEE: ARVAMUS |
(0.5)
S: aa, | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
* kr�binaid * | SEJ: N�USTUMINE |
(3.3) 
S: ma panin kohe `kaks piima, | YA: INFO ANDMINE |
(0.8)
K: hmm | VR: MUU |
S: teeks �kki mingi {--} (.) hoopis. | DIE: ETTEPANEK |
(1.9)
S: * ma=i=oska [{-}] * | YA: PRAAK |
K: [.hhh] teeks tervitus`napsuks seda (.) ilgelt ead seda `�unamahla viina ja natuke ka`neeli. | DIE: ETTEPANEK |
(0.7)
S: kas=se=oli ilgelt `hea=v�. | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE |
K: jaa? | KYJ: JAH | | PPJ: L�BIVIIMINE |
(0.7) 
K: miks ei `olnud. | KYE: AVATUD | | YA: RETOORILINE K�SIMUS |
(.)
S: no siss ka`neel, | DIJ: N�USTUMINE | | SEE: ARVAMUS |
(0.5) 
K: a meil `on kaneeli, | SEJ: MITTEN�USTUMINE |
(0.3)
S: aa,= | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
K: =a=tegelt ma=i=ole `kindel `ka kas ta v�ga `hea oli. | SEE: ARVAMUS | | PA: ENESEPARANDUS |
`minu=st {-} `oli. | SEE: ARVAMUS |
(0.3)
S: `ma nagu ei `m�leta h�sti. | SEJ: MUU |
(0.8) minu=arust oli seal mingi `h�da ja=s=me panime `piima juurde ja siis me panime `veel mingit `asja=[ja=sis] | SEJ: MUU | | SEE: ARVAMUS |
K: [ei] olnt `piima k�ll. ((nipsakalt)) | SEJ: MITTEN�USTUMINE |
(2.1)
S: me saame {-} maha v�tta {-} `homme. | YA: MUU |
(1.6) 
S: aa oota `mis=se oli, (.) `viin, �unamahl=ja: (.) see | KYE: AVATUD | | PPE: MITTEM�ISTMINE |
(.)
K: kaneel. | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE |
(0.4)
S: siss j�tkub `viina kauemaks `ka. | SEE: ARVAMUS |
(1.5) 
S: < siis > aa siss=se=on `pitsi sisse v�i=sis `topsi=[sis-] | KYE: ALTERNATIIV |
K: [pitsi] `sisse, | KYJ: ALTERNATIIV: �KS | | SEE: ARVAMUS |
siis ta on nagu v�ike `shott. | IL: P�HJENDAMINE |
S: jah | SEJ: N�USTUMINE |
(1.1) siis=�����m (2.8) ������� ot `salat v�i komp`lekt on sis p�himtselt `olemas onju, | KYE: VASTUST PAKKUV |
(0.9)
K: mhmh? | KYJ: JAH |
(0.3)
S: siis::::::: `nii,= | YA: JUTU PIIRIDE OSUTAMINE |
K: =aga mis=sa sinna `k�rvale pakud. | KYE: AVATUD | 
v�i=nagu (.) mitte nagu `k�rvale aga=noh `peale `salati, | PA: ENESEPARANDUS |
(0.5)
S: no `Epp toob seda `pitsat=e=ja siis neid `viineripirukaid, (0.7) Mare tahtis `ka mingit `salatit teha=ja < kui `Anne > teeb `ka=vel=mingi:: (0.6) [`koogi,] | KYJ: INFO ANDMINE | 
K: [koogi] (.) nojah | VR: MUU |
S: mh `m:ina=ei=taha nagu rohkem `midagi teha. | KYJ: INFO ANDMINE | 
(.) mul oleks isegi `lihtsalt salatist `piisand t�iesti. | KYJ: INFO ANDMINE | 
(1.8)
K: kas `Epp tuleb `bussiga=v�. | KYE: SULETUD KAS |
S: jah | KYJ: JAH |
(0.6)
K: heh $ kaks `pitsat, .hhh `ja `pirukad. $ | KYE: VASTUST PAKKUV |
S: .jah | KYJ: JAH |
(0.3)
K: vaau. ((imetledes)) | VR: HINNANGULINE J�TKAJA |
(1.4)
S: `hakkaja t�druk | IL: HINNANG |
ma pean `kind`lalt tellima: `Joosepi talle: `bussi vastu. | SEE: ARVAMUS |
(.)
K: jaa | SEJ: N�USTUMINE |
(0.5)
S: see on nagu:=o� `essso`esss. | IL: P�HJENDAMINE |
`aa ja=sis=ma pean `Svenile Maretile ka `helistama k�sima mis `kellast nad `tulevad. | SEE: ARVAMUS |
noh Epuga sama`moodi, | SEE: ARVAMUS |
K: nad `kolmekesi ei `v�i tulla=v�, | SEJ: MUU | | KYE: VASTUST PAKKUV |
S: �������� (.) ma=i=`tea vaata seal on=se mingi siuke `lugu=et=��� (1.1) et `Svenil on vaba `p�ev=ja=s=ta=saks isegi `varem tulla=v�. | KYJ: INFO ANDMINE |
(0.4) aga `Maret ei teadnud j�lle. | KYJ: INFO ANDMINE |
(0.8)
K: `varem kui `teised tulla=ve, | KYE: VASTUST PAKKUV | | PPE: �MBERS�NASTAMINE |
S: jah | KYJ: JAH | | PPJ: L�BIVIIMINE |
(0.4) aga siis peab `see olema=et `keegi on > `kindlasti kodus= | SEE: V�IDE |
kas <=eee Eve-= `Evelil on vaba p��v=v�. | KYE: SULETUD KAS |
K: ei `ole vist `enam. | KYJ: EI |
(0.4)
S: hmm | VR: MUU |
(1.0) `EVELI, ((h�ikab tuppa)) | KKE: ALGATUS |
E: noo? ((toast)) | KKJ: KINNITAMINE |
S: MIS `KELLAKS SA `KOJU J�UAD. ((h�ikab tuppa)) | KYE: AVATUD | 
(1.0)
E: eeeee (0.4) `kaheks ((toast)) | KYJ: INFO ANDMINE |
S: `kaheks, | VR: NEUTRAALNE VASTUV�TUTEADE |
siis on `p�rfekt. | VR: HINNANGULINE VASTUV�TUTEADE |
(1.3) [eee] | YA: MUU |
K: [`ma j�u]an isegi `varem mul=on=ain- ainult kahe`teistk�mneni, | YA: INFO ANDMINE |
S: aa. | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
no=siis on h�sti. | VR: HINNANGULINE VASTUV�TUTEADE |
.hhhh et noh=et=et `igaks=juks on keegi `kodus=et | IL: KOKKUV�TMINE |
`neile ma �tlen `k�ll �ra=et kuidas `saab nagu `siia {-} .hhhhhh kui ma ei `saa ise neile `vastu minna. | YA: INFO ANDMINE | 
(.) v�i [{-}] | YA: PRAAK |
K: [aga] `Sven ja Maret `teavad=ju. | SEE: V�IDE |
(0.4) nad on `k�ind. | SEE: V�IDE |
S: mhmh? | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.4) no siis on {`tore} ju. | VR: HINNANGULINE VASTUV�TUTEADE |
(0.6)
K: noo | VR: NEUTRAALNE J�TKAJA |
(.)
S: mmm (1.1) a `igaks peab neile < `helistama > s=ma pean helistama ka < `Jessele, >{--} r��kima mis iganes. | SEE: ARVAMUS |
(0.8) SIIS �������� (0.4) noh `s��gi poole pealt siis on nagu `ses=m�ttes meil `k�ik | IL: KOKKUV�TMINE |
t�na peap=� hakkama: peab hakkama `hakkima=ja `keetma=ja m�llama,= | SEE: ARVAMUS |
K: =kas `mingit sellist nagu (0.5) .hhhh `�ldist `mahla asja on ka `vaja neile:: `pakkuda=v�i v�i=`nii=v�. | KYE: JUTUSTAV KAS |
(0.4)
S: `IGAks juhuks v�iks midagi `olla. | KYJ: JAH |
ei=no=ma: `ise m�ttesin k�ll=et meil on �una`mahla, | KYJ: INFO ANDMINE |
(1.0)
K: teeme oma `kodukootud �una`mahla neile ei akka `ostma mingi [{--}] | SEE: ARVAMUS |
S: [jah, kule=aga] `mille jaoks, | SEJ: N�USTUMINE |
(0.5) ja kui nad `nii v�ga `tahavad jumalu`kene `pood on `samas �le `�ue. | SEE: ARVAMUS |
(0.4) {-} | YA: PRAAK |
(1.1)
K: tegelt see on `�ige kah. | SEJ: N�USTUMINE | 
(1.1) ja kui nad `tulevad kel=`seitse. | KYE: VASTUST PAKKUV |
(0.4)
S: noo ma arvan=et `kaheksaks. | KYJ: INFO ANDMINE |
(0.4) `umbes v�i {-} | IL: T�PSUSTAMINE |
(1.3)
K: jah | VR: NEUTRAALNE J�TKAJA |
(1.5)
S: `Andrus ja `siukset=e Ve- ee `Kaido v�i- `need v�ivad `varem tulla. | KYJ: INFO ANDMINE | 
(1.6) ���mm | YA: MUU |
K: ma `m�tlesin selle `koristamise peale seda `k�ll `t�na ei `tee. | SEE: ARVAMUS |
S: miks. | KYE: AVATUD | | PPE: MITTEM�ISTMINE |
(0.8)
K: % ma=i=`j�ua % vaata mis `kell on. | KYJ: INFO ANDMINE |  | PPJ: L�BIVIIMINE |
(0.3) siin on vaja veel `keeta=ja=qq (.) `hakkida=ja | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE |
(0.4)
S: mm (.) okei=sis= | SEJ: N�USTUMINE |
K: =ma=san (.) omme on `lihtne ma `tulen ma=len=ju: (.) mingi (.) `�ks, (0.5) pool=�ks `�ks, .hhhh olen `siin, (0.6) ja siis ma t�mban lihtsalt tolmu`imejaga ��� p�randad `�ra=ja (0.7) ja siis on:=�� (0.3) `korras. | SEE: ARVAMUS |
(0.4)
S: tegelt nagu `elutuba tuleks `lapiga `ka �le k�ia `mopiga, | SEJ: PIIRATUD N�USTUMINE | | SEE: ARVAMUS |
K: noh seda `nigunii. | SEJ: N�USTUMINE |
(0.9)
S: ja `v�rki. | IL: MUU |
(0.4) .hhh okei, (.) < siis >=������� (.) `s��k on `olemas, < `jook > ideeliselt peaks `olemas=olem= | IL: KOKKUV�TMINE |
`KURAT K�LL MIKS `VALDURIT SIIN EI=`OLE. | KYE: AVATUD | | YA: RETOORILINE K�SIMUS |
(0.6) na`hhui ma=taks teada mida `tema tahab juua. | YA: INFO ANDMINE |
(.) `on tal �kki i`deid. | YA: INFO ANDMINE |
(0.7)
K: `�lut=v�i (.) `viina=v�i? | KYE: VASTUST PAKKUV |
(.)
S: noo `t�pselt. | KYJ: JAH |
.hhhhh < siis >=������ (0.3) `sina (.) ostad `omme v�i t�na, | KYE: ALTERNATIIV |
(1.3)
K: `ma vist ostan `omme. | KYJ: ALTERNATIIV: �KS |
S: mm EVELI, ((h�ikab tuppa)) | KKE: ALGATUS |
(0.4)
E: ah ((toast)) | KKJ: KINNITAMINE |
S: MIS=SA `JOOD. ((h�ikab tuppa)) | KYE: AVATUD |
(0.3)
E: ma=i=`tea. ((toast)) | KYJ: INFO PUUDUMINE |
S: okei. | VR: NEUTRAALNE VASTUV�TUTEADE |
(0.8) mt (0.9) < siis >=��������� `muusika on s�- | YA: MUU |
`AA. | YA: MUU |
����� majapidamis`paberit on vaja. | SEE: V�IDE |
(0.4)
K: se oli sul `kirjas. | SEJ: N�USTUMINE |
(0.5)
S: `peldikupaberit vist ei `ole vaja | SEE: ARVAMUS |
(.) mul on mingi `kolm {-} `rulli. | IL: P�HJENDAMINE |
(0.7) pluss �ks `poolik, | IL: P�HJENDAMINE |
(1.1)
K: no (.) vaata kui siia `tuleb kakskend `inimest siis v�ibolla kulub p�ris `palju. | SEJ: MITTEN�USTUMINE |
(1.0) 
K: aga: ma arvan=et `omse elab `�le. | SEJ: N�USTUMINE | | SEE: ARVAMUS |
(0.8)
S: ma arvan [`ka.] | SEJ: N�USTUMINE | 
K: [v�ibola] on vaja siis `n�dalavahetusel lihtsalt `juurde osta. | SEE: ARVAMUS | 
(0.3) `kauaks see `Sven ja `Maret j��vad. | KYE: AVATUD |
(0.4)
S: ma=i=`tea, | KYJ: INFO PUUDUMINE |
(.) ������� `oleneb, vaatab mis nad `teevad. | KYJ: INFO ANDMINE |
(.) kas=nad tahavad `laub�val �ra minna v�i `p�hab�val. | KYJ: INFO ANDMINE |
K: v�i `esmasp��val=v�i? | KYE: VASTUST PAKKUV |
(0.3)
S: ma=i=`usu=et | KYJ: EI |
esmasp�val ma arvan=t neil on `kool. | IL: P�HJENDAMINE |
(1.5)
K: mm | VR: NEUTRAALNE J�TKAJA |
S: jaa::::::::: (0.7) jah see `oleneb t�iesti t�en�oliselt `Mareti tujust. | KYJ: INFO ANDMINE | 
(0.7) hehe | YA: MUU |
K: {---} | YA: PRAAK |
S: jaa ja=sis `Epp:=eee v�ibolla j��b ka `kauemaks=aga ta v�ibolla l�heb ot=sinna `Meeli juurde. | YA: INFO ANDMINE |
(0.3) neil oli `Meeliga mingi `kokku lepitud mingi `h�ppening, (.) mis nad `teevad. | YA: INFO ANDMINE | 
(0.3) tshillivad ringi `Tartus (.) `laub�val, | YA: INFO ANDMINE |
(.)
K: `ausalt, | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE |
S: mhmh? | KYJ: JAH | | PPJ: L�BIVIIMINE |
(1.2) 
S: sest `Magnus on ju `Norras. | IL: P�HJENDAMINE | | PPJ: L�BIVIIMINE |
(0.3)
K: aa | VR: NEUTRAALNE INFO OSUTAMINE UUEKS | | VR: PARANDUSE HINDAMINE |
(0.9)
S: hehe $ `m(h)itte=et see nagu `takistaks aga okei. $ | IL: MUU |
.hhhhhhhhh | YA: MUU |
K: Epp `lahutab Meeli `meelt, | KYE: VASTUST PAKKUV |
S: jajah | KYJ: JAH | 
(.) .hhhhhhh < siis >=�����mmm (2.2) mt `ongi vist k�ik=v�. | KYE: VASTUST PAKKUV |
(1.7) 
S: v�i on `veel midagi `vaja=v�. | KYE: JUTUSTAV KAS |
(.) 
S: `salhvakate asemel se=on=se `majapidst-`taber, | YA: INFO ANDMINE |
(.) * `taber. * | PA: MUU |
(0.3) mh�h  | IL: HINNANG |
* {-} [{-}] * | YA: PRAAK |
K: [pa]ber | PA: MUU |
S: `keel on nii `s�lmes. | IL: HINNANG |
(1.3) se=on=se `kaks {--} raisk.=hhh | YA: PRAAK |
(1.3)
K: se=on `nii: `r�ve. | SEE: V�IDE |
(.) `au[salt.]  | IL: �LER�HUTAMINE |
S: [hhh]hhhh ((ohkab)) | YA: MUU |
(1.4) 
K: ja ta `haiseb, (1.3) > ta haiseb `nii=v�i=naa aga=ta < `haiseb. | SEE: V�IDE | 
(0.8) $ ma tulin enne `korterisse `k�ik oli `p�rmiaisu t�is. $ | SEE: V�IDE |
(0.9)
S: no=mi=`teeme=siss. | KYE: AVATUD |
K: `midagi ei tee. | KYJ: INFO ANDMINE |
(0.6) joodame enne `t�is=nad ja `siis anname {---} {asja maitsta.} | KYJ: INFO ANDMINE | | DIE: ETTEPANEK |
(.)
S: hehe .hhhhhhhh $ okei, $ | DIJ: N�USTUMINE |
 `kahju=et=ta:: `persse l�ks, muidu `pr��gu peaks ta `kali olema siuke `magus, (1.2) kali. | SEE: ARVAMUS |
(1.3) siuke `hea magus kali, | IL: �LER�HUTAMINE |
(0.5) 
K: ja[h] | SEJ: N�USTUMINE |
S: [`v�]i=sis ma `m�letan=valesti. | PA: ENESEPARANDUS |
(1.7)
K: v�ib`olla me panime ikkagi `p�rmi palju. | SEE: ARVAMUS |
(0.5)
S: v�i=me panime `suhkrut liiga `v�he. | SEE: ARVAMUS |
(1.2)
K: `ei aga sul (.) see ema `�tles=ju=et: `suhkrut tuleb panna: (0.4) `v�hem. | SEJ: MITTEN�USTUMINE | | SEE: V�IDE |
(0.3) `kui `terve `kilo ja=me=panime=`ka. | SEJ: MITTEN�USTUMINE | | SEE: V�IDE |
(0.7)
S: aga ta `alguses �tles terve `kilo=kohe. | SEJ: MITTEN�USTUMINE | | SEE: V�IDE |
(1.5)
K: no aga me=i=pand `nii=palju v�hem. | SEJ: MITTEN�USTUMINE | | SEE: V�IDE |
(0.3) ja `osa suhkrut l�ks ju .hh `p�rmi sisse `ka (.) enne. | SEJ: MITTEN�USTUMINE | | SEE: V�IDE |
S: mhmh | SEJ: PIIRATUD N�USTUMINE |
(1.7) [ma=i=`tea] | SEJ: MITTEN�USTUMINE |
K: [{mm}] | VR: MUU |
(0.6)
S: mt kahtlane. | SEJ: MITTEN�USTUMINE |
(1.6)
S: [{-}]| YA: PRAAK |
K: [ma] `ise m�tlen=et `ainuke asi mis seal `saab=olla .hhh on se=et `p�rmi on `palju, | SEE: ARVAMUS |
(0.5) sest=sul=`ema=�tles=et kaks `pakki, aga ta=i=`�tlend kui:=`rasked pakid. | IL: P�HJENDAMINE |
(.) v�i=noh `kaalult=vata. | PA: ENESEPARANDUS |
S: kas=ma `k�sisin= | KYE: SULETUD KAS | | YA: RETOORILINE K�SIMUS |
ei `k�sinud. | KYJ: EI | | YA: RETOORILINE VASTUS |
(0.8)
K: ei=`vist. | KYJ: EI |
(1.4)
S: k�sisin=`k�ll, | SEE: V�IDE |
K: `mida sa k�- | KYE: AVATUD | | PPE: MITTEM�ISTMINE |
et mi- mitu=`grammi=v�. | KYE: VASTUST PAKKUV | | PPE: �MBERS�NASTAMINE |
S: jaa, | KYJ: JAH | | PPJ: L�BIVIIMINE |
(0.5)
K: ja sis ta �tles=et=`sada=v�. | KYE: VASTUST PAKKUV |
(1.3) 
K: v�i `kaks viie`k�mnest=v�. | KYE: VASTUST PAKKUV |
(0.6)
S: kurat ma=i=`m�leta enam. | KYJ: INFO PUUDUMINE |
K: no=`t�pselt, | VR: HINNANGULINE VASTUV�TUTEADE |
(1.5)
S: {`siukene on=se.} | IL: KOKKUV�TMINE |
(1.4) no=s=ei=ole `midagi, peldikupotist `alla=ja, (0.7) mis=�le=`j��b, | SEE: ARVAMUS |
(1.0)
K: `kui j��b=�le. | SEJ: PIIRATUD N�USTUMINE |
(.) v�ibola osadele `maitseb, ma=i=`tea, (.) v�ibola see=lihtsalt kes=ei=ole=varem=`saand kodu�lu=ja=s=nad .hh maitsvad @ `vuou=nii=`ea, @ | IL: P�HJENDAMINE |
S: hehe .hhhhh $ {-} sureb `viimasena, $ | SEE: ARVAMUS |
(.)
K: jaa | SEJ: N�USTUMINE |
(3.0)
S: `nii, | YA: JUTU PIIRIDE OSUTAMINE |
(.) aaaaaaaaa (0.3) mis `veel=vaja=on | KYE: AVATUD |
`on midagi vaja ei=`ole, | KYE: JUTUSTAV KAS |
(2.2) 
S: [{-}]| YA: PRAAK |
K: [ma=i=`tea] ma arvan=et `poenimekiri on `koos=nagu, | KYJ: EI |
(0.6)
S: no `mis=se v�iks `minna mingi `viis-= | KYE: AVATUD |
K: =sa=k�id `ise poes=�ra=v�. | KYE: SULETUD KAS |
(0.6)
S: ma=i=`viitsi nagu �ksi `minna | KYJ: EI |
sa=i=viitsi `kaasa=tulla=v�, | KYE: SULETUD KAS |
(0.6)
K: no `ma vist `v�in. | KYJ: JAH |
.hhh ma `ise m�tlesin=et ma olks muidu `munad �ra keetnud v�i? | YA: INFO ANDMINE |
(.) `kartulid keema=pannud. | YA: INFO ANDMINE |
.hhh kule=kas `kartulitega teeme `nii=et keedame (.) `koorega v�i `ilma=kooreta. | KYE: ALTERNATIIV |
(0.4)
S: no `eks nad koorime `�ra= | KYJ: ALTERNATIIV: �KS |
et siis ei=ole `homme vaja sellega `tegeleda. | IL: P�HJENDAMINE |
(1.2) et=ma v�in `koorida k�ll. | DIE: PAKKUMINE |
(1.4)
K: me peame `t�na peame `�ra hakkima=ju. | SEE: ARVAMUS |
S: jah. | SEJ: N�USTUMINE |
(2.3) aa. | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.3)
K: ma=m�tsin `KEETmist. | PA: ENESEPARANDUS |
(.)
S: .hhhh mmmmmmm. | VR: MUU |
(0.5) ma=i=tea kas=sis `on mingit vahet v�=i=ola, | KYE: JUTUSTAV KAS | | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.3)
K: ainuke vahe=on `selles=et kui=sa (.) mt (0.3) .hhhh (0.4) `keedad `koos `koorega siis `p�rast on `kartuli `kadu kui selline `v�iksem, | KYJ: INFO ANDMINE | | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(0.7)
S: noo=sis keedame `koorega. | KYJ: ALTERNATIIV: �KS |
(0.4)
K: aga siis=on=vaja neid enne `pesta, | SEE: V�IDE |
S: jaa | SEJ: N�USTUMINE |
(1.1) `Eveli peseb=`�ra onju, | DIE: ETTEPANEK |
(0.3) v�i `�pid=sa=v�i mis=sa=`teed, | KYE: AVATUD |
(0.5)
E: $ midagi? $ | KYJ: INFO ANDMINE |
((on vahepeal teiste juurde tulnud))
(0.5)
K: hehe [hehe] | YA: MUU |
S: [`viitsid kar]tult `pesta, | KYE: SULETUD KAS |
(1.6)
E: `k�iki=neid=v�. | KYE: VASTUST PAKKUV | | VTE: VASTUSE TINGIMUSTE T�PSUSTAMINE |
(1.4)
S: vist jah? | KYJ: JAH | | VTJ: VASTUSE TINGIMUSTE T�PSUSTAMINE |
.haaaa, tegelikult peaks `kindlaks tegema kas selest kartulist=tse=`j�tkub. | KYE: SULETUD KAS |
(.)
K: `j�tkub, seda on �le`arugi. | KYJ: JAH |
S: aa=okei. | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
sis=on=`�sti. | VR: HINNANGULINE VASTUV�TUTEADE |
(1.8)
E: pesen kartuli `�ra ja=`siis?  | KYE: AVATUD |
(1.2)
K: paned `keema lihtsalt. | KYJ: INFO ANDMINE |
(0.9)
S: `suurde=potti. | IL: T�PSUSTAMINE |
(0.4) nii=palju=ku=sin[na=ma-] | KYJ: INFO ANDMINE |
K: [v�i]bola peaks `nii ka `tegema=et kui=sa=nad=`�ra (.) eeee `seda teed, (1.0) pesed, .hhhh et=siis `l�ikad `pooleks sest=et=nad on `ilgelt `pirakad s=nad `keevad `kiiremini, | KYJ: INFO ANDMINE |
(2.1)
E: `n��d `pr��gu `kohe, | KYE: VASTUST PAKKUV |
(0.3)
S: mhmh? | KYJ: JAH |
(.)
E: aa. | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(.)
K: einoh mis n�d `kohe aga `varsti. | KYJ: MUU |
(0.4)
S: mmnnnn `ma l�hen `poodi | YA: INFO ANDMINE |
ma=i=tea kas=sa `tuled=kaasa v�i=`tule. | KYE: ALTERNATIIV |
(1.6)
K: .hhhhhhhhhh HHH @ ma eile `k�isin,@ ((h�diselt)) | KYJ: MUU |
(0.6) > ei ma v�in tulla `k�ll mulle meeldib asju `valida. < | KYJ: ALTERNATIIV: �KS |
(0.4)
S: siis=on=`�sti.= | VR: HINNANGULINE VASTUV�TUTEADE |
K: =peasi=t `nimekiri oleks olemas. | IL: �LER�HUTAMINE |
S: hmheh | YA: MUU |
K: hehe | YA: MUU |
S: juu meik sents=not (2.0) eeeeeeeeee[eee] | YA: MUU |
K: [{mis} on] `loogiline. | YA: MUU |
(0.5)
S: `sibulat meil=`on onju, | KYE: VASTUST PAKKUV |
(0.5)
K: ullult `v�he, | KYJ: INFO ANDMINE |
(0.5)
S: [aga] | YA: MUU |
K: [`mille]=jaoks=sul `sibulat �ldse `vaja=[on] | KYE: AVATUD |
S: > [kartuli]`salati sisse. < | KYJ: INFO ANDMINE |
K: `SIBULAT. | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE |
(0.5) 
K: `mis `ajast, | KYE: AVATUD | | PPE: MITTEM�ISTMINE | | YA: RETOORILINE K�SIMUS |
(1.0)
S: ma=i=`tea. | KYJ: INFO PUUDUMINE | | PPJ: L�BIVIIMINE |
K: kesse paneb `SIBULAT `karttuli[`salati `sisse.] | KYE: AVATUD | | YA: RETOORILINE K�SIMUS |
S: [{-}] * pannakse, * | SEE: V�IDE |
K: `TOOREST `SIBULAT. | KYE: VASTUST PAKKUV | | PPE: �LEK�SIMINE |
(1.4)
S: h�sti=`v�he, ja seda tehakse `ka nagu `�sti �sti `pulbriks nagu. | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE |
(0.8)
S: [{-}]| YA: PRAAK |
K: [a=miks] nad `�ldse seda panevad=sis.= | KYE: AVATUD |
S: =maitse `p�rast pidi: `hullult nagu `oluline olema. | KYJ: INFO ANDMINE |
K: {-} | YA: PRAAK |
(2.2)
K: `r�ve=ju, | SEE: V�IDE |
(0.5)
S: ma=i=`tea, ma olen (0.6) `s��nud seda {---} | SEJ: MITTEN�USTUMINE |
v�ljaarvatut=�� noh (0.4) `v�ljaarvatud hernesupp | IL: T�PSUSTAMINE |
hernesupp mulle=i=`maitse. | SEE: V�IDE |
(.)
K: no `see jah, | SEJ: N�USTUMINE |
(2.4) ((toksimine))
S: `vott, | YA: JUTU PIIRIDE OSUTAMINE <
(0.5) SUL `ON mingi mingi::=ee-eeeee=i`deid. | KYE: JUTUSTAV KAS |
(0.9)
E: ei. | KYJ: EI |
(0.6)
K: mille `kohta �ldse. | KYE: AVATUD | | PPE: MITTEM�ISTMINE |
(0.9)
S: `poenimekirja. | KYJ: INFO ANDMINE | | PPJ: L�BIVIIMINE |
K: aa | VR: NEUTRAALNE INFO OSUTAMINE UUEKS | | VR: PARANDUSE HINDAMINE |
ei see=on=`koos, | KYJ: EI |
(1.6)
S: m�tlen=j�lle `KOHVI=pidin=ostma. | YA: INFO ANDMINE |
(.) `kohvi akkab `otsa=saama, | SEE: V�IDE |
(0.9)
K: aa=| VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
jah, | SEJ: N�USTUMINE |
(2.8)
S: teed `on vaja=v�, | KYE: SULETUD KAS |
(0.7)
E: ei, | KYJ: EI |
(0.4)
S: * aa * | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(1.1)
K: `musta teed=on `piparm�nditeed=on siis=seda: seda `pulbriteed=on=vel, | IL: T�PSUSTAMINE |
(2.3)
S: mt (0.6) sii:::::::::::s::::: (.) `taldrikuid papp`taldrikuid on k�ll `vaja=vist. | SEE: ARVAMUS |
(1.7)
K: a=need=on=seal `kirjas, | SEJ: N�USTUMINE | | SEE: V�IDE |
(0.6)
S: h�sti, | SEJ: N�USTUMINE |
(2.0) `taldrikud. | IL: �LER�HUTAMINE |
(0.8) .hhhhhhhhh okei? eeeeeee | YA: MUU |
(4.6)
K: kas sa tahad kohvi `enne �ra=juua ku=me `poodi l�hme v�i (.) sa jood `p�rast, | KYE: ALTERNATIIV |
(1.5)
K: [{-}]| YA: PRAAK |
S: < [{-}] nii=et me k�ime > mida `varem poes `�ra seda `parem. | KYJ: MUU |
(.) `kell akkab ni=`palju saama=t varsti v-voolab=sinna `viimane rahvas `sisse ja=sis=on toutud * `j�rjekor- * ((s�na l�pp kaob �ra)) | IL: P�HJENDAMINE |
(2.0) 
S: ��[�] | YA: MUU |
K: [et] `tegevusplaan n�eb pr��gu `seda=ette=et `Eveli paneb `kartulid (.)`keema (0.3) `meie l�hme=sis `poodi. | DIE: ETTEPANEK |
S: mh | DIJ: N�USTUMINE |
(0.7)
E: .hh kas ma panen=`ainut sinna `�hte suurde, (1.5) v�i `panen=nagu `teise potti `veel? | KYE: ALTERNATIIV |
(0.8)
K: �hhhhh * kartulid ke- *= | KYJ: MUU |
S: =`koorega=sis. | IL: T�PSUSTAMINE |
(1.3)
E: m | VR: MUU |
K: .hh aga nigu`nii on `vaja (.) mitu `korda keeta. | KYJ: MUU |
(0.3)
S: .hhhhhhh `kaks `korda=umbes (.) selle `suure=potiga. | KYJ: ALTERNATIIV: �KS |
(.)
E: mhmh | VR: NEUTRAALNE J�TKAJA |
(.)
S: onju, | KYE: VASTUST PAKKUV | 
(0.6) umbes | KYE: VASTUST PAKKUV | 
(.)
E: [mhmh?] | KYJ: JAH | 
S: [peaks=`ole]ma. | KYJ: JAH | 
 (0.3) no=et siis=ei=ole nagu `m�tet nagu: (0.4) ma arvan: | IL: P�HJENDAMINE |
K: ma=i=`oska nagu `ennustada kui=`suur see `kogus peaks `olema=v�i=et kui=`palju seda peaks `olema. | KYJ: INFO PUUDUMINE |
S: ma `ka=ei=tea ma `K�SISIN `K�LL `ema k�est siis nad akkasid seal `naerma k��gis k�va `h��lega @{--} {`tunde j�rgi,} @ | KYJ: INFO PUUDUMINE |
(3.3)
E: mhemhe | YA: MUU |
S: ee= | YA: MUU |
K: =meil on k�ll `kodus=et v�ta kolgend `kartulit=ja k�mme `muna=ja umbes=`nimodi. | YA: INFO ANDMINE |
(0.5)
S: meil on `k�ik see: tuleb j�lle=et nii=nagu: `tundub nii `pannakse=ja tehakse=ja=ollakse. | YA: INFO ANDMINE |
K: noh `�ldjuhul `jah=aga noo `ikkagi. | VR: MUU |
S: kas=m�ni `kuivtoit `enda=jaoks on `ka vaja=osta=v�. | KYE: JUTUSTAV KAS |
(1.1) 
S: mingit `riisi=makarone {---} iga: (.) v�i [`n�davahetus] | IL: T�PSUSTAMINE |
E: [`riisi]=makarone ja `siukseid asju ei=`ol�, | KYJ: MUU |
(.)
S: v�i l�hme `n�dalavahetusel `uuesti=lissalt [poodi.] | KYE: VASTUST PAKKUV |
K: [l�hme] `lihtsalt uuesti `poodi. | KYJ: JAH |
(.) `pr��g=on::: `peo:=v�rk. | IL: P�HJENDAMINE |
(0.3)
{-}: mt (.) pidu, | YA: MUU |
(.)
S: pidu=pidu (.) teeme `nimodi=et `mina olen `pidu=ja teie `k�ik * olete pidu. * | YA: MUU |
(1.3)
K: {shii:::t}= | VR: MUU |
E: =`seda=sa=`tahad. ((nipsakalt)) | SEE: MUU |
S: {-} | YA: PRAAK |
K: hehe | YA: MUU |
S: OO=JAA, (0.4) mulle=nii `meeldiks. | SEJ: N�USTUMINE |
rrrrrrrr ((kurgu r-h��lik)) (0.4) mt (0.3) nii, | YA: JUTU PIIRIDE OSUTAMINE |
(0.8) ���������� (1.0) kurat (.) `tekke peaks `�hutama tegelikult. | DIE: ETTEPANEK |
(1.5) `homme hommikul {---} persse, | IL: MUU |
(1.2)
K: okei. | DIJ: N�USTUMINE |
S: `tekke on vaja �hutada. | IL: �LER�HUTAMINE |
(0.9)
K: `l�mme=s �ra `poodi, | DIE: ETTEPANEK |
(1.1)
S: no `l�hme=sis �ra `poodi. | DIJ: N�USTUMINE |
(1.9) � lallallallallaa. � | YA: MUU |

