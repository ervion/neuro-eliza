((531_a24 infotelefon))
((�hestas Andriela R��bis 16.10.2008))
((kutsung))    | RIE: KUTSUNG | 
V: info`telefon   | RIJ: KUTSUNGI VASTUV�TMINE |     | RY: TUTVUSTUS |
=`Kersti   | RY: TUTVUSTUS |
 `tere.    | RIE: TERVITUS |
(1.0) 
H: * tere *   | RIJ: VASTUTERVITUS |
 (.) ma=`paluksin `Eesti:, (0.6) t�endab `P�rnu (0.2) e`lektri`v�rgud.    | DIE: SOOV |
(1.2)
V: [Eest-]    | YA: MUU |
H: [ju]hataja=v�i.    | IL: T�PSUSTAMINE |
 (0.3) ((telefonihelin))
V: kas `Eesti `Energia `m�tlete.    | KYE: VASTUST PAKKUV |        | PPE: �MBERS�NASTAMINE |
(0.5)
H: jah,    | KYJ: JAH |        | PPJ: L�BIVIIMINE |
`Eesti `Energia `P�rnu `osakond.   | IL: T�PSUSTAMINE |        | PPJ: L�BIVIIMINE |
(.)
V: jah.   | VR: NEUTRAALNE VASTUV�TUTEADE |    | VR: PARANDUSE HINDAMINE |
 (.) `osakonnas `numbreid `eraldi ei=`n�ita   | DIJ: INFO PUUDUMINE |
 on `viisteist `nelikend=viis `kliendi`info.       | DIJ: INFO ANDMINE |
(0.7)
H: kuidas=`palun.    | KYE: AVATUD |     | PPE: MITTEM�ISTMINE |
(.)
V: .hh (0.5) `VIISTEIST `NELIK�MMEND=`VIIS ON `KLIENDI`INFO `�LE=`EESTI.    | KYJ: INFO ANDMINE |      | PPJ: L�BIVIIMINE |
(2.0)
V: `eraldi `osa`kondi ei=`n�ita.    | IL: �LER�HUTAMINE |      | PPJ: L�BIVIIMINE |
(2.1)
H: �le `Eesti.   | VR: NEUTRAALNE VASTUV�TUTEADE |
(1.3)
V: [s-]   | YA: MUU |
H: [a=`siin]=ju `midagi `P�rnus `peab `ka `olema `ikka.    | SEE: ARVAMUS |      | DIE: SOOV |
((2. v�ltes))
(1.0)
V: jah, `neil=on `kontor `olemas   | SEJ: N�USTUMINE |
 aga `numbrit sealt `koha`pealt ei=`n�ita.    | DIJ: INFO PUUDUMINE |
(1.8)
V: e[t=`viis]teist `nelikend `viis `saate `k�sida.    | IL: �LER�HUTAMINE |
H: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
(0.9) 
H: viis`teist `nelik�mend=`viis.    | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(.)
V: jah.    | KYJ: JAH |      | PPJ: L�BIVIIMINE |
(.) see=on `kliendi`info.   | IL: T�PSUSTAMINE |     | PPJ: L�BIVIIMINE |
(0.6)
H: viis`teist `nelik�mmend=`viis.    | KYE: VASTUST PAKKUV |     | PPE: �LEK�SIMINE |
(0.2)
V: jah.   | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(0.4)
H: neli `numbrit.    | KYE: VASTUST PAKKUV |     | PPE: �MBERS�NASTAMINE |
(0.3)
V: jah.    | KYJ: JAH |     | PPJ: L�BIVIIMINE |
(1.0)
H: t�nan.   | RIE: T�NAN |
(0.3)
V: palun.    | RIJ: PALUN |
      
