((347a2 infotelefon))
((�htlustas Andriela R��bis 30.09.2003))

 ((kutsung))  | RIE: KUTSUNG |
V: tere    | RIJ: KUTSUNGI VASTUV�TMINE |   | RIE: TERVITUS |
tasuli=`infoliin,     | RY: TUTVUSTUS |

Liis `kuuleb?   | RY: TUTVUSTUS |
H: tere.    | RIJ: VASTUTERVITUS |
.hh ee mul on selline prob`leem=   | YA: EELTEADE |
et `Otep��l on vaja viia `m��blit pr�gim�ele.   | YA: INFO ANDMINE |
 kas te `oskate midagi soovitada `kelle poole `p��rduda.   | KYE: AVATUD |

(...) ((V otsib infot))
V: sin=on=aint `transport`teenus pakkuda.   | KYJ: INFO ANDMINE |
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |

(...) ((V otsib infot))
V: `rahvus`vahelist n�itab mulle aga see ei=ole p�ris `see. | KYJ: INFO ANDMINE |  | KYJ: KAHTLEV |
H: mhmh   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (0.8) aga mingit `firmat kes seal seda (.) p�gi`veoga `tegeleb.   | DIE: SOOV |

(0.5)
V: vaatan?    | DIJ: EDASIL�KKAMINE |
(0.8) et sealt te soovite nagu=sis `tellida jah?   | KYE: VASTUSE TINGIMUSTE T�PSUSTAMINE |   | KYE: VASTUST PAKKUV |
H: jah   | KYJ: JAH |
V: vaatan?   | DIJ: EDASIL�KKAMINE |

(...) ((V otsib infot))
V: `pr�gila, pr�gi`vedu, `pr�giga ei=n�ita `mitte `midagi.   | DIJ: INFO PUUDUMINE |
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: `mitte midagi `pr�giga kes `tegeleb.    | IL: �LER�HUTAMINE |
.hh v�bla=siss (.) .hh annaks teile `linnavalitsuse=numbri saate `k�sida.   | DIE: PAKKUMINE |
H: jahah, jah.   | DIJ: N�USTUMINE |   | DIE: SOOV |
V: `nemad kindlasti oskavad te[id `ai]data.   | IL: P�HJENDAMINE |
H: [mhmh]   | VR: NEUTRAALNE J�TKAJA |
V: .hh ja siis linnaval- linna (.) hooldus`amet on siin antud otseselt,    | YA: INFO ANDMINE |

.hh te soovite �ld- (.) �ld`numbrit v�i `info   | KYE: ALTERNATIIV |

(.)
H: andke `m�lemad.   | KYJ: ALTERNATIIV: M�LEMAD |   | DIE: SOOV |
V: �ld`number on siis `null seitse kuus `suunaga, .hh=  | DIJ: INFO ANDMINE |
H: =jah?   | VR: NEUTRAALNE J�TKAJA |
V: `kuus �heksa `�heksa,   | DIJ: INFO ANDMINE |
H: jah   | VR: NEUTRAALNE J�TKAJA |
V: null `null.   | DIJ: INFO ANDMINE |
H: ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
V: ja linna `hooldusameti number on siis `kuus �heksa `�heksa,=   | DIJ: INFO ANDMINE |
H: =jah   | VR: NEUTRAALNE J�TKAJA |
V: kolm `null,   | DIJ: INFO ANDMINE |

(1.2)
H: ait�h=teile   | RIE: T�NAN |
V: palun?   | RIJ: PALUN |
H: n�gemist?   | RIE: H�VASTIJ�TT |
V: k�ike head.  | RIJ: VASTUH�VASTIJ�TT |  | RIE: SOOVIMINE |

