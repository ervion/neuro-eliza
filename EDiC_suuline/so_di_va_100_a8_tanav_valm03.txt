((100a8 teek�simine))
((�htlustas Andriela R��bis 14.12.2003))

P.T: ((h��ab l�henevatele inimestele)) KAS SIIT SAAB `ROKK SITISS�.   | KYE: JUTUSTAV KAS |

(2.41)
M.S: ((kaugelt)) mis?   | PPE: MITTEM�ISTMINE |      | KYE: AVATUD |
P.T: KAS SIIT SAAB `ROKK `SITISSE.    | PPJ: L�BIVIIMINE |     | KYJ: INFO ANDMINE |     | KYE: JUTUSTAV KAS |
(2.56)
M.S: ((ikka veel kaugelt)) `kuskilt ma lugesin `k�ll tolle kohta=aga `kus too oli, (.) `Rokk Siti.    | KYJ: EDASIL�KKAMINE |     | YA: RETOORILINE K�SIMUS |
(1.55)
P.T: haa et `te ol- ei ole `ka kohalikud.=   | KYE: VASTUST PAKKUV |
M.S: =ei `ole    | KYJ: N�USTUV EI |
(.) ta on `kuskil (.) ta j�i minu=meelest `siia- `siiappoole `k�lge siia n��d `nurka=aga.    | KYJ: INFO ANDMINE |     | KYJ: KAHTLEV |

(1.10) sest `me l�hme praegu kui `kaardi j�rgi?    | KYJ: INFO ANDMINE |
sis=ma v�in `�elda et   | KYJ: INFO ANDMINE |

((v�tab v�lja v�ikese Kihnu kaardi))
K.K: kuhu `siitpoolt saab.   | KYE: AVATUD |
M.S: kus me `oleme.   | KYE: AVATUD |
N.S: siit me saame `sadamasse   | KYJ: INFO ANDMINE |
M.S: ((n�itab kaarti)) me n��d `siit siit seda teed `tulime, > me oleme siin < `metsa `serva p��le `minekul.    | KYJ: INFO ANDMINE |

(.)
K.K: mhmh   | VR: NEUTRAALNE J�TKAJA |
M.S: aga ma=ei `tea kas too j��b siis `sinna v�i j�i `siia ta j�i [`siia k�lge kuhugi `j�i `lennujaam on] `seal   | KYJ: INFO ANDMINE |     | KYJ: KAHTLEV |
P.T: [lennujaam (-)]   | YA: PRAAK |
K.K: ta j�i minu=meelest [ikka] `siia �les.   | SEE: ARVAMUS |
P.T: [siia]   | YA: MUU |
M.S: seep�rast `sis: peab sinna `�les minema, see=on `turismitalu.    | SEJ: N�USTUMINE |
(.) v�i too `ongi too tur- v�i see `turismitalu v�i `misasi [seal `on].   | KYE: JUTUSTAV KAS |
K.K: [ei ma ei `tea seal lihtsalt on {kaks t�kki}]   | KYJ: INFO PUUDUMINE |
P.T: [ahah   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
 teie l�hete `sadamasse] ujuma   | KYE: VASTUST PAKKUV |
M.S: me l�heme `sadamasse (.) .nhh    | KYJ: JAH |
((t�mbab ninaga �hku))
P.T: ja me `peaks hoidma hoopis teisele `poole=sis.   | KYE: VASTUST PAKKUV |
M.S: jaa    | KYJ: JAH |
(.) ma ei `tea, `minu meelest ta j�i kuhugi siia=`kanti | KYJ: INFO ANDMINE |     | KYJ: KAHTLEV |
aga m�ne m�ne `m�ne k�est kuskilt `k�sida=siit.  | DIE: ETTEPANEK | 
K.K: [mhmh]   | DIJ: PIIRATUD N�USTUMINE |
P.T: [ahah]    | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |

(1.70)
K.K: ahaa   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
P.T: jah   | VR: NEUTRAALNE VASTUV�TUTEADE |
 (.) noo?   | VR: NEUTRAALNE PIIRITLEJA |
K.K: no igatahes `ait�h=sis   | RIE: T�NAN |
P.T: [ait�h]   | RIE: T�NAN |
M.S: [me] v�ime plaani `teile j�tta=    | DIE: PAKKUMINE |
((ulatab plaani K.Kle))
K.K: [kas teil ei l�he seda tarvis]   | DIJ: PIIRATUD N�USTUMINE |     | KYE: JUTUSTAV KAS |
M.S: = [sis on parem `k�ndida]    | IL: P�HJENDAMINE |
me l�heme juba s�idame `�ra=[sis] | KYJ: INFO ANDMINE |
 [ja] teil `laenate j�rgmistele      | DIE: ETTEPANEK |
K.K: [ahaa]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |
P.T: [ahah]   | VR: NEUTRAALNE INFO OSUTAMINE UUEKS |

   

 

