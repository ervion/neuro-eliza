#!/usr/bin/python3
# ###
# Võtab märgendatud dialoogid ning teisendab tõlkekorpuseks.
#
# Ervin Oro
###
import glob
import json
import re


def clean(message):
    ## Võtab märgendatud tekstirea ning tagastab tühikutega eraldatud tokenid
    message = re.sub(r'\s*\|.*\|\s*', '', message)
    message = message.lower()
    message = re.sub(r'[\.=]h+\W', '', message)
    message = re.sub(r'`|\$\s|(?<=(\W))\.|(?<=^)\.', '', message)
    message = re.sub(r'(?<=\w)[=](?=\w)', ' ', message)
    message = re.sub(r'=|:', '', message)
    message = re.sub(r'(\(\(|\[|{|\().*?(\)\)|\]|}|\))', '', message)
    message = re.sub(r'(?<=[^.\s])([.,!?])(?=($|\s))', ' \g<1>', message)
    message = re.sub(r'(?<=\w)([-])(?=[a-z]{1,3}\W)', ' \g<1>', message)
    message = re.sub(r'[^a-züõöäžš .,?]', ' ', message)
    message = re.sub(r'\s+', ' ', message)
    return message.strip()


failid = glob.glob('EDiC_arvuti/Annika*_valm*.txt')
failid.extend(glob.glob('EDiC_woz/*.txt'))
failid.extend(glob.glob('EDiC_suuline/*.txt'))
question_out = ""
answer_out = ""

vocabularies = {
    'q': {

    },
    'a': {

    }
}


def addVocab(question, answer):
    global vocabularies
    for word in question.split():
        if word not in vocabularies['q']:
            vocabularies['q'][word] = 1
        else:
            vocabularies['q'][word] += 1
    for word in answer.split():
        if word not in vocabularies['a']:
            vocabularies['a'][word] = 1
        else:
            vocabularies['a'][word] += 1

for failinimi in failid:
    failisisu = open(failinimi, encoding='ISO-8859-15').read().strip().split('\n')

    freim = {}
    last = None
    for line in failisisu:
        parsed = re.match(r'^\s*([A-ZÜÕÖÄŽŠ][a-züõöäžš]*)(\W+\(.+?\))?:\s+(.*)$', line)
        if parsed is not None:
            if last != parsed.group(1):
                freim[parsed.group(1)] = []
            last = parsed.group(1)
            message = clean(parsed.group(3))
            if message != "":
                freim[last].append(message)
        else:
            if last == None: continue
            message = clean(line.strip())
            if message != "":
                freim[last].append(message)
        if message != "":
            for k in freim.keys():
                if k == last: continue
                for question in freim[k]:
                    answer_out += message + "\n"
                    question_out += question + "\n"
                    addVocab(question, message)

with open("questions.corpus", "w") as questions:
    questions.write(question_out)
with open("answers.corpus", "w") as answers:
    answers.write(answer_out)

with open("questions.vocab.json", "w") as questions:
    questions.write(json.dumps(vocabularies['q']))
with open("answers.vocab.json", "w") as answers:
    answers.write(json.dumps(vocabularies['a']))
